import React, { FC } from 'react'
import { DateTimePicker } from '@mui/lab'
import { TextField } from '@mui/material'
import { FormikProps } from 'formik'

type Props = {
  formikProps: FormikProps<any>
}
export const NewTestTimes: FC<Props> = ({ formikProps }) => {
  const handleChange = (newValue: any) => {
    formikProps.setFieldValue('startDate', newValue)
  }
  return (
    <DateTimePicker
      label="Date&Time picker"
      value={formikProps.values.startDate}
      onChange={handleChange}
      renderInput={(params) => <TextField {...params} />}
    />
  )
}
