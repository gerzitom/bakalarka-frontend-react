import React, { FC } from 'react'
import { Classroom } from '../types/entities'
import { StyledListItem } from '../bundles/HomePageBundle/components/TestTemplatePreview'
import { Box } from '@mui/system'
import { Divider, Typography } from '@mui/material'

type Props = {
  classroom: Classroom
}
export const ClassroomPreview: FC<Props> = ({ classroom }) => {
  return (
    <Box sx={{ mx: -4 }}>
      <Divider />
      <Box sx={{ px: 4, py: 2 }}>
        <Typography variant={'h6'}>{classroom.name}</Typography>
        <Typography variant={'body2'}>
          Studenti: {classroom.students.length}
        </Typography>
      </Box>
    </Box>
  )
}
