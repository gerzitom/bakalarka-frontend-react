import React, { FC } from 'react'
import { Chip, Grid, Typography } from '@mui/material'
import { Test } from '../types/entities'
import { StyledListItem } from '../bundles/HomePageBundle/components/TestTemplatePreview'
import styled from 'styled-components'
import { formatTime, isTimeClose } from '../utils'
import Link from 'next/link'

type Props = {
  test: Test
}
export const TestPreview: FC<Props> = ({ test }) => {
  return (
    <Link href={`/app/test_preview/${test.id}`}>
      <a>
        <StyledListItem sx={{ mx: -4, px: 4, py: 2 }}>
          <Grid
            container
            justifyContent={'space-between'}
            alignItems={'center'}
          >
            <Grid item flexGrow={1}>
              <Typography variant={'body1'}>{test.name}</Typography>
              <Chip label={'8.A'} size={'small'} />
            </Grid>
            <Grid item>
              <TestTiming test={test} />
            </Grid>
          </Grid>
        </StyledListItem>
      </a>
    </Link>
  )
}

/**
 * Shows test status.
 * Statuses:
 *  - not started: shows start time
 *  - started: shows chip, that test has started
 *  - finished: shows chip. that test has ended
 * @param test
 * @constructor
 */
const TestTiming: FC<Props> = ({ test }) => {
  const startDateTimestamp = Date.parse(test.startDate)
  if (isNaN(startDateTimestamp)) return <></>
  const startDate = new Date(startDateTimestamp)
  const testDone = new Date() > startDate
  const timeClose = isTimeClose(startDate)

  const start = new Date(test.startDate)
  const end = new Date(test.endDate)
  const now = new Date()
  if (start < now && now < end)
    return (
      <Chip color={'success'} label={'Konec v: ' + formatTime(end, true)} />
    )

  const formatedStartDate = formatTime(startDate)
  if (testDone) return <Chip color={'info'} label={'Ukončeno'} />
  else
    return (
      <div>
        <Typography variant={'body2'} color={timeClose ? 'error' : 'secondary'}>
          {formatedStartDate}
        </Typography>
      </div>
    )
}

const TimeStart = styled(Typography)``
