import React, { FC, useState } from 'react'
import styled from 'styled-components'
import { Card, Fade, IconButton, Modal } from '@mui/material'
import CloseIcon from '@mui/icons-material/Close'

type UseModalReturnType = {
  isModalOpen: boolean
  openModal(): void
  closeModal(): void
  toggleIsModalOpen: () => void
}
export type UseModalType = (initialState?: boolean) => UseModalReturnType

export const useModalState: UseModalType = (initialState = false) => {
  const [isModalOpen, setIsModalOpen] = useState<boolean>(initialState)
  const toggleIsModalOpen = () => setIsModalOpen(!isModalOpen)
  const closeModal = () => setIsModalOpen(false)
  const openModal = () => setIsModalOpen(true)

  return {
    isModalOpen,
    closeModal,
    openModal,
    toggleIsModalOpen,
  }
}

type Props = {
  isOpen: boolean
  closeModal(): void
}

export const ModalWrapper: FC<Props> = (props) => {
  const { children, isOpen, closeModal } = props

  return (
    <Modal open={isOpen} onClose={closeModal}>
      <Fade in={isOpen}>
        <ModalCard>
          <StyledCloseButton color="primary" onClick={closeModal}>
            <CloseIcon />
          </StyledCloseButton>
          {children}
        </ModalCard>
      </Fade>
    </Modal>
  )
}

const ModalCard = styled(Card)`
  position: relative;
  padding: 58px 48px;
  max-width: 500px;
  margin: 50px auto;
`

const StyledCloseButton = styled(IconButton)`
  position: absolute !important;
  top: 20px;
  right: 20px;
  outline: none;

  &:focus {
    outline: none;
  }
`
