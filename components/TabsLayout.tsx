import React, { FC, ReactNode, useState } from 'react'
import styled from 'styled-components'
import { IconButton, Tab, Tabs, Tooltip } from '@mui/material'
import AddIcon from '@mui/icons-material/Add'
import DeleteIcon from '@mui/icons-material/Delete'

export const useTabState = (startTab: number = 0) => {
  const [activeTab, setActiveTab] = useState(startTab)
  return {
    activeTab,
    setActiveTab,
  }
}

type Props = {
  headings: String[]
  tabsContent: ReactNode[]
  activeTab: number
  setActiveTab: (activeTab: number) => void
  ariaLabel?: string
  newTabAdded?: () => void
  deletePart?: () => void
}
export const TabsLayout: FC<Props> = (props) => {
  const {
    headings,
    tabsContent,
    ariaLabel,
    newTabAdded,
    activeTab,
    setActiveTab,
    deletePart,
  } = props
  const handleChange = (event: React.ChangeEvent<{}>, newValue: number) =>
    setActiveTab(newValue)

  const ariaProps = (index: number) => ({
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`,
  })
  return (
    <>
      <TabsHeader>
        <StyledTabs
          value={activeTab}
          onChange={handleChange}
          aria-label={ariaLabel || 'Tabs'}
          indicatorColor="primary"
        >
          {headings.map((heading, index) => (
            <StyledTab key={index} label={heading} {...ariaProps(index)} />
          ))}
          {newTabAdded && (
            <AddButtonContainer>
              <IconButton size={'small'} onClick={newTabAdded}>
                <AddIcon />
              </IconButton>
            </AddButtonContainer>
          )}
        </StyledTabs>
        {deletePart && (
          <Tooltip title={'Smazat část testu'}>
            <IconButton color={'error'} onClick={deletePart}>
              <DeleteIcon fontSize={'large'} />
            </IconButton>
          </Tooltip>
        )}
      </TabsHeader>
      {tabsContent.map((tabContent, index) => (
        <TabPanel key={index} value={activeTab} index={index}>
          {tabContent}
        </TabPanel>
      ))}
    </>
  )
}

interface TabPanelProps {
  children?: ReactNode
  index: number
  value: number
}
const TabPanel: FC<TabPanelProps> = (props) => {
  const { children, value, index, ...other } = props

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && <>{children}</>}
    </div>
  )
}

const TabsHeader = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
`

const StyledTabs = styled(Tabs)`
  position: relative;
  background: transparent;
  .MuiTabs-flexContainer {
    &::after {
      content: '';
      width: 100%;
      bottom: 0;
      position: absolute;
      border-bottom: 2px solid rgba(0, 0, 0, 0.05);
    }
  }
`

const StyledTab = styled(Tab)`
  min-width: 140px;
  &:focus {
    outline: none;
  }
`

const AddButtonContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
`
