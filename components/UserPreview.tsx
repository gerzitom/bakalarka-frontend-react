import React, { FC, useState } from 'react'
import { Button, Menu, MenuItem, Typography } from '@mui/material'
import { useUser } from '../hooks/useUser'
import styled from 'styled-components'
import { signOut } from 'next-auth/react'
import { UserAvatar } from './UserAvatar'
import Link from 'next/link'

export const UserPreview: FC = () => {
  const [anchorElUser, setAnchorElUser] = useState<null | HTMLElement>(null)
  const { user } = useUser()
  const handleOpenUserMenu = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorElUser(event.currentTarget)
  }

  const handleCloseUserMenu = () => {
    setAnchorElUser(null)
  }
  if (!user)
    return (
      <Link href={'/auth/login'}>
        <Button color={'secondary'} variant={'contained'}>
          Přihlásit se
        </Button>
      </Link>
    )
  return (
    <>
      <Button
        variant={'text'}
        sx={{ px: 3, display: 'flex', alignItems: 'center' }}
        onClick={handleOpenUserMenu}
      >
        <UserAvatar avatarId={user?.email} />
        <UserName>{user?.name ?? ''}</UserName>
      </Button>
      <Menu
        sx={{ mt: '45px' }}
        id="menu-appbar"
        anchorEl={anchorElUser}
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'right',
        }}
        keepMounted
        transformOrigin={{
          vertical: 'top',
          horizontal: 'right',
        }}
        open={!!anchorElUser}
        onClose={handleCloseUserMenu}
      >
        <MenuItem onClick={handleCloseUserMenu}>
          <Typography textAlign="center">Nastavení</Typography>
        </MenuItem>
        <MenuItem onClick={() => signOut({ callbackUrl: '/app' })}>
          <Typography textAlign="center">Odhlásit se</Typography>
        </MenuItem>
      </Menu>
    </>
  )
}

const UserName = styled.div`
  color: ${(props) => props.theme.palette.text.primary};
`
