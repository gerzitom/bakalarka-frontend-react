import React, { FC } from 'react'
import styled from 'styled-components'
import { CircularProgress } from '@mui/material'

export const FullpageSpinner: FC = () => {
  return (
    <FullPage>
      <CircularProgress disableShrink />
    </FullPage>
  )
}

const FullPage = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background: white;
  z-index: 999;
  display: flex;
  justify-content: center;
  align-items: center;
`
