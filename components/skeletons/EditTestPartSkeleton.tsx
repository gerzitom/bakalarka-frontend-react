import React, { FC } from 'react'
import { Button, Grid, Skeleton, Typography } from '@mui/material'
import { BasicLayout } from '../layout/BasicLayout'

export const EditTestPartSkeleton: FC = () => {
  return (
    <BasicLayout>
      <Typography variant={'overline'}>Upravit část testu</Typography>
      <Typography variant={'h3'}>
        <Skeleton />
      </Typography>
      <Typography variant={'body2'}>Otázky</Typography>
      <Grid container spacing={3}>
        <Grid item xs={3}>
          <Button disabled fullWidth>
            Přidat další otázku
          </Button>
        </Grid>
        <Grid item xs={9}></Grid>
      </Grid>
    </BasicLayout>
  )
}
