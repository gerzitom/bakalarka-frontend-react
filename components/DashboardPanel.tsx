import React, { FC } from 'react'
import { Paper, Typography } from '@mui/material'

type Props = {
  heading: string
}
export const DashboardPanel: FC<Props> = ({ heading, children }) => {
  const paperStyles = {
    sx: {
      p: 4,
      mb: 3,
    },
    elevation: 0,
  }
  return (
    <Paper {...paperStyles}>
      <Typography variant={'h3'} sx={{ mb: 3 }}>
        {heading}
      </Typography>
      {children}
    </Paper>
  )
}
