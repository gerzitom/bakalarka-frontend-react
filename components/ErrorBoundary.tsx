import React from 'react'
import { ApiErrorPage } from './ApiErrorPage'
import * as Sentry from '@sentry/browser'

class ErrorBoundary extends React.Component {
  constructor(props: any) {
    super(props)
    this.state = { hasError: false }
  }
  static getDerivedStateFromError() {
    return { hasError: true }
  }
  componentDidCatch(error: Error) {
    Sentry.captureException(error)
  }
  render() {
    //@ts-ignore
    if (this.state.hasError) {
      return <ApiErrorPage />
    }

    // Return children components in case of no error

    return this.props.children
  }
}

export default ErrorBoundary
