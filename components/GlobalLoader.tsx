import React, { createContext, FC, useContext, useState } from 'react'
import styled from 'styled-components'
import { Card, CircularProgress } from '@mui/material'
import { TestTemplate } from '../types/entities'
import { TemplateContext } from '../bundles/EditTemplateBundle/utils'

type GlobalLoading = {
  state: boolean
  setState: (newState: boolean) => void
}
export const GlobalLoadingContext = createContext<GlobalLoading | undefined>(
  undefined,
)
export const useGlobalLoading = () => {
  const globalLoading = useContext(GlobalLoadingContext)
  const startGlobalLoading = () => globalLoading?.setState(true)
  const stopGlobalLoading = () => globalLoading?.setState(false)

  return {
    globalLoading: globalLoading?.state ?? false,
    startGlobalLoading,
    stopGlobalLoading,
  }
}

export const GlobalLoader: FC = () => {
  const { globalLoading } = useGlobalLoading()
  return (
    <LoadingBadge $visible={globalLoading}>
      <Card sx={{ p: 1, display: 'flex', alignItems: 'center' }}>
        <CircularProgress disableShrink size={20} color={'info'} />
        <SavingText>Ukládání</SavingText>
      </Card>
    </LoadingBadge>
  )
}

const LoadingBadge = styled.div<{ $visible: boolean }>`
  @keyframes example {
    from {
      opacity: 1;
    }
    to {
      opacity: 0.5;
    }
  }
  position: fixed;
  bottom: 40px;
  right: 40px;
  transition: 300ms;
  animation: example 1s linear 0s infinite alternate;
  display: ${(props) => (props.$visible ? 'block' : 'none')};
`

const SavingText = styled.span`
  padding-left: 10px;
  font-weight: 500;
  font-size: 0.9rem;
  text-transform: uppercase;
`
