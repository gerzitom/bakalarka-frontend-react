import React, { FC } from 'react'
import { Button, TextField, Typography } from '@mui/material'
import { ModalWrapper, useModalState } from './ModalWrapper'
import { Formik, Form } from 'formik'
import * as Yup from 'yup'
import { FormikSubmitHandler } from '../types'

type Props = {
  buttonText: string
  heading: string
  formSubmitted: (name: string) => void
  additionalSchema?: any
  fieldsInitVals?: Object
}
export const ModalWithNameInput: FC<Props> = (props) => {
  const {
    buttonText,
    heading,
    formSubmitted,
    fieldsInitVals,
    children,
    additionalSchema,
  } = props
  const { openModal, isModalOpen, closeModal } = useModalState(false)
  const Schema = Yup.object().shape({
    name: Yup.string().required('Jméno je povinné'),
    ...additionalSchema,
  })
  const initVals = {
    name: '',
    ...fieldsInitVals,
  }

  const handleSubmit: FormikSubmitHandler<any> = (values) => {
    closeModal()
    formSubmitted(values)
  }
  return (
    <div>
      <Button variant={'outlined'} onClick={openModal}>
        {buttonText}
      </Button>
      <ModalWrapper isOpen={isModalOpen} closeModal={closeModal}>
        <Typography variant={'h4'}>{heading}</Typography>
        <Formik
          initialValues={initVals}
          onSubmit={handleSubmit}
          validationSchema={Schema}
        >
          {(formikProps) => (
            <Form>
              <TextField
                fullWidth
                size={'small'}
                name={'name'}
                label={'Název'}
                value={formikProps.values.name}
                onChange={formikProps.handleChange}
                error={!!formikProps.errors.name}
                helperText={formikProps.errors.name}
                autoFocus
                sx={{ my: 2 }}
              />
              {children}
              <Button
                color={'primary'}
                variant={'outlined'}
                fullWidth
                type={'submit'}
                sx={{ mt: 2 }}
              >
                Vytvořit šablonu
              </Button>
            </Form>
          )}
        </Formik>
      </ModalWrapper>
    </div>
  )
}
