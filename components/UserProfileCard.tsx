import React, { FC } from 'react'
import { UserAvatar } from './UserAvatar'
import { Typography } from '@mui/material'
import { Box } from '@mui/system'
import { User } from '../types/entities'

type Props = {
  user: User
}
export const UserProfileCard: FC<Props> = ({ user }) => {
  return (
    <Box key={user.id} sx={{ display: 'flex', alignItems: 'center', my: 2 }}>
      <UserAvatar avatarId={user.email} />
      <div>
        <Typography variant={'body1'} sx={{ lineHeight: 1 }}>
          {user.name}
        </Typography>
        <Typography variant={'body2'} color={'gray'}>
          {user.username}
        </Typography>
      </div>
    </Box>
  )
}
