import React, { FC, useState } from 'react'
import styled from 'styled-components'
import { DndProvider } from 'react-dnd'
import { HTML5Backend } from 'react-dnd-html5-backend'
import { Grid, Typography } from '@mui/material'
import { Option, TestQuestionGroup } from '../../types/entities'
import { Dustbin } from './components/Dustbin'
import { Box } from './components/Box'
import { colors } from '../../styles/colors'
import { useFormikContext } from 'formik'
interface SpojovackaFormikValues {
  [key: string]: number
}
type Props = {
  questionGroup: TestQuestionGroup
}
export const Spojovacka: FC<Props> = ({ questionGroup }) => {
  const { questions } = questionGroup
  const { values } = useFormikContext<SpojovackaFormikValues>()
  const items = questions
    .filter((question) => !values[question.id])
    .map((question) => question.possibleAnswers[0])
  const [availableItems, setAvailableItems] = useState(items)
  const dropped = (option: Option) => {
    setAvailableItems((prevState) => {
      return prevState.filter((item) => item.id !== option.id)
    })
  }
  const deleted = (item: Option) => {
    setAvailableItems((prevState) => {
      return [...prevState, item]
    })
  }
  return (
    <div>
      <DndProvider backend={HTML5Backend}>
        <Grid container justifyContent={'space-between'} sx={{ mt: 3 }}>
          <Grid item style={{ overflow: 'hidden' }}>
            {questions.map((question, index) => (
              <QuestionRow key={index}>
                <span>{question.content}</span>
                <Dustbin
                  id={question.id}
                  questions={questions}
                  dropped={dropped}
                  deleted={deleted}
                  items={items}
                />
              </QuestionRow>
            ))}
          </Grid>
          <Grid item style={{ overflow: 'hidden' }}>
            <OptionsContent>
              {availableItems.map((item, key) => (
                <Box key={item.id} item={item} />
              ))}
            </OptionsContent>
          </Grid>
        </Grid>
      </DndProvider>
    </div>
  )
}

export const QuestionRow = styled.div`
  display: flex;
  align-items: center;
  column-gap: 10px;
  margin-bottom: 1em;
`

const OptionsContent = styled.div`
  background: ${colors.lightBackground};
  padding: 1em;
  display: flex;
  flex-direction: column;
  height: 100%;
  border-radius: 10px;
`
