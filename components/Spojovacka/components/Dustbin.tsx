import type { FC } from 'react'
import { useDrop } from 'react-dnd'
import { ItemTypes } from '../types'
import { IconButton, Typography } from '@mui/material'
import { Delete } from '@mui/icons-material'
import styled from 'styled-components'
import { Option, Question } from '../../../types/entities'
import { useField, useFormikContext } from 'formik'
import { colors } from '../../../styles/colors'

type Props = {
  id: number | string
  questions: Question[]
  dropped: (item: Option) => void
  deleted: (item: Option) => void
  items: Option[]
}
export const Dustbin: FC<Props> = ({
  id,
  dropped,
  deleted,
  items,
  questions,
}) => {
  const [field] = useField(String(id))
  const { values, setFieldValue } = useFormikContext()
  const [{ canDrop, isOver }, drop] = useDrop<Option, Option, any>(() => ({
    accept: ItemTypes.BOX,
    drop: (item, monitor) => {
      setFieldValue(String(id), item.id)
      dropped(item)
      return item
    },
    collect: (monitor) => ({
      isOver: monitor.isOver(),
      canDrop: monitor.canDrop(),
    }),
  }))

  const isActive = canDrop && isOver
  let backgroundColor = colors.lightBackground
  let borderColor = ''
  let opacity = ''
  if (isActive) {
    borderColor = colors.main
  } else if (canDrop) {
    opacity = '1'
  }

  // @ts-ignore
  const selectedId = values[id]
  const selectedItem = questions
    .map((question) => question.possibleAnswers[0])
    .find((possibleAnswer) => possibleAnswer.id === selectedId)
  const text = selectedItem?.singleOption.text ?? ''

  const handleDelete = () => {
    deleted(selectedItem!)
    setFieldValue(String(id), '')
  }
  return (
    <BinContainer
      ref={drop}
      role={'Dustbin'}
      style={{ backgroundColor, borderColor, opacity }}
      active={!!text}
    >
      <div>{text}</div>
      {selectedItem && (
        <IconButton onClick={handleDelete}>
          <Delete fontSize={'small'} />
        </IconButton>
      )}
    </BinContainer>
  )
}

export const BinContainer = styled.div<{ active: boolean }>`
  height: 40px;
  min-width: 150px;
  display: flex;
  align-items: center;
  text-align: center;
  border-radius: 5px;
  transition: 300ms;
  border: 1px solid ${(props) => (props.active ? colors.main : 'transparent')};
  opacity: 0.7;
  div {
    flex-grow: 1;
  }
`
