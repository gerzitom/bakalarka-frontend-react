import type { CSSProperties, FC } from 'react'
import { useDrag } from 'react-dnd'
import { ItemTypes } from '../types'
import { Option } from '../../../types/entities'
import { Typography } from '@mui/material'
import React from 'react'

const style: CSSProperties = {
  border: '1px dashed gray',
  backgroundColor: 'white',
  padding: '0.5rem 1rem',
  marginRight: '1.5rem',
  marginBottom: '1.5rem',
  cursor: 'move',
  float: 'left',
}

export interface BoxProps {
  item: Option
}

export const Box: FC<BoxProps> = function Box({ item }) {
  const [{ isDragging }, drag] = useDrag<Option, Option, any>(() => ({
    type: ItemTypes.BOX,
    item: item,
    collect: (monitor) => ({
      isDragging: monitor.isDragging(),
    }),
  }))
  const opacity = isDragging ? 0.4 : 1
  return (
    <div ref={drag} role="Box" style={{ ...style, opacity }}>
      {item?.singleOption.text ?? ''}
    </div>
  )
}
