export const ItemTypes = {
  BOX: 'box',
}

export type ItemData = {
  id: number | string
  name: string
}
