import React, { FC, useEffect, useState } from 'react'
import { Button, Card, Container } from '@mui/material'
import { signIn, useSession } from 'next-auth/react'
import { Box } from '@mui/system'
import styled from 'styled-components'
import { BasicLayoutAppBar } from './BasicLayoutAppBar'
import { GlobalLoader, useGlobalLoading } from '../GlobalLoader'
import { UserContext } from '../../hooks/useUser'
import { settings } from '../../utils/ReactQueryClient'
import { User } from '../../types/entities'
import UserService from '../../utils/api/UserService'
import { ApiErrorPage } from '../ApiErrorPage'
import * as Sentry from '@sentry/browser'

type Props = {
  fullWidth?: boolean
}
export const BasicLayout: FC<Props> = ({ children, fullWidth = false }) => {
  const { data: session, status } = useSession()
  const [user, setUser] = useState<User | null>(null)
  const [apiError, setApiError] = useState(false)
  useEffect(() => {
    settings.token = session?.token as string
    UserService.getCurrentUser()
      .then((userfromApi) => setUser(userfromApi))
      .catch((err) => {
        Sentry.captureException(err)
        setApiError(true)
      })
  }, [session?.token])

  if (apiError) return <ApiErrorPage />
  else if (status === 'loading' || !user) return <GlobalLoader />
  else if (session) {
    if (fullWidth) {
      return (
        <UserContext.Provider value={user}>
          <BasicLayoutAppBar />
          <StyledFullWidthBox sx={{ p: 5 }}>
            {children}
            <GlobalLoader />
          </StyledFullWidthBox>
        </UserContext.Provider>
      )
    } else
      return (
        <UserContext.Provider value={user}>
          <BasicLayoutAppBar />
          <Container sx={{ mt: 3 }}>
            {children}
            <GlobalLoader />
          </Container>
        </UserContext.Provider>
      )
  } else
    return (
      <Box
        sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}
      >
        <Card elevation={2} sx={{ p: 3, maxWidth: 400 }}>
          <p>Nejste přihlášeni, musíte se přihlásit</p>
          <Button onClick={() => signIn()}>Přihlásit se</Button>
        </Card>
      </Box>
    )
}

const StyledFullWidthBox = styled(Box)`
  display: flex;
  flex-direction: column;
  flex-grow: 1;
  max-height: calc(100vh - 72px);
`
