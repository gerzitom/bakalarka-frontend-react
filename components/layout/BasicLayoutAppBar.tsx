import React, { FC } from 'react'
import { AppBar, Box, Grid } from '@mui/material'
import Link from 'next/link'
import { Logo } from '../Logo'
import { UserPreview } from '../UserPreview'

export const BasicLayoutAppBar: FC = () => {
  return (
    <AppBar sx={{ p: 1 }} position={'static'}>
      <Grid
        container
        justifyContent={'space-between'}
        alignItems={'center'}
        sx={{ px: 3 }}
      >
        <Grid item>
          <Link href={'/app'}>
            <a>
              <Logo />
            </a>
          </Link>
        </Grid>
        <Grid item>
          <Box sx={{ display: 'flex' }}>
            <UserPreview />
          </Box>
        </Grid>
      </Grid>
    </AppBar>
  )
}
