import React, { FC } from 'react'
import { Button, Container, Paper, Typography } from '@mui/material'
import Link from 'next/link'
import { signOut } from 'next-auth/react'

type Props = {}
export const ApiErrorPage: FC<Props> = () => {
  return (
    <Container>
      <Paper sx={{ p: 4, mt: 8, textAlign: 'center' }}>
        <Typography variant={'h1'} sx={{ mb: 2 }}>
          API chyba
        </Typography>
        <Typography variant={'subtitle1'}>
          Omlouváme se, ale naše servery v této chvíli nefungují, zkuste to
          prosím později.
        </Typography>
        <div>
          <Button
            color={'error'}
            variant={'outlined'}
            sx={{ mt: 3 }}
            onClick={() => signOut({ callbackUrl: '/app' })}
          >
            Odhlásit se
          </Button>
        </div>
        <Link href={'/'}>
          <Button variant={'text'} sx={{ mt: 4 }}>
            Zpět na hlavní stránku
          </Button>
        </Link>
      </Paper>
    </Container>
  )
}
