import { useField } from 'formik'
import React, { FC } from 'react'
import {
  FormHelperText,
  InputLabel,
  MenuItem,
  Select,
  SelectProps,
  TextFieldProps,
} from '@mui/material'
import { SelectItem } from '../types'
import styled from 'styled-components'

type Props = SelectProps &
  TextFieldProps & {
    name: string
    label?: string
    textLabel: string
    values: SelectItem[]
    placeholder?: string
    className?: string
    helperText?: string
    required?: boolean
    customOnChange?(
      event: React.ChangeEvent<{ name?: string; value: unknown }>,
    ): any
  }

export const FormSelect: FC<Props> = (props) => {
  const {
    textLabel,
    name,
    values,
    placeholder,
    className,
    customOnChange,
    helperText,
    error,
    required,
    ...rest
  } = props

  const [field] = useField(name)
  const { onChange, value, onBlur } = field
  const labelId = `${name}Label`

  return (
    <div className={className}>
      <InputLabel htmlFor={name} id={labelId}>
        {textLabel}
        {required && ' *'}
      </InputLabel>
      <Select
        name={name}
        id={name}
        labelId={labelId}
        onBlur={onBlur}
        value={value}
        variant="outlined"
        // onChange={customOnChange || onChange}
        onChange={onChange}
        fullWidth
        displayEmpty
        error={error}
        {...rest}
      >
        {placeholder && (
          <StyledMenuItem value="" disabled>
            <em>{placeholder}</em>
          </StyledMenuItem>
        )}
        {values.map(({ value, label }) => {
          if (value === 'disabled') {
            return (
              <StyledMenuItem disabled={true} key={value} value={value}>
                {label}
              </StyledMenuItem>
            )
          } else {
            return (
              <StyledMenuItem key={value} value={value}>
                {label}
              </StyledMenuItem>
            )
          }
        })}
      </Select>
      {helperText && (
        <FormHelperText error={error}>{helperText}</FormHelperText>
      )}
    </div>
  )
}

const StyledMenuItem = styled(MenuItem)`
  display: block;
  padding: 5px 10px;
`
