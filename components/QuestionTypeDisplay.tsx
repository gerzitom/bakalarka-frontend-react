import React, { FC } from 'react'
import { QuestionType } from '../types/entities'
import RadioButtonCheckedIcon from '@mui/icons-material/RadioButtonChecked'
import CheckBoxIcon from '@mui/icons-material/CheckBox'
import { Box } from '@mui/system'
import BallotIcon from '@mui/icons-material/Ballot'
import CropSquareIcon from '@mui/icons-material/CropSquare'
import { PrefferedType } from '../types/template.type'

type Props = {
  questionType: PrefferedType
}
export const QuestionTypeDisplay: FC<Props> = ({ questionType }) => {
  const containerProps = {
    sx: {
      display: 'flex',
      alignItems: 'center',
      columnGap: '5px',
    },
  }
  const types = {
    ONE_OPTION: (
      <Box {...containerProps}>
        <RadioButtonCheckedIcon fontSize={'small'} />
        <span>Výběr jedné odpovědi</span>
      </Box>
    ),
    MULTIPLE_OPTION: (
      <Box {...containerProps}>
        <CheckBoxIcon fontSize={'small'} />
        <span>Výběr více odpovědí</span>
      </Box>
    ),
    SPOJOVACKA: (
      <Box {...containerProps}>
        <BallotIcon fontSize={'small'} />
        <span>Spojení slov</span>
      </Box>
    ),
    TEXT_AREA: (
      <Box {...containerProps}>
        <CropSquareIcon fontSize={'small'} />
        <span>Otevřená odpověď</span>
      </Box>
    ),
  }
  if (questionType in types) return types[questionType]
  else return <span>Neznámý typ odpovědi</span>
}
