import React, { FC } from 'react'
import styled from 'styled-components'

type Props = {}
export const MyChip: FC<Props> = ({ children }) => {
  return <StyledContainer>{children}</StyledContainer>
}

const StyledContainer = styled.div`
  background: #18494f;
  border-radius: 20px;
  padding: 7px;
  display: inline-flex;
  align-items: center;
  color: white;
  column-gap: 5px;
  font-size: 15px;
  font-weight: bold;
  * {
    line-height: 1;
  }
`
