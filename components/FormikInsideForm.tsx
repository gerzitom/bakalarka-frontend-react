import React, { FC, FormEventHandler } from 'react'
import { FormikProps } from 'formik'

/**
 * This form component should be put inside Formik component
 * to increase usability.
 * Features.
 *  - handle submit on enter pressed
 */
type Props = {
  formikProps: FormikProps<any>
}
export const FormikInsideForm: FC<Props> = ({ children, formikProps }) => {
  const { handleSubmit } = formikProps
  const handleNativeSubmit: FormEventHandler<HTMLFormElement> = (e) => {
    e.preventDefault()
    handleSubmit()
  }
  return (
    <form
      onSubmit={handleNativeSubmit}
      onKeyDown={(e) => {
        if (e.key === 'Enter') {
          handleSubmit()
        }
      }}
    >
      {children}
    </form>
  )
}
