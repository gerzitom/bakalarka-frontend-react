import React, { FC } from 'react'
import { Button, Typography } from '@mui/material'
import { ModalWrapper, useModalState } from './ModalWrapper'
import Grid from '@mui/material/Grid'

type Props = {
  buttonText: string
  text: string
  sureDelete: () => void
}
export const DeleteModal: FC<Props> = ({ buttonText, text, sureDelete }) => {
  const { isModalOpen, closeModal, openModal } = useModalState(false)
  return (
    <>
      <Button color={'error'} sx={{ mt: 2 }} onClick={openModal} fullWidth>
        {buttonText}
      </Button>
      <ModalWrapper isOpen={isModalOpen} closeModal={closeModal}>
        <Typography variant={'h2'} sx={{ textAlign: 'center' }}>
          Smazat?
        </Typography>
        <Typography variant={'subtitle1'} sx={{ textAlign: 'center' }}>
          {text}
        </Typography>
        <Grid container spacing={4} sx={{ pt: 3 }}>
          <Grid item xs={6}>
            <Button
              variant={'outlined'}
              size={'large'}
              fullWidth
              onClick={closeModal}
            >
              NE
            </Button>
          </Grid>
          <Grid item xs={6}>
            <Button
              variant={'contained'}
              color={'primary'}
              size={'large'}
              sx={{ mr: 2 }}
              fullWidth
              onClick={sureDelete}
            >
              ANO
            </Button>
          </Grid>
        </Grid>
      </ModalWrapper>
    </>
  )
}
