import React, { FC } from 'react'
import Gravatar from 'react-gravatar'
import styled from 'styled-components'

type Props = {
  avatarId: string
}
export const UserAvatar: FC<Props> = ({ avatarId }) => {
  return (
    <GravatarContainer>
      <Gravatar
        email={avatarId ?? ''}
        size={40}
        rating="pg"
        default={'mp'}
        className="CustomAvatar-image"
        protocol="https://"
      />
    </GravatarContainer>
  )
}

const GravatarContainer = styled.div`
  border-radius: 50%;
  overflow: hidden;
  width: 40px;
  height: 40px;
  margin-right: 10px;
`
