export type TestState = 'before' | 'init' | 'started' | 'finished'
export type Result = 'correct' | 'bad' | 'normal' | 'test'
