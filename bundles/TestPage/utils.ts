import { FormikValues } from 'formik'
import {
  QuestionGroup,
  StudentTestSummary,
  Test,
  TestAnswers,
  TestQuestionGroup,
  UserAnswers,
} from '../../types/entities'
import TestService from '../../utils/api/TestService'
import { TestState } from './types'
import * as Sentry from '@sentry/browser'

export const LOCAL_STORAGE_ACTIVE_STEP = 'test_active_step'

/**
 * Create initial values for formik form for student test
 * @param questions question Groups to be stored in formik props
 * @return {FormikValues} object for setting up initial values in Formik form.
 */
export const setInitValues = (
  questions: QuestionGroup[],
  answers: UserAnswers[],
): FormikValues => {
  return questions.reduce((acc, question) => {
    const groupAnswers = answers.find(
      (answer) => answer.questionGroupId === question.id,
    )
    if (question.groupType === 'SIMPLE') {
      handleSimpleQuestion(question, acc, groupAnswers)
    } else if (question.groupType === 'SPOJOVACKA') {
      handleSpojovacka(question, acc, answers)
    } else if (question.groupType === 'OPEN') {
      handleOpenAnswer(question, acc)
    } else {
      console.warn('Unsupported question type at: ', question)
      Sentry.captureMessage(
        `Unsupported question group type appeared with ID: ${question.id} with type: ${question.groupType}!`,
      )
    }
    return acc
  }, {} as FormikValues)
}

const handleSimpleQuestion = (
  questionGroup: QuestionGroup,
  acc: FormikValues,
  answers?: UserAnswers,
) => {
  const question = questionGroup.questions[0]
  if (question.answerType === 'MULTIPLE_OPTIONS') {
    acc[question.id] = getMultipleOptionAnswer(answers)
  } else {
    acc[question.id] = getSingleOptionAnswer(answers)
  }
}

/**
 * Parsing multiple options answer returned from BE
 * @param answers
 */
const getMultipleOptionAnswer = (answers?: UserAnswers) => {
  const answer = answers?.userAnswersQuestionsDtos[0]
  if (answer?.possibleAnswers && answer?.possibleAnswers.length > 0) {
    return answer?.possibleAnswers.map((posAnswer) => String(posAnswer.id))
  }
  return []
}

/**
 * Parsing single option answer returned from BE
 * @param answers
 */
const getSingleOptionAnswer = (answers?: UserAnswers) => {
  const answer = answers?.userAnswersQuestionsDtos[0]
  if (answer?.possibleAnswers && answer?.possibleAnswers.length > 0) {
    return String(answer?.possibleAnswers[0].id)
  }
  return ''
}

const handleSpojovacka = (
  questionGroup: QuestionGroup,
  acc: FormikValues,
  answers: UserAnswers[],
) => {
  const groupAnswers = answers.find(
    (answer) => answer.questionGroupId === questionGroup.id,
  )
  questionGroup.questions.forEach((question) => {
    const questionAnswer = groupAnswers?.userAnswersQuestionsDtos.find(
      (answer) => answer.questionId === question.id,
    )
    if (questionAnswer) {
      acc[question.id] =
        questionAnswer.possibleAnswers.length === 1
          ? questionAnswer.possibleAnswers[0].id
          : ''
    } else {
      acc[question.id] = ''
    }
  })
}

const handleOpenAnswer = (questionGroup: QuestionGroup, acc: FormikValues) => {
  questionGroup.questions.forEach((question) => {
    acc[question.id] = ''
  })
}

/**
 * Save all questions separately in question group.
 * Used when student move from question.
 * @param testId id of the test
 * @param questionGroup
 * @param formikProps formik props from formik test form,
 *  where every question is stored separately with key of their ID.
 */
export const saveQuestionGroup = async (
  testId: number,
  questionGroup: TestQuestionGroup,
  formikProps: any,
) => {
  for (const question of questionGroup.questions) {
    if (questionGroup.groupType === 'OPEN') {
      await TestService.saveOpenAnswer(
        testId,
        question.id,
        formikProps.values[question.id],
      )
    } else {
      await TestService.saveQuestionAnswer(
        testId,
        question.id,
        formikProps.values[question.id],
      )
    }
  }
}

export const resolveTestState = (test: Test): TestState => {
  const userStarted = test.results[0].timeStarted
  const userEnded = test.results[0].timeFinished
  if (userStarted && !userEnded) return 'started'
  if (userEnded) return 'finished'
  const userStartedDate = userStarted ? new Date(userStarted) : null
  const startDate = new Date(test.startDate)
  const endDate = new Date(test.endDate)
  const now = new Date()
  if (endDate < now) return 'finished'
  else if (startDate > now) return 'before'
  else if (startDate < now && now < endDate && userStartedDate) return 'init'
  return 'init'
}

export const findAnswers = (
  answers: { possibleAnswers: any[]; questionId: number }[],
  questionId: number,
) => answers.find((answer) => answer.questionId === questionId)!.possibleAnswers
