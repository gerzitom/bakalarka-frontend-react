import React, { FC, useMemo, useState } from 'react'
import { Test, TestAnswers, UserAnswers } from '../../types/entities'
import { Formik } from 'formik'
import { FormikSubmitHandler } from '../../types'
import { StudentTest } from './components/StudentTest'
import { TestFinished } from './components/TestFinished'
import { BeforeTest } from './components/BeforeTest'
import TestService from '../../utils/api/TestService'
import { TestContext } from './components/TestContext'
import { resolveTestState, setInitValues } from './utils'
import { TestState } from './types'
import { Breadcrumbs, Typography } from '@mui/material'
import Link from 'next/link'
import { TestNotStarted } from './components/TestNotStarted'
import { useNotifications } from '../../utils/useNotifications'

type Props = {
  test: Test
  answers: UserAnswers[]
}
export const TestPageBundle: FC<Props> = ({ test, answers }) => {
  const { questionGroups, id } = test
  const defaultTestState = resolveTestState(test)
  const [testState, setTestState] = useState<TestState>(defaultTestState)
  const initVals = useMemo(
    () => setInitValues(questionGroups, answers),
    [questionGroups],
  )

  const handleSubmit: FormikSubmitHandler<any> = async (values) => {
    await TestService.endTest(id)
    setTestState('finished')
  }

  const startTest = async () => {
    await TestService.startTest(id)
    setTestState('started')
  }
  return (
    <TestContext.Provider value={{ test, answers }}>
      <Breadcrumbs>
        <Link href={'/app'}>Profil studenta</Link>
        <Typography color="text.primary">Test: {test.name}</Typography>
      </Breadcrumbs>
      <Formik initialValues={initVals} onSubmit={handleSubmit}>
        {(formikProps) => (
          <>
            {testState === 'before' && (
              <TestNotStarted testStarted={startTest} />
            )}
            {testState === 'init' && (
              <BeforeTest test={test} startTest={startTest} />
            )}
            {testState === 'started' && (
              <StudentTest
                questions={questionGroups ?? []}
                formikProps={formikProps}
              />
            )}
            {testState === 'finished' && (
              <TestFinished
                answers={formikProps.values}
                questions={questionGroups ?? []}
              />
            )}
          </>
        )}
      </Formik>
    </TestContext.Provider>
  )
}
