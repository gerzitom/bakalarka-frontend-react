import React, { FC, useEffect } from 'react'
import { useTimer } from 'react-timer-hook'
import { Paper } from '@mui/material'
import styled from 'styled-components'

type Props = {
  endTime: Date
  onFinished: () => void
}
export const TestTimer: FC<Props> = ({ endTime, onFinished }) => {
  const { start, minutes, seconds, hours } = useTimer({
    expiryTimestamp: endTime,
    onExpire: onFinished,
  })
  useEffect(() => {
    start()
  }, [])
  return (
    <StyledTimer sx={{ p: 1 }} elevation={0}>
      <span>
        {hours}:{minutes}:{seconds}
      </span>
    </StyledTimer>
  )
}

const StyledTimer = styled(Paper)`
  font-family: 'Bebas Neue', serif;
  font-size: 2.5em;
  text-align: center;
`
