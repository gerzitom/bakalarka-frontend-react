import React, { FC } from 'react'
import { FormControlLabel, Radio, RadioGroup } from '@mui/material'
import { Option } from '../../../../types/entities'
import styled from 'styled-components'
import { Result } from '../../types'

type Props = {
  options: Option[]
  rightAnswers: Option[]
  userAnswers: Option[]
}
export const SingleOption: FC<Props> = ({
  options,
  rightAnswers,
  userAnswers,
}) => {
  const isUserAnswer = (optionId: number) =>
    !!userAnswers.find((answer) => answer.id === optionId)
  const result = (optionId: number): Result => {
    const userAnswer = isUserAnswer(optionId)
    const right = rightAnswers.find((answer) => answer.id === optionId)
    if (userAnswer && right) return 'correct'
    else if (userAnswer) return 'bad'
    else if (right) return 'bad'
    return 'normal'
  }

  return (
    <RadioGroup>
      {options.map((option) => {
        return (
          <StyledLabel
            key={option.id}
            control={<Radio />}
            label={option.singleOption.text}
            checked={isUserAnswer(option.id)}
            disabled={true}
            result={result(option.id)}
          />
        )
      })}
    </RadioGroup>
  )
}

interface ResultColor {
  [key: string]: string
}

const getResultColor = (result: string): string => {
  const ret: ResultColor = {
    correct: 'green',
    bad: 'red',
    normal: 'grey',
    test: '',
  }
  return ret[result]
}

const StyledLabel = styled(FormControlLabel)<{ result: Result }>`
  span {
    color: ${(props) => getResultColor(props.result)}!important;
  }
`
