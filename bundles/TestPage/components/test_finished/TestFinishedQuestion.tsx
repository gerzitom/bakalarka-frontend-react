import React, { FC } from 'react'
import { TestQuestionGroup } from '../../../../types/entities'
import { Divider } from '@mui/material'
import { TestFinishedQuestionGroup } from './TestFinishedQuestionGroup'

type Props = {
  question: TestQuestionGroup
  userAnswers: { possibleAnswers: any[]; questionId: number }[]
  rightAnswers: { possibleAnswers: any[]; questionId: number }[]
}
export const TestFinishedQuestion: FC<Props> = ({
  question,
  userAnswers,
  rightAnswers,
}) => {
  return (
    <>
      <div>{question.instructions}</div>
      <Divider className={'pt-4'} sx={{ mt: 2 }} />
      <TestFinishedQuestionGroup
        question={question}
        rightAnswers={rightAnswers}
        userAnswers={userAnswers}
      />
    </>
  )
}
