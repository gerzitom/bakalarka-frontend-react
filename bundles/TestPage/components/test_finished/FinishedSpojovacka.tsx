import React, { FC, useCallback, useMemo } from 'react'
import { Option, Question, TestQuestionGroup } from '../../../../types/entities'
import { Box } from '@mui/system'
import {
  BinContainer,
  Dustbin,
} from '../../../../components/Spojovacka/components/Dustbin'
import { QuestionRow } from '../../../../components/Spojovacka/Spojovacka'
import styled from 'styled-components'
import { inspect } from 'util'
import { colors, Typography } from '@mui/material'
import { findAnswers } from '../../utils'

type Props = {
  question: TestQuestionGroup
  userAnswers: { possibleAnswers: any[]; questionId: number }[]
  rightAnswers: { possibleAnswers: any[]; questionId: number }[]
}
export const FinishedSpojovacka: FC<Props> = ({
  question,
  userAnswers,
  rightAnswers,
}) => {
  const { questions } = question
  return (
    <Box sx={{ mt: 3 }}>
      {questions.map((question, index) => (
        <QuestionLine
          key={question.id}
          question={question}
          userAnswers={userAnswers}
          rightAnswers={rightAnswers}
        />
      ))}
    </Box>
  )
}

type QuestionLineProps = {
  question: Question
  userAnswers: { possibleAnswers: any[]; questionId: number }[]
  rightAnswers: { possibleAnswers: any[]; questionId: number }[]
}
const QuestionLine: FC<QuestionLineProps> = ({
  question,
  userAnswers,
  rightAnswers,
}) => {
  const computedUserAnswers = findAnswers(userAnswers, question.id)
  const computedRightAnswers = findAnswers(rightAnswers, question.id)
  const computeState = (): SpojovackaState => {
    const correctAnswerId = computedRightAnswers[0].id
    const userAnswerId = computedUserAnswers[0]?.id ?? null
    if (correctAnswerId === userAnswerId) return 'correct'
    else return 'error'
  }
  const state: SpojovackaState = computeState()
  return (
    <QuestionRow>
      <span>{question.content}</span>
      <StyledAnswerContainer active={true} state={state}>
        <span>{computedUserAnswers[0]?.singleOption.text ?? ''}</span>
      </StyledAnswerContainer>
      {state === 'error' && (
        <Typography color={colors.red[500]}>
          {computedRightAnswers[0].singleOption.text}
        </Typography>
      )}
    </QuestionRow>
  )
}

type SpojovackaState = 'correct' | 'error' | 'normal'
const StyledAnswerContainer = styled(BinContainer)<{ state: SpojovackaState }>`
  border-color: ${(props) =>
    props.state === 'correct' ? colors.green[400] : colors.red[300]};
  border-width: 2px;
  justify-content: center;
`
