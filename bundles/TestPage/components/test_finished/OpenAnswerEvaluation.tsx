import React, { FC, useState } from 'react'
import { useTeacherPreviewContext } from '../../../../pages/app/test_preview/[testId]/user/[userId]'
import { Box, Button, Slider } from '@mui/material'
import TestService from '../../../../utils/api/TestService'
import * as Sentry from '@sentry/browser'
import { useNotifications } from '../../../../utils/useNotifications'
import { OpenAnswer, Question } from '../../../../types/entities'

type Props = {
  question: Question
  userAnswer: OpenAnswer
  evaluated: () => void
}
export const OpenAnswerEvaluation: FC<Props> = ({
  question,
  userAnswer,
  evaluated,
}) => {
  const { successNotification, errorNotification } = useNotifications()
  const [points, setPoints] = useState(0)

  const evaluate = () => {
    TestService.evaluateOpenAnswer(userAnswer.id, points)
      .then(() => {
        successNotification('Otázka ohodnocena')
        evaluated()
      })
      .catch((err) => {
        errorNotification('Otázku jste už hodnotili.')
        Sentry.captureException(err)
      })
  }
  return (
    <Box sx={{ px: 3, mt: 3 }}>
      <Slider
        value={points}
        onChange={(ev, value) => setPoints(Number(value))}
        min={0}
        max={question.points}
        valueLabelDisplay={'auto'}
        marks={[
          { value: 0, label: '0 bodů' },
          { value: question.points, label: `${question.points} bodů` },
        ]}
      />
      <Button onClick={evaluate}>Ohodnotit</Button>
    </Box>
  )
}
