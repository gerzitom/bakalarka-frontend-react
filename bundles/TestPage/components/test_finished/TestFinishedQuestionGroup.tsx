import React, { FC, useCallback } from 'react'
import { Spojovacka } from '../../../../components/Spojovacka/Spojovacka'
import { TestQuestionGroup } from '../../../../types/entities'
import { SingleOption } from './SingleOption'
import { MultipleOption } from './MultipleOption'
import { Alert } from '@mui/material'
import { FinishedSpojovacka } from './FinishedSpojovacka'
import { findAnswers } from '../../utils'
import { FinishedOpenAnswer } from './FinishedOpenAnswer'

type Props = {
  question: TestQuestionGroup
  userAnswers: { possibleAnswers: any[]; questionId: number }[]
  rightAnswers: { possibleAnswers: any[]; questionId: number }[]
}
export const TestFinishedQuestionGroup: FC<Props> = ({
  question,
  userAnswers,
  rightAnswers,
}) => {
  const getUserAnswers = useCallback(
    (questionId: number) => findAnswers(userAnswers, questionId),
    [],
  )
  const getRightAnswers = useCallback(
    (questionId: number) => findAnswers(rightAnswers, questionId),
    [rightAnswers],
  )
  if (question.groupType === 'SIMPLE') {
    return (
      <>
        {question.questions.map((simpleQuestion, index) => {
          if (simpleQuestion.answerType === 'ONE_OPTION')
            return (
              <SingleOption
                options={simpleQuestion.possibleAnswers}
                userAnswers={getUserAnswers(simpleQuestion.id)}
                rightAnswers={getRightAnswers(simpleQuestion.id)}
                key={simpleQuestion.id}
              />
            )
          if (simpleQuestion.answerType === 'MULTIPLE_OPTIONS')
            return (
              <MultipleOption
                options={simpleQuestion.possibleAnswers}
                userAnswers={getUserAnswers(simpleQuestion.id)}
                rightAnswers={getRightAnswers(simpleQuestion.id)}
                key={simpleQuestion.id}
              />
            )
        })}
      </>
    )
  } else if (question.groupType === 'SPOJOVACKA') {
    return (
      <>
        <FinishedSpojovacka
          question={question}
          rightAnswers={rightAnswers}
          userAnswers={userAnswers}
        />
      </>
    )
  } else if (question.groupType === 'OPEN') {
    return (
      <>
        {question.questions.map((question) => (
          <FinishedOpenAnswer key={question.id} question={question} />
        ))}
      </>
    )
  }
  return <Alert severity={'error'}>Nepodporovaný typ otázky</Alert>
}
