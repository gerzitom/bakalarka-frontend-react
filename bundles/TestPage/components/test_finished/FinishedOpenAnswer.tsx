import React, { FC, useEffect, useState } from 'react'
import { OpenAnswer, Question } from '../../../../types/entities'
import { Alert, Box, Button, Slider, TextField } from '@mui/material'
import TestService from '../../../../utils/api/TestService'
import * as Sentry from '@sentry/browser'
import { useNotifications } from '../../../../utils/useNotifications'
import { useTeacherPreviewContext } from '../../../../pages/app/test_preview/[testId]/user/[userId]'
import { OpenAnswerEvaluation } from './OpenAnswerEvaluation'

type Props = {
  question: Question
}
export const FinishedOpenAnswer: FC<Props> = ({ question }) => {
  const teacherContext = useTeacherPreviewContext()
  const [userAnswer, setUserAnswer] = useState<OpenAnswer | undefined>(
    undefined,
  )
  useEffect(() => {
    fetchOpenAnswer()
  }, [])

  const fetchOpenAnswer = () => {
    TestService.getOpenAnswer(question.id).then((response) => {
      if (response.length > 0) {
        setUserAnswer(response[0])
      }
    })
  }

  const refetch = () => {
    fetchOpenAnswer()
    teacherContext?.refetch()
  }

  return (
    <>
      <TextField
        multiline
        fullWidth
        disabled
        defaultValue={userAnswer?.answer}
        sx={{ my: 3 }}
      />
      {userAnswer?.marked && <Alert>Odnoceno</Alert>}
      {!userAnswer?.marked && !teacherContext && (
        <Alert severity={'warning'}>Čekání na schválení učitele</Alert>
      )}
      {teacherContext && !userAnswer?.marked && (
        <OpenAnswerEvaluation
          question={question}
          userAnswer={userAnswer!}
          evaluated={refetch}
        />
      )}
    </>
  )
}
