import { createContext, useContext } from 'react'
import { Test, TestAnswers, UserAnswers } from '../../../types/entities'

type TestContextType = {
  test: Test
  answers: UserAnswers[]
}
// @ts-ignore
export const TestContext = createContext<TestContextType>({})
export const useTestContext = () => {
  const context = useContext(TestContext)

  return {
    test: context.test,
    answers: context.answers,
    ...context.test,
  }
}
