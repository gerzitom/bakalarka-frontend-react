import React, { FC, useMemo, useState } from 'react'
import { Box, Button, Grid, Paper, Typography } from '@mui/material'
import { TestQuestion } from './TestQuestion'
import { FormikProps } from 'formik'
import { QuestionStep } from './QuestionStep'
import styled from 'styled-components'
import { TestQuestionGroup } from '../../../types/entities'
import { TestTimer } from './TestTimer'
import { useTestContext } from './TestContext'
import ArrowForwardIosIcon from '@mui/icons-material/ArrowForwardIos'
import ArrowBackIosIcon from '@mui/icons-material/ArrowBackIos'
import { ModalWrapper, useModalState } from '../../../components/ModalWrapper'
import testService from '../../../utils/api/TestService'
import { LOCAL_STORAGE_ACTIVE_STEP, saveQuestionGroup } from '../utils'
import { useGlobalLoading } from '../../../components/GlobalLoader'
import { useNotifications } from '../../../utils/useNotifications'
import { computeMinutesToEnd, computeTestRealEnd } from '../../../utils'

type Props = {
  questions: TestQuestionGroup[]
  formikProps: FormikProps<any>
}
export const StudentTest: FC<Props> = ({ questions, formikProps }) => {
  const { test } = useTestContext()
  const { successNotification } = useNotifications()
  const endDate = useMemo(() => computeTestRealEnd(test), [test])
  const { startGlobalLoading, stopGlobalLoading } = useGlobalLoading()
  const cachedActiveStep = localStorage.getItem(LOCAL_STORAGE_ACTIVE_STEP)
  const [activeStep, setActiveStep] = useState(Number(cachedActiveStep) ?? 0)
  const [visited, setVisited] = useState<{
    [k: number]: boolean
  }>({})
  const { isModalOpen, openModal, closeModal } = useModalState(false)
  const { errorNotification } = useNotifications()

  const totalSteps = () => {
    return questions.length
  }

  const isLastStep = () => {
    return activeStep === totalSteps() - 1
  }

  const handleQuestionMove = (newStep: number) => {
    handleVisited()
    saveCurrentValue().then(() => successNotification())
    updateStepCache(newStep)
    setActiveStep(newStep)
  }

  const handleNext = () => {
    const newActiveStep = isLastStep()
      ? questions.findIndex((step, i) => !(i in visited))
      : activeStep + 1
    handleQuestionMove(newActiveStep)
  }

  const handleBack = () => {
    handleQuestionMove(activeStep - 1)
  }

  const handleVisited = () => {
    const newCompleted = visited
    newCompleted[activeStep] = true
    setVisited(newCompleted)
  }

  const handleStep = (step: number) => () => {
    handleQuestionMove(step)
    handleVisited()
    updateStepCache(step)
    setActiveStep(step)
  }

  const updateStepCache = (step: number) => {
    localStorage.setItem(LOCAL_STORAGE_ACTIVE_STEP, String(step))
  }

  const handleCompleteTest = async () => {
    await saveCurrentValue()
    openModal()
  }
  const handleComplete = async () => formikProps.handleSubmit()

  const saveCurrentValue = async () => {
    const activeQuestion = questions[activeStep]
    startGlobalLoading()
    saveQuestionGroup(test.id, activeQuestion, formikProps)
      .catch(() => {
        errorNotification(
          'Test už skončil. Budete přesměrováni na vyhodnocení.',
        )
        handleComplete()
      })
      .finally(stopGlobalLoading)
  }

  return (
    <Box sx={{ width: '100%', mt: 5 }}>
      <Grid container spacing={4}>
        <Grid item xs={8}>
          <Grid
            container
            justifyContent={'space-between'}
            alignItems={'center'}
          >
            <Grid item>
              <Typography variant={'h1'}>{test.name}</Typography>
            </Grid>
            <Grid item xs={3}>
              <TestTimer endTime={endDate} onFinished={handleComplete} />
            </Grid>
          </Grid>
          <Paper elevation={0} sx={{ p: 4, mt: 3 }}>
            {questions.map((question, index) => (
              <TestQuestion
                question={question}
                key={index}
                visible={index === activeStep}
              />
            ))}
          </Paper>
          <Box sx={{ display: 'flex', flexDirection: 'row', pt: 2 }}>
            <Paper sx={{ p: 1 }} elevation={0}>
              <Button
                color="inherit"
                disabled={activeStep === 0}
                onClick={handleBack}
                sx={{ mr: 1 }}
                startIcon={<ArrowBackIosIcon />}
              >
                Předchozí otázka
              </Button>
            </Paper>
            <Box sx={{ flex: '1 1 auto' }} />
            <Paper sx={{ p: 1 }} elevation={0}>
              <Button
                fullWidth
                disabled={activeStep === questions.length - 1}
                onClick={handleNext}
                sx={{ mr: 1 }}
                color="inherit"
                endIcon={<ArrowForwardIosIcon />}
              >
                Další otázka
              </Button>
            </Paper>
          </Box>
        </Grid>
        <Grid item xs={4}>
          <Paper elevation={0} sx={{ p: 4 }}>
            <Typography variant={'overline'}>Otázky</Typography>
            <QuestionStepsContainer>
              {questions.map((question, index) => (
                <QuestionStep
                  key={question.id}
                  question={question}
                  active={activeStep}
                  index={index}
                  visited={visited[index] ?? false}
                  clicked={handleStep(index)}
                />
              ))}
            </QuestionStepsContainer>
            <Button
              variant={'contained'}
              fullWidth
              sx={{ mt: 3 }}
              onClick={handleCompleteTest}
            >
              Ukončit test
            </Button>
            <ModalWrapper isOpen={isModalOpen} closeModal={closeModal}>
              <Typography variant={'h2'} sx={{ mb: 3 }}>
                Opravdu si přejete ukončit test?
              </Typography>
              <Button
                variant={'contained'}
                color={'primary'}
                size={'large'}
                sx={{ mr: 2 }}
                onClick={handleComplete}
              >
                Ukončit test
              </Button>
              <Button variant={'outlined'} size={'large'} onClick={closeModal}>
                Zpět
              </Button>
            </ModalWrapper>
          </Paper>
        </Grid>
      </Grid>
    </Box>
  )
}

const QuestionStepsContainer = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr 1fr 1fr;
  grid-gap: 15px;
`
