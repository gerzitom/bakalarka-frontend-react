import React, { FC } from 'react'
import { Box, Divider } from '@mui/material'
import { TestQuestionGroup } from '../../../types/entities'
import { TestQuestionContainer } from './TestQuestionContainer'

type Props = {
  question: TestQuestionGroup
  answer?: any
  disabled?: boolean
  rightAnswers?: any
  visible: boolean
}
export const TestQuestion: FC<Props> = ({
  question,
  answer,
  disabled,
  rightAnswers,
  visible,
}) => {
  return (
    <Box sx={{ display: visible ? 'block' : 'none' }}>
      <div>{question.instructions}</div>
      <Divider className={'pt-4'} sx={{ mt: 2 }} />
      <TestQuestionContainer
        question={question}
        answer={answer}
        disabled={disabled}
      />
    </Box>
  )
}
