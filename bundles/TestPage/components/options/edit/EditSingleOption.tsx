import React, { FC, useState } from 'react'
import {
  Alert,
  FormControlLabel,
  IconButton,
  Radio,
  RadioGroup,
  Typography,
} from '@mui/material'
import { AddNewOption } from './AddNewOption'
import { PossibleType } from '../../../../../types/entities'
import { Delete } from '@mui/icons-material'
import styled from 'styled-components'
import QuestionService from '../../../../../utils/api/QuestionService'
import { useNotifications } from '../../../../../utils/useNotifications'
import * as Sentry from '@sentry/browser'
import { useQueryClient } from 'react-query/react'
import { useTemplateContext } from '../../../../EditTemplateBundle/utils'

type Props = {
  options: PossibleType[]
  deleteOption: (optionId: number) => void
}
export const EditSingleOption: FC<Props> = ({ options, deleteOption }) => {
  const queryClient = useQueryClient()

  const findCorrectOption = (options: PossibleType[]) => {
    const correctOption = options.find((option) => option.correct)
    return correctOption?.id ?? ''
  }

  const [correct, setCorrect] = useState(findCorrectOption(options))

  const handleChange = async (
    event: React.ChangeEvent<HTMLInputElement>,
    value: string,
  ) => {
    if (correct) await QuestionService.updateOption(Number(correct), false)
    await QuestionService.updateOption(Number(value), true)
    setCorrect(value)
    await queryClient.refetchQueries()
  }

  return (
    <RadioGroup name={'rightAnswer'} onChange={handleChange} value={correct}>
      {options.map((option, index) => (
        <OptionLine key={index}>
          <FormControlLabel
            value={option.id}
            control={<Radio />}
            defaultChecked={option.correct}
            label={option.singleOption.text ?? ''}
          />
          <IconButton
            className={'delete'}
            size={'small'}
            onClick={() => deleteOption(option.id)}
          >
            <Delete fontSize={'small'} />
          </IconButton>
        </OptionLine>
      ))}
    </RadioGroup>
  )
}

export const OptionLine = styled.div`
  .delete {
    transition: 300ms;
    opacity: 0;
  }
  &:hover .delete {
    opacity: 1;
  }
`
