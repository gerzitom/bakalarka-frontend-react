import React, { FC, useCallback, useEffect, useRef, useState } from 'react'
import { FormikSubmitHandler } from '../../../../../types'
import { Field, Formik } from 'formik'
import { Option, PossibleType } from '../../../../../types/entities'
import { FormikInsideForm } from '../../../../../components/FormikInsideForm'
import { useMutation } from 'react-query/react'
import { queryClient, request } from '../../../../../utils/ReactQueryClient'
import { LoadingButton } from '@mui/lab'
import * as Yup from 'yup'
import { useTemplateContext } from '../../../../EditTemplateBundle/utils'
import { useNotifications } from '../../../../../utils/useNotifications'
import TestTemplateService from '../../../../../utils/api/TestTemplateService'
import * as Sentry from '@sentry/browser'
import { TextField } from '@mui/material'

/**
 * Component for creating new option to options questions.
 * Has its own mutation to create.
 */
type FormikVals = {
  newOption: string
}
type Props = {
  questionId: number
  options: PossibleType[]
}
export const AddNewOption: FC<Props> = ({ questionId, options }) => {
  const [loading, setLoading] = useState(false)
  const { errorNotification } = useNotifications()
  const { template, refresh } = useTemplateContext()
  const input = useRef<HTMLInputElement>()
  const initVals: FormikVals = {
    newOption: '',
  }

  useEffect(() => {}, [])

  const isUnique = useCallback(
    (newOption: string): boolean => {
      const found = options.find(
        (option) => option.singleOption.text === newOption,
      )
      return !found
    },
    [options],
  )

  const handleSubmit: FormikSubmitHandler<FormikVals> = (
    values,
    { resetForm, setFieldError },
  ) => {
    setLoading(true)
    const unique = isUnique(values.newOption)
    if (!unique) {
      resetForm()
      setFieldError('newOption', 'Tato možnost už existuje')
      errorNotification('Tato možnost už existuje')
      setLoading(false)
      return
    }

    TestTemplateService.addNewOption(questionId, { text: values.newOption })
      .then(() => {
        refresh().then(() => {
          resetForm()
          input.current?.focus()
        })
      })
      .catch((err) => Sentry.captureException(err))
      .finally(() => setLoading(false))
  }
  const Schema = Yup.object().shape({
    newOption: Yup.string().required('Jméno je povinné'),
  })
  return (
    <Formik
      initialValues={initVals}
      validationSchema={Schema}
      onSubmit={handleSubmit}
    >
      {(formikProps) => (
        <FormikInsideForm formikProps={formikProps}>
          {/*<input type="text" ref={input}/>*/}
          <TextField
            name={'newOption'}
            value={formikProps.values.newOption}
            onChange={formikProps.handleChange}
            helperText={formikProps.errors.newOption}
            error={!!formikProps.errors.newOption}
            variant={'standard'}
            sx={{ mt: 2 }}
            fullWidth
            autoFocus
            inputRef={input}
            // ref={input}
          />
          <LoadingButton
            loading={loading}
            fullWidth
            disabled={formikProps.values.newOption.length === 0}
            type={'submit'}
          >
            Přidat novou možnost
          </LoadingButton>
        </FormikInsideForm>
      )}
    </Formik>
  )
}
