import React, { ChangeEvent, FC } from 'react'
import {
  Alert,
  Checkbox,
  FormControl,
  FormControlLabel,
  FormGroup,
  IconButton,
  Typography,
} from '@mui/material'
import { PossibleType } from '../../../../../types/entities'
import { Formik } from 'formik'
import { FormikSubmitHandler } from '../../../../../types'
import { useNotifications } from '../../../../../utils/useNotifications'
import QuestionService from '../../../../../utils/api/QuestionService'
import { queryClient } from '../../../../../utils/ReactQueryClient'
import { AddNewOption } from './AddNewOption'
import * as Sentry from '@sentry/browser'
import { OptionLine } from './EditSingleOption'
import { Delete } from '@mui/icons-material'
import { useTemplateContext } from '../../../../EditTemplateBundle/utils'

type Props = {
  options: PossibleType[]
  deleteOption: (optionId: number) => void
}
export const EditMultipleOption: FC<Props> = ({ options, deleteOption }) => {
  const { refresh } = useTemplateContext()

  const handleChange = async (
    event: ChangeEvent<HTMLInputElement>,
    checked: boolean,
  ) => {
    await QuestionService.updateOption(Number(event.target.name), checked)
    await refresh()
  }
  return (
    <FormControl component="fieldset" style={{ display: 'flex' }}>
      <FormGroup>
        {options.map((option, index) => (
          <OptionLine key={option.id}>
            <FormControlLabel
              control={
                <Checkbox name={String(option.id)} onChange={handleChange} />
              }
              checked={option.correct}
              label={option.singleOption.text}
            />
            <IconButton
              className={'delete'}
              size={'small'}
              onClick={() => deleteOption(option.id)}
            >
              <Delete fontSize={'small'} />
            </IconButton>
          </OptionLine>
        ))}
      </FormGroup>
    </FormControl>
  )
}
