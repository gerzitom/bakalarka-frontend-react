import React, { FC } from 'react'
import { FormControl, FormControlLabel, FormGroup } from '@mui/material'
import { Field } from 'formik'
import { OptionsProps } from '../../../../types/entities'
import styled from 'styled-components'
import { CheckboxWithLabel } from 'formik-mui'

type Result = 'correct' | 'bad' | 'normal'
export const MultipleOption: FC<OptionsProps> = ({
  options,
  questionId,
  disabled,
  value,
  rightAnswers,
}) => {
  return (
    <FormControl component="fieldset" style={{ display: 'flex' }}>
      <FormGroup>
        {options.map((option, index) => (
          <Field
            type="checkbox"
            component={CheckboxWithLabel}
            name={questionId.toString()}
            key={option.id}
            value={option.id.toString()}
            Label={{ label: option.singleOption.text }}
            disabled={disabled}
          />
        ))}
      </FormGroup>
    </FormControl>
  )
}

interface ResultColor {
  [key: string]: string | undefined
}

const getResultColor = (result: string): string | undefined => {
  const ret: ResultColor = {
    correct: 'green',
    bad: 'red',
    normal: undefined,
  }
  return ret[result]
}

const StyledLabel = styled(FormControlLabel)<{ result: Result }>`
  span {
    color: ${(props) => getResultColor(props.result)}!important;
  }
`
