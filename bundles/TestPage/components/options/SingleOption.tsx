import React, { FC } from 'react'
import { FormControlLabel, Radio, RadioGroup } from '@mui/material'
import { useField } from 'formik'
import { OptionsProps } from '../../../../types/entities'
import styled from 'styled-components'

type Result = 'correct' | 'bad' | 'normal' | 'test'
export const SingleOption: FC<OptionsProps> = ({
  options,
  questionId,
  disabled,
  value,
  rightAnswers,
}) => {
  const [field] = useField(questionId.toString())
  const resolveState = (optionId: number): Result => {
    if (rightAnswers) {
      if (
        (optionId == value && rightAnswers.includes(Number(value))) ||
        rightAnswers.includes(Number(optionId))
      ) {
        return 'correct'
      }
      if (optionId == value && !rightAnswers.includes(Number(value))) {
        return 'bad'
      }
      return 'normal'
    }
    return 'test'
  }
  return (
    <RadioGroup {...field}>
      {options.map((option) => (
        <StyledLabel
          key={option.id}
          control={<Radio />}
          label={option.singleOption.text}
          value={option.id}
          disabled={disabled}
          result={resolveState(option.id)}
        />
      ))}
    </RadioGroup>
  )
}

interface ResultColor {
  [key: string]: string
}

const getResultColor = (result: string): string => {
  const ret: ResultColor = {
    correct: 'green',
    bad: 'red',
    normal: 'grey',
    test: '',
  }
  return ret[result]
}

const StyledLabel = styled(FormControlLabel)<{ result: Result }>`
  span {
    color: ${(props) => getResultColor(props.result)}!important;
  }
`
