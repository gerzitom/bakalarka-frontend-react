import React, { FC } from 'react'
import { useField } from 'formik'
import { Question } from '../../../../types/entities'
import { TextField } from '@mui/material'

type Props = {
  questionId: number
}
export const OpenAnswer: FC<Props> = ({ questionId }) => {
  const [field, meta, helper] = useField(questionId.toString())
  return (
    <TextField
      multiline
      fullWidth
      name={String(questionId)}
      value={field.value}
      onChange={(ev) => {
        helper.setValue(ev.target.value)
      }}
    />
  )
}
