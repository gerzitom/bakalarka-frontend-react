import React, { FC } from 'react'
import { Paper } from '@mui/material'
import { TestFinishedQuestion } from './test_finished/TestFinishedQuestion'
import { Test, TestAnswers } from '../../../types/entities'

type Props = {
  test: Test
  results: TestAnswers
}
export const StudentTestResults: FC<Props> = ({ test, results }) => {
  const { questionGroups } = test
  const userAnswers = (questionId: number) =>
    results!.userAnswers.find((result) => result.questionGroupId === questionId)
      .userAnswersQuestionsDtos
  const correctAnswers = (questionId: number) =>
    results!.rightAnswers.find(
      (result) => result.questionGroupId === questionId,
    ).rightAnswersQuestionsDtoList
  return (
    <>
      {questionGroups.map((question) => (
        <Paper
          sx={{ mb: 4, p: 3 }}
          key={question.id}
          component={'div'}
          elevation={0}
        >
          <TestFinishedQuestion
            question={question}
            userAnswers={userAnswers(question.id)}
            rightAnswers={correctAnswers(question.id)}
          />
        </Paper>
      ))}
    </>
  )
}
