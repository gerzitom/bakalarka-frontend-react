import React, { FC } from 'react'
import { TestQuestionGroup } from '../../../types/entities'
import { SingleOption } from './options/SingleOption'
import { MultipleOption } from './options/MultipleOption'
import { Alert, Paper, TextField } from '@mui/material'
import { Spojovacka } from '../../../components/Spojovacka/Spojovacka'
import { OpenAnswer } from './options/OpenAnswer'

type Props = {
  question: TestQuestionGroup
  answer?: any
  disabled?: boolean
}
export const TestQuestionContainer: FC<Props> = ({
  question,
  answer,
  disabled,
}) => {
  if (question.groupType === 'SIMPLE') {
    return (
      <>
        {question.questions.map((simpleQuestion, index) => {
          const props = {
            key: index,
            options: simpleQuestion.possibleAnswers,
            questionId: simpleQuestion.id,
            disabled: disabled,
            value: answer,
          }
          if (simpleQuestion.answerType === 'ONE_OPTION')
            return <SingleOption {...props} />
          else if (simpleQuestion.answerType === 'MULTIPLE_OPTIONS')
            return <MultipleOption {...props} />
          else
            return <Alert severity={'warning'}>Nepodporovaný typ otázky</Alert>
        })}
      </>
    )
  } else if (question.groupType === 'SPOJOVACKA') {
    return (
      <>
        <Spojovacka questionGroup={question} />
      </>
    )
  } else if (question.groupType === 'OPEN') {
    return (
      <>
        {question.questions.map((openAnswerQuestion) => (
          <OpenAnswer
            key={openAnswerQuestion.id}
            questionId={openAnswerQuestion.id}
          />
        ))}
      </>
    )
  }
  return <></>
}
