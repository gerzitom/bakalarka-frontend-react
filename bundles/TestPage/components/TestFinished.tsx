import React, { FC, useState } from 'react'
import { Alert, Button, Grid, Paper, Typography } from '@mui/material'
import { FormikValues } from 'formik'
import { TestAnswers, TestQuestionGroup } from '../../../types/entities'
import styled from 'styled-components'
import { useTestContext } from './TestContext'
import { useQuery } from 'react-query'
import { TestFinishedQuestion } from './test_finished/TestFinishedQuestion'
import { FullpageSpinner } from '../../../components/FullpageSpinner'
import { TestResultIndicator } from './TestResultIndicator'
import * as Sentry from '@sentry/browser'
import Link from 'next/link'
import { formatTime } from '../../../utils'

type Props = {
  questions: TestQuestionGroup[]
  answers: FormikValues
}
export const TestFinished: FC<Props> = ({ questions, answers }) => {
  const { test } = useTestContext()
  const [endingTest, setEndingTest] = useState(false)
  const { data: results, isLoading } = useQuery<TestAnswers>(
    `/tests/${test.id}/answers`,
  )

  if (isLoading || endingTest) return <FullpageSpinner />
  if (!results) {
    Sentry.captureMessage(`Error loading test results from test: ${test.id}`)
    return <Alert severity={'error'}>Výsledky se nepodařilo načíst!</Alert>
  }
  const userAnswers = (questionId: number) =>
    results!.userAnswers.find((result) => result.questionGroupId === questionId)
      .userAnswersQuestionsDtos
  const correctAnswers = (questionId: number) =>
    results!.rightAnswers.find(
      (result) => result.questionGroupId === questionId,
    ).rightAnswersQuestionsDtoList
  return (
    <>
      <Header>
        <Typography variant={'h3'} sx={{ mt: 10 }}>
          Dokončeno
        </Typography>
        <Typography variant={'h1'} sx={{ mt: 2, mb: 5 }}>
          {test.name}
        </Typography>
      </Header>
      <Typography variant={'h3'} sx={{ mb: 2 }}>
        Vaše odpovědi:
      </Typography>
      <Grid container spacing={4}>
        <Grid item xs={8}>
          {questions.map((question) => (
            <Paper
              sx={{ mb: 4, p: 3 }}
              key={question.id}
              component={'div'}
              elevation={0}
            >
              <TestFinishedQuestion
                question={question}
                userAnswers={userAnswers(question.id)}
                rightAnswers={correctAnswers(question.id)}
              />
            </Paper>
          ))}
        </Grid>
        <Grid item xs={4}>
          <Paper sx={{ p: 3 }}>
            <Typography variant={'h3'} sx={{ mb: 2 }}>
              Výsledek
            </Typography>
            <TestResultIndicator
              maxPoints={results!.testSummaryReadDto.maxPoints}
              totalPoints={results!.testSummaryReadDto.numberOfPoints}
            />
            <Grid sx={{ pt: 4 }} container>
              <Grid item xs={6}>
                <Typography variant={'overline'}>Začátek testu</Typography>
                <Typography variant={'h5'}>
                  {formatTime(results.testSummaryReadDto.timeStarted, true)}
                </Typography>
              </Grid>
              <Grid item xs={6}>
                <Typography variant={'overline'}>Konec testu</Typography>
                <Typography variant={'h5'}>
                  {formatTime(results.testSummaryReadDto.timeFinished, true)}
                </Typography>
              </Grid>
            </Grid>

            <Link href={'/app'}>
              <Button variant={'contained'} fullWidth sx={{ mt: 4 }}>
                Ukončit prohlídku
              </Button>
            </Link>
          </Paper>
        </Grid>
      </Grid>
    </>
  )
}

const Header = styled.div`
  text-align: center;
`
