import React, { FC } from 'react'
import { Typography } from '@mui/material'
import { Box } from '@mui/system'
import { formatTime } from '../../../utils'
import { useTestContext } from './TestContext'
import { TestTimer } from './TestTimer'
import { differenceInMinutes } from 'date-fns'

type Props = {
  testStarted: () => void
}
export const TestNotStarted: FC<Props> = ({ testStarted }) => {
  const { test } = useTestContext()
  const start = new Date(test.startDate)
  return (
    <Box sx={{ textAlign: 'center', mt: 8 }}>
      <Typography variant={'h1'} sx={{ mb: 3 }}>
        Tento test ještě nezačal!
      </Typography>
      <Typography variant={'overline'}>Začátek testu</Typography>
      <Typography variant={'h2'}>{formatTime(test.startDate)}</Typography>
      <TestTimer endTime={start} onFinished={testStarted} />
    </Box>
  )
}
