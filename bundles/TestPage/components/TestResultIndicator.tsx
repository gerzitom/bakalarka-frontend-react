import React, { FC } from 'react'
import styled from 'styled-components'

type Props = {
  totalPoints: number
  maxPoints: number
}
export const TestResultIndicator: FC<Props> = ({ totalPoints, maxPoints }) => {
  const progress = (totalPoints / maxPoints) * 100
  const color = progress > 50 ? '#21E202' : 'red'
  return (
    <StyledProgress progress={progress} color={color}>
      <span>
        {totalPoints}/{maxPoints}
      </span>
    </StyledProgress>
  )
}

export const StyledProgress = styled.div<{
  progress: number
  color: string
  small?: boolean
}>`
  padding: ${(props) => (props.small ? '7px' : '14px')};
  text-align: center;
  font-family: 'Bebas Neue';
  font-size: ${(props) => (props.small ? '16px' : '1.5em')};
  line-height: 1;
  position: relative;
  span {
    position: relative;
    z-index: 2;
  }
  &:before {
    content: '';
    background: rgba(89, 89, 109, 0.09);
    position: absolute;
    left: 0;
    top: 0;
    right: 0;
    bottom: 0;
    z-index: 0;
  }
  &:after {
    content: '';
    position: absolute;
    background: ${(props) => props.color};
    left: 0;
    top: 0;
    bottom: 0;
    width: ${(props) => props.progress}%;
    z-index: 1;
  }
`
