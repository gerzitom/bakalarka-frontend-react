import React, { FC } from 'react'
import { Button } from '@mui/material'
import styled from 'styled-components'
import { colors } from '../../../styles/colors'
import { useFormikContext } from 'formik'
import { TestQuestionGroup } from '../../../types/entities'

type Props = {
  index: number
  question: TestQuestionGroup
  visited: boolean
  active: number
  clicked: () => void
}
export const QuestionStep: FC<Props> = ({
  index,
  active,
  clicked,
  visited,
  question,
}) => {
  const { values } = useFormikContext()
  const isActive = active === index
  const computeAnswered = () => {
    const answered = question.questions.reduce((acc, question) => {
      // @ts-ignore
      const value = values[question.id]
      if (
        (value && !Array.isArray(value) && !!value) ||
        (value && Array.isArray(value) && value.length > 0)
      )
        acc++
      return acc
    }, 0)
    const total = answered / question.questions.length
    return total
  }
  const totalAnswered = computeAnswered()

  computeAnswered()
  const resolveState = () => {
    let state = ''
    if (visited) state += ' visited'
    if (isActive) state += ' active'
    return state
  }

  const className = resolveState()
  return (
    <StyledQuestionStep
      onClick={() => clicked()}
      variant={'text'}
      className={className}
      totalAnswered={totalAnswered}
    >
      <span>{index + 1}</span>
    </StyledQuestionStep>
  )
}

const StyledQuestionStep = styled(Button)<{ totalAnswered: number }>`
  transition: 300ms;
  position: relative;
  background: transparent;
  color: black !important;
  border: 1px solid transparent;
  span {
    position: relative;
    z-index: 1;
  }
  &::after {
    content: '';
    position: absolute;
    left: 0;
    right: 0;
    bottom: 0;
    background: ${colors.lightBackground};
    z-index: 0;
    transition: 300ms;
  }
  &.visited {
    border-color: ${colors.main};
    &::after {
      bottom: 0;
      height: ${(props) => props.totalAnswered * 100}%;
      background: ${colors.main};
    }
  }
  &.active {
    box-shadow: 0 3px 5px rgba(0, 0, 0, 0.2);
    transform: translateY(-3px);
    border-color: ${colors.main};
    &::after {
      background: ${colors.main};
      height: 100%;
    }
  }
`
