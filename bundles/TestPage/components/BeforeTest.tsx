import React, { FC, useState } from 'react'
import Link from 'next/link'
import { Test } from '../../../types/entities'
import {
  Alert,
  Button,
  Card,
  CardContent,
  Divider,
  Grid,
  Paper,
  Typography,
} from '@mui/material'
import { Box } from '@mui/system'
import { LOCAL_STORAGE_ACTIVE_STEP } from '../utils'
import * as Sentry from '@sentry/browser'

type Props = {
  test: Test
  startTest: () => Promise<void>
}
export const BeforeTest: FC<Props> = ({ test, startTest }) => {
  const [error, setError] = useState(false)
  const start = () => {
    localStorage.removeItem(LOCAL_STORAGE_ACTIVE_STEP)
    startTest().catch((err) => {
      setError(true)
      Sentry.captureException(err)
    })
  }
  return (
    <>
      <Box
        sx={{
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
        }}
      >
        <Typography variant={'h3'} sx={{ mt: 10 }}>
          začátek testu
        </Typography>
        <Typography variant={'h1'} sx={{ mt: 2, mb: 5 }}>
          {test.name}
        </Typography>

        {error && (
          <Alert severity={'error'} sx={{ mb: 6 }}>
            Omlouváme se, ale v současné době není možné tento test spustit.
            Zkusto to prosím za chvilku znovu.
          </Alert>
        )}

        <Card sx={{ p: 3, width: 400 }}>
          <Typography variant={'body1'}>
            Možný zisk bodů: <b>{test.results[0].maxPoints}</b>
          </Typography>
          <Grid
            container
            justifyContent={'space-between'}
            sx={{ mr: 3, mt: 0 }}
            spacing={3}
          >
            <Grid item xs={6}>
              <Link href={'/app'}>
                <Button
                  color={'secondary'}
                  size={'large'}
                  variant={'outlined'}
                  fullWidth
                >
                  Zpět
                </Button>
              </Link>
            </Grid>
            <Grid item xs={6}>
              <Button
                variant={'contained'}
                color={'primary'}
                size={'large'}
                onClick={start}
                fullWidth
              >
                Spustit
              </Button>
            </Grid>
          </Grid>
        </Card>
      </Box>
    </>
  )
}
