import React, { FC, Fragment, useMemo, useState } from 'react'
import { Box, Breadcrumbs, Button, Paper, Typography } from '@mui/material'
import { TemplatePartsPreviewSlider } from './components/TemplatePartsPreviewSlider'
import { EditTestPart } from './components/EditTestPart'
import { TestTemplate } from '../../types/entities'
import { TemplateContext } from './utils'
import styled from 'styled-components'
import { NewTemplateTutorialModal } from './components/NewTemplateTutorialModal'
import { ModalWrapper, useModalState } from '../../components/ModalWrapper'
import { NewTestPart } from './components/NewTestPart'
import { EmptyTestPartPlaceholder } from './components/EmptyTestPartPlaceholder'
import Link from 'next/link'
import { DeleteTemplate } from './components/DeleteTemplate'
import { useGlobalLoading } from '../../components/GlobalLoader'

type Props = {
  template: TestTemplate
}
export const EditTemplateBundle: FC<Props> = ({ template }) => {
  const cachedActivePart = getCachedTestTemplateActivePart(template.id)
  const { globalLoading } = useGlobalLoading()

  const initialActivePart = useMemo(
    () =>
      template.parts && template.parts.length > 0 ? template.parts[0].id : 0,
    [template],
  )
  const [activePart, setActivePart] = useState(
    cachedActivePart ?? initialActivePart,
  )
  const newPartSet = (newPart: number) => {
    saveActivePart(template.id, newPart)
    setActivePart(newPart)
  }
  const { isModalOpen, openModal, closeModal } = useModalState(false)
  const partDeleted = () => {
    newPartSet(initialActivePart)
  }
  const newPartCreated = (newPartId: number) => {
    newPartSet(newPartId)
    closeModal()
  }
  return (
    <TemplateContext.Provider value={template}>
      <Breadcrumbs sx={{ mb: 4 }}>
        <Link href={`/app`}>Profil učitele</Link>
        <Typography color="text.primary">Šablony testu</Typography>
        <Typography color="text.primary">{template.name}</Typography>
      </Breadcrumbs>
      <StyledGrid>
        <Slider>
          <TemplatePartsPreviewSlider
            parts={template.parts}
            activePart={activePart}
            newActivePartSet={newPartSet}
          />
        </Slider>
        <Content>
          <ContentHeader>
            <Typography variant={'h1'}>{template.name}</Typography>
            <Button
              variant={'contained'}
              sx={{ mt: 1, mb: 3 }}
              onClick={openModal}
            >
              Přidat novou část
            </Button>
          </ContentHeader>
          <ModalWrapper isOpen={isModalOpen} closeModal={closeModal}>
            <NewTestPart created={newPartCreated} />
          </ModalWrapper>
          {template.parts.map((part, index) =>
            activePart === part.id ? (
              <EditTestPart part={part} key={index} partDeleted={partDeleted} />
            ) : (
              <Fragment key={index}></Fragment>
            ),
          )}
          {template.parts.length === 0 && (
            <EmptyTestPartPlaceholder addFirstPart={openModal} />
          )}
          <DeleteTemplate templateId={template.id} />
        </Content>
      </StyledGrid>
      <NewTemplateTutorialModal />
    </TemplateContext.Provider>
  )
}

const getCachedTestTemplateActivePart = (templateId: number) => {
  const cachedActiveParts = localStorage.getItem('TEMPLATE_ACTIVE_PARTS')
  if (!cachedActiveParts) {
    localStorage.setItem('TEMPLATE_ACTIVE_PARTS', '{}')
    return null
  }
  const parsedActiveParts = JSON.parse(cachedActiveParts!)
  if (templateId in parsedActiveParts) return parsedActiveParts[templateId]
  return null
}

const saveActivePart = (templateId: number, activePart: number) => {
  const cachedActiveParts = localStorage.getItem('TEMPLATE_ACTIVE_PARTS')
  const parsedActiveParts = JSON.parse(cachedActiveParts!)
  parsedActiveParts[templateId] = activePart
  localStorage.setItem(
    'TEMPLATE_ACTIVE_PARTS',
    JSON.stringify(parsedActiveParts),
  )
}

const StyledGrid = styled.div`
  display: grid;
  grid-template-columns: 320px 1fr;
  grid-column-gap: 30px;
  max-height: calc(100vh - 72px);
  flex-grow: 1;
`

const Slider = styled(Paper)`
  overflow: auto;
  max-height: calc(100vh - 72px - 40px - 40px);
`

const Content = styled.div`
  height: 100%;
  display: flex;
  flex-direction: column;
`

const ContentHeader = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin-bottom: 20px;
`
