import React, { createContext, useContext } from 'react'
import {
  QuestionWithAnswers,
  TestTemplate,
  TestTemplatePart,
} from '../../types/entities'
import { useQueryClient } from 'react-query/react'
import { EditQuestionGroup } from './components/EditQuestionGroup'

// @ts-ignore
export const TemplateContext = createContext<TestTemplate>({})

export const useTemplateContext = () => {
  const template: TestTemplate = useContext(TemplateContext)
  const queryClient = useQueryClient()
  const refresh = async () =>
    queryClient.refetchQueries(`/test_templates/${template.id}`)
  return { template, refresh }
}

export const formatVariations = (variations: number) => {
  if (variations === 0) return 'Žádná variace'
  else if (variations <= 4) return `${variations} variace`
  else `${variations} variací`
}

export const sortQuestions = (a: QuestionWithAnswers, b: QuestionWithAnswers) =>
  a.id > b.id ? 1 : 0
