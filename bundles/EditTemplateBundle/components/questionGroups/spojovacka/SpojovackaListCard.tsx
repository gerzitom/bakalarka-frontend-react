import { FC, useRef, useState } from 'react'
import { useDrag, useDrop } from 'react-dnd'
import type { Identifier, XYCoord } from 'dnd-core'
import { ItemTypes } from '../../../../../components/Spojovacka/types'
import { QuestionWithAnswers } from '../../../../../types/entities'
import styled from 'styled-components'
import { Box } from '@mui/system'
import DragIndicatorIcon from '@mui/icons-material/DragIndicator'
import { SpojovackaEditField } from './SpojovackaEditField'
import { IconButton, InputAdornment, TextField } from '@mui/material'
import { Form, Formik } from 'formik'
import { FormikSubmitHandler } from '../../../../../types'
import CheckIcon from '@mui/icons-material/Check'
import TestTemplateService from '../../../../../utils/api/TestTemplateService'
import { useTemplateContext } from '../../../utils'
import CloseIcon from '@mui/icons-material/Close'
import DeleteIcon from '@mui/icons-material/Delete'
import { useNotifications } from '../../../../../utils/useNotifications'

export interface CardProps {
  question: QuestionWithAnswers
}

export const SpojovackaListCard: FC<CardProps> = ({ question }) => {
  const option =
    question.possibleAnswers.length === 1 ? question.possibleAnswers[0] : null

  const { refresh } = useTemplateContext()
  const { successNotification } = useNotifications()

  const contentChanged = async (newValue: string) => {
    await TestTemplateService.updateQuestion(question.id, newValue)
    successNotification()
    await refresh()
  }

  const valueChanged = async (newValue: string) => {
    await TestTemplateService.updateOption(option!.id, { text: newValue })
    successNotification()
    await refresh()
  }

  const deleteLine = async () => {
    await TestTemplateService.deleteQuestion(question.id)
    successNotification('Řádek smazán')
    await refresh()
  }
  return (
    <Box sx={{ p: 1, mb: 1, display: 'flex', alignItems: 'center' }}>
      <ContentBox>
        <EditableField
          name={'content'}
          value={question.content}
          valueChanged={contentChanged}
        />
        <Box sx={{ mx: 2 }}>:</Box>
        <EditableField
          name={'text'}
          value={option?.singleOption.text ?? ''}
          valueChanged={valueChanged}
        />
        <IconButton onClick={deleteLine}>
          <DeleteIcon />
        </IconButton>
      </ContentBox>
    </Box>
  )
}

type EditableFieldProps = {
  name: string
  value: string
  valueChanged: (newValue: string) => Promise<void>
}
const EditableField: FC<EditableFieldProps> = ({
  name,
  value,
  valueChanged,
}) => {
  const handleSubmit: FormikSubmitHandler<{ value: string }> = (
    values,
    { resetForm },
  ) => {
    valueChanged(values.value)
    // .then(() => resetForm())
  }
  return (
    <Formik initialValues={{ value }} onSubmit={handleSubmit}>
      {(formikProps) => (
        <Form>
          <TextField
            name={'value'}
            value={formikProps.values.value}
            onChange={formikProps.handleChange}
            size={'small'}
            variant={'standard'}
            InputProps={{
              endAdornment: (
                <ActionButtons
                  position="end"
                  visible={formikProps.values.value !== value}
                >
                  <IconButton
                    disabled={formikProps.values.value === value}
                    onClick={formikProps.submitForm}
                  >
                    <CheckIcon />
                  </IconButton>
                  <IconButton
                    disabled={formikProps.values.value === value}
                    onClick={() => formikProps.resetForm()}
                  >
                    <CloseIcon />
                  </IconButton>
                </ActionButtons>
              ),
            }}
          />
        </Form>
      )}
    </Formik>
  )
}

const ActionButtons = styled(InputAdornment)<{ visible: boolean }>`
  transition: 300ms;
  opacity: ${(props) => (props.visible ? 1 : 0)};
`

const ContentBox = styled.div`
  display: flex;
  align-items: center;
`
