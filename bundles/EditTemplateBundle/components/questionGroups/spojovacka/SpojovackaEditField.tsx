import React, { FC } from 'react'
import styled from 'styled-components'
import { Box } from '@mui/system'
import { Button, IconButton } from '@mui/material'
import EditIcon from '@mui/icons-material/Edit'
import DeleteIcon from '@mui/icons-material/Delete'

type Props = {
  text: string | undefined
}
export const SpojovackaEditField: FC<Props> = ({ text }) => {
  if (text) {
    return (
      <TextBox>
        {text}
        <IconButton size={'small'} sx={{ ml: 2 }}>
          <EditIcon fontSize={'small'} />
        </IconButton>
        <IconButton size={'small'}>
          <DeleteIcon fontSize={'small'} />
        </IconButton>
      </TextBox>
    )
  } else return <Button>Přidat správnou možnost</Button>
}

const TextBox = styled(Box)`
  padding: 0.2em 0 0.2em 0.7em;
  margin-left: 0.7em;
  background: rgba(0, 0, 0, 0.1);
  border-radius: 10px;
  display: flex;
  align-items: center;
`
