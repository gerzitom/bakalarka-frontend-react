import React, { FC, useState } from 'react'
import { QuestionGroup } from '../../../../types/entities'
import { Alert, Box, TextareaAutosize } from '@mui/material'

type Props = {
  questionGroup: QuestionGroup
}
export const EditOpenQuestion: FC<Props> = ({ questionGroup }) => {
  return (
    <Box sx={{ p: 3 }}>
      <Alert severity={'info'}>
        Zde nic vyplňovat nemusíte, studentovi se zobrazí pouze pole pro
        vyplnění své odpovědi
      </Alert>
    </Box>
  )
}
