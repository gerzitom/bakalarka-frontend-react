import React, { FC, useMemo } from 'react'
import { Alert, Typography } from '@mui/material'
import { EditSingleOption } from '../../../TestPage/components/options/edit/EditSingleOption'
import { EditMultipleOption } from '../../../TestPage/components/options/edit/EditMultipleOption'
import { QuestionWithAnswers } from '../../../../types/entities'
import QuestionService from '../../../../utils/api/QuestionService'
import * as Sentry from '@sentry/browser'
import { useNotifications } from '../../../../utils/useNotifications'
import { useTemplateContext } from '../../utils'
import { AddNewOption } from '../../../TestPage/components/options/edit/AddNewOption'
import { useGlobalLoading } from '../../../../components/GlobalLoader'

type Props = {
  question: QuestionWithAnswers
}
export const EditSimpleOption: FC<Props> = ({ question }) => {
  const { errorNotification, successNotification } = useNotifications()
  const { startGlobalLoading, stopGlobalLoading } = useGlobalLoading()
  const { refresh } = useTemplateContext()
  const deleteOption = (optionId: number) => {
    startGlobalLoading()
    QuestionService.removeOption(optionId)
      .then(() => {
        refresh().then(() => successNotification('Možnost smazána!'))
      })
      .catch((err) => {
        Sentry.captureException(err)
        errorNotification(JSON.parse(err.message).message)
      })
      .finally(() => stopGlobalLoading())
  }

  const sortedOptions = useMemo(
    () =>
      question.possibleAnswers.sort((a, b) => {
        return a.singleOption.text.localeCompare(b.singleOption.text)
      }),
    [question],
  )
  const hasCorrectVals = useMemo(() => {
    // @ts-ignore
    return !!sortedOptions.find((option) => option.correct)
  }, [sortedOptions])
  return (
    <>
      {!hasCorrectVals && (
        <Alert severity={'warning'} sx={{ mb: 3, mt: 2 }}>
          Ještě nemáte zvolenou žádnou správnou odpověď
        </Alert>
      )}
      <Typography variant={'overline'}>Možnosti</Typography>
      {question.answerType === 'ONE_OPTION' && (
        <EditSingleOption options={sortedOptions} deleteOption={deleteOption} />
      )}
      {question.answerType === 'MULTIPLE_OPTIONS' && (
        <EditMultipleOption
          options={sortedOptions}
          deleteOption={deleteOption}
        />
      )}
      {sortedOptions.length === 0 && (
        <Alert severity={'warning'}>Ještě nejsou žádné možnosti</Alert>
      )}
      <AddNewOption questionId={question.id} options={sortedOptions} />
    </>
  )
}
