import React, { FC, useCallback, useMemo, useState } from 'react'
import { QuestionGroup, QuestionWithAnswers } from '../../../../types/entities'
import { HTML5Backend } from 'react-dnd-html5-backend'
import { DndProvider } from 'react-dnd'
import { SpojovackaListCard } from './spojovacka/SpojovackaListCard'
import { Box } from '@mui/system'
import TestTemplateService from '../../../../utils/api/TestTemplateService'
import { sortQuestions, useTemplateContext } from '../../utils'
import { useQueryClient } from 'react-query/react'
import { ModalWithNameInput } from '../../../../components/ModalWithNameInput'
import { Field } from 'formik'
import { TextField } from 'formik-mui'
import * as Yup from 'yup'

type Props = {
  questionGroup: QuestionGroup
}
export const EditSpojovacka: FC<Props> = ({ questionGroup }) => {
  const { refresh } = useTemplateContext()

  const addNewLine = async (values: any) => {
    await TestTemplateService.addNewSpojovackaLine(
      questionGroup.id,
      values.name,
      values.answer,
    )
    await refresh()
  }

  const sortedOptions = useMemo(
    () => questionGroup.questions.sort(sortQuestions),
    [questionGroup],
  )

  return (
    <Box sx={{ p: 3 }}>
      <div>
        {sortedOptions.map((question, index) => (
          <SpojovackaListCard key={question.id} question={question} />
        ))}
        <ModalWithNameInput
          buttonText={'Přidat nový řádek'}
          heading={'Nový řádek'}
          formSubmitted={addNewLine}
          additionalSchema={{
            answer: Yup.string().required('Správná odpověď je povinná'),
          }}
          fieldsInitVals={{ answer: '' }}
        >
          <Field
            name={'answer'}
            label={'Správná odpověď'}
            size={'small'}
            fullWidth
            component={TextField}
          />
        </ModalWithNameInput>
      </div>
    </Box>
  )
}
