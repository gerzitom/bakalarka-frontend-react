import React, { FC, useMemo, useRef, useState } from 'react'
import { QuestionGroup } from '../../../../types/entities'
import { Alert, Button, Grid, Typography } from '@mui/material'
import { EditSingleOption } from '../../../TestPage/components/options/edit/EditSingleOption'
import testTemplateService from '../../../../utils/api/TestTemplateService'
import MUIRichTextEditor, {
  TMUIRichTextEditorRef,
} from '../../../../lib/MUIRichTextEditor/MUIRichTextEditor'
import { Box } from '@mui/system'
import { EditMultipleOption } from '../../../TestPage/components/options/edit/EditMultipleOption'
import { useNotifications } from '../../../../utils/useNotifications'
import styled from 'styled-components'
import { EditSimpleOption } from './EditSimpleOption'

/**
 * This component renders editing option for SIMPLE question group.
 * This means, that there will be only 1 question.
 */

type UpdateMutationDto = {
  questionId: number
  assignment: string
}

type Props = {
  questionGroup: QuestionGroup
}
export const EditSimpleQuestionGroup: FC<Props> = ({ questionGroup }) => {
  const [customContent, setCustomContent] = useState(false)
  const { successNotification } = useNotifications()
  const { updateQuestion } = testTemplateService
  const editor = useRef<TMUIRichTextEditorRef>(null)

  const question = questionGroup.questions[0]
  const saveQuestion = () => {
    editor.current?.save()
  }

  const onSaveQuestionAssignment = async (newText: string) => {
    await updateQuestion(question.id, newText)
    successNotification('Otázka byla úspěšně uložena.')
  }

  const emojis = [
    {
      keys: ['face', 'grin'],
      value: '😀',
      content: '😀',
    },
    {
      keys: ['face', 'joy'],
      value: '😂',
      content: '😂',
    },
    {
      keys: ['face', 'sweat'],
      value: '😅',
      content: '😅',
    },
  ]

  const variables = [
    {
      keys: ['prumer'],
      value: '[prumer]',
      content: 'Průměr',
    },
  ]

  if (!question)
    return <Alert severity={'error'}>Chyba API, není vytvořená otázka</Alert>
  if (questionGroup.questions.length > 1)
    return (
      <Alert severity={'error'}>
        Tento typ otázky nepodporuje více variací!
      </Alert>
    )
  return (
    <Box sx={{ px: 4, pb: 4 }}>
      {/*WYSIWYG editor not yet implemented on BE*/}
      {/*<Button*/}
      {/*  variant={'outlined'}*/}
      {/*  color={'secondary'}*/}
      {/*  sx={{ mt: 3 }}*/}
      {/*  onClick={() => setCustomContent(true)}*/}
      {/*>*/}
      {/*  Přidat vlastní zadání*/}
      {/*</Button>*/}
      {/*{customContent && (*/}
      {/*  <MUIRichTextEditor*/}
      {/*    ref={editor}*/}
      {/*    label={'Vaše zadání otázky'}*/}
      {/*    onSave={onSaveQuestionAssignment}*/}
      {/*    defaultValue={question.content}*/}
      {/*    autocomplete={{*/}
      {/*      strategies: [*/}
      {/*        {*/}
      {/*          items: emojis,*/}
      {/*          triggerChar: ':',*/}
      {/*        },*/}
      {/*        {*/}
      {/*          items: variables,*/}
      {/*          triggerChar: '@',*/}
      {/*        },*/}
      {/*      ],*/}
      {/*    }}*/}
      {/*  />*/}
      {/*)}*/}

      <Grid container sx={{ mt: 2 }}>
        <Grid item xs={6}>
          <EditSimpleOption question={question} />
        </Grid>
      </Grid>
      {/*<Button*/}
      {/*  size={'large'}*/}
      {/*  variant={'contained'}*/}
      {/*  sx={{ mt: 4 }}*/}
      {/*  onClick={saveQuestion}*/}
      {/*>*/}
      {/*  Uložit otázku*/}
      {/*</Button>*/}
    </Box>
  )
}

const StyledRichTextEditor = styled(MUIRichTextEditor)`
  & .variable {
    background: grey;
    color: white;
    padding: 5px 10px;
    display: inline-block;
  }
`
