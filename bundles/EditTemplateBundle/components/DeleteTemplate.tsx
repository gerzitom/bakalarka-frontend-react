import React, { FC, useRef } from 'react'
import { ModalWrapper, useModalState } from '../../../components/ModalWrapper'
import { Button, Typography } from '@mui/material'
import TestTemplateService from '../../../utils/api/TestTemplateService'
import { useRouter } from 'next/router'
import { useNotifications } from '../../../utils/useNotifications'

type Props = {
  templateId: number
}
export const DeleteTemplate: FC<Props> = ({ templateId }) => {
  const { isModalOpen, closeModal, openModal } = useModalState(false)
  const { successNotification } = useNotifications()

  const router = useRouter()
  const deleteTestTemplate = () => {
    TestTemplateService.deleteTestTemplate(templateId).then(() => {
      successNotification('Šablona odstraněna!')
      router.push('/app')
    })
  }
  return (
    <>
      <Button color={'error'} sx={{ mt: 2 }} onClick={openModal}>
        Smazat šablonu testu
      </Button>
      <ModalWrapper isOpen={isModalOpen} closeModal={closeModal}>
        <Typography variant={'h3'}>Smazání testové šablony</Typography>
        <Typography variant={'subtitle1'}>
          Opravdu si přejete smazat testovou šablonu?
        </Typography>
        <Button variant={'outlined'} size={'large'} onClick={closeModal}>
          NE
        </Button>
        <Button
          variant={'contained'}
          color={'primary'}
          size={'large'}
          sx={{ mr: 2 }}
          onClick={deleteTestTemplate}
        >
          ANO
        </Button>
      </ModalWrapper>
    </>
  )
}
