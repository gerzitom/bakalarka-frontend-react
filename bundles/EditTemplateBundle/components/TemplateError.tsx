import React, { FC } from 'react'
import { Alert, Button } from '@mui/material'
import Link from 'next/link'

type Props = {}
export const TemplateError: FC<Props> = () => {
  return (
    <>
      <Alert severity={'error'}>
        Nastala chyba serveru, prosím zkuste to později.
      </Alert>
      <Link href={'/app'} passHref>
        <Button variant={'contained'} size={'large'}>
          Přejít na hlavní stránku
        </Button>
      </Link>
    </>
  )
}
