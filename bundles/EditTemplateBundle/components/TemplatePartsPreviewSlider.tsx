import React, { FC, useCallback, useEffect, useState } from 'react'
import { TestTemplatePart } from '../../../types/entities'
import {
  DragDropContext,
  Draggable,
  Droppable,
  DropResult,
} from 'react-beautiful-dnd'
import { TemplatesPartsSlider } from './TemplatesPartsSlider'
import { TemplatePartPreview } from './TemplatePartPreview'
import styled from 'styled-components'

type Props = {
  parts: TestTemplatePart[]
  activePart: number
  newActivePartSet: (newActivePart: number) => void
}
export const TemplatePartsPreviewSlider: FC<Props> = ({
  parts,
  activePart,
  newActivePartSet,
}) => {
  const [list, setList] = useState<TestTemplatePart[]>(parts)
  useEffect(() => {
    setList(parts)
  }, [parts])

  const onDragEnd = useCallback(
    () =>
      ({ destination, source }: DropResult) => {
        // dropped outside the list
        if (!destination) return
        const newItems = reorder(list, source.index, destination.index)
        setList(newItems)
      },
    [],
  )

  return (
    <StyledContainer>
      <DragDropContext onDragEnd={onDragEnd}>
        <Droppable droppableId={'droppable'}>
          {(provided, snapshot) => (
            <SliderContainer
              {...provided.droppableProps}
              ref={provided.innerRef}
            >
              <TemplatesPartsSlider>
                {list.map((part, index) => (
                  <Draggable
                    key={part.id}
                    draggableId={String(part.id)}
                    index={index}
                  >
                    {(provided, snapshot) => (
                      <StyledPartPreviewContainer
                        ref={provided.innerRef}
                        {...provided.draggableProps}
                        {...provided.dragHandleProps}
                      >
                        <TemplatePartPreview
                          testPart={part}
                          active={activePart === part.id}
                          clicked={() => newActivePartSet(part.id)}
                        />
                      </StyledPartPreviewContainer>
                    )}
                  </Draggable>
                ))}
              </TemplatesPartsSlider>
            </SliderContainer>
          )}
        </Droppable>
      </DragDropContext>
    </StyledContainer>
  )
}

const SliderContainer = styled.div`
  height: 100%;
`

const StyledPartPreviewContainer = styled.div`
  width: 100%;
`

const StyledContainer = styled.div`
  height: 100%;
`

export const reorder = (
  list: TestTemplatePart[],
  startIndex: number,
  endIndex: number,
): TestTemplatePart[] => {
  const result = Array.from(list)
  const [removed] = result.splice(startIndex, 1)
  result.splice(endIndex, 0, removed)

  return result
}
