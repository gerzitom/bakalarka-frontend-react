import React, { FC } from 'react'
import Image from 'next/image'
import { Button, Typography } from '@mui/material'
import styled from 'styled-components'
import { colors } from '../../../styles/colors'

type Props = {
  addFirstPart: () => void
}
export const EmptyTestPartPlaceholder: FC<Props> = ({ addFirstPart }) => {
  return (
    <EmptyTestPart>
      <div>
        <Image
          src={'/empty_question.svg'}
          width={500}
          height={450}
          alt={'Empty question placeholder'}
        />
        <Typography variant={'h3'}>Zatím zde není žádná otázka</Typography>
        <Button
          variant={'contained'}
          size={'large'}
          sx={{ mt: 4 }}
          onClick={addFirstPart}
        >
          Přidat první část testu
        </Button>
      </div>
    </EmptyTestPart>
  )
}

const EmptyTestPart = styled.div`
  border: 1px solid ${() => colors.grey};
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  text-align: center;
  flex-grow: 1;
`
