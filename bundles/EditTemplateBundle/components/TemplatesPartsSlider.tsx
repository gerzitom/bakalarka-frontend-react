import React, { FC } from 'react'
import { Stack } from '@mui/material'
import styled from 'styled-components'
import { TemplateSliderItem } from './TemplateSliderItem'

type Props = {}
export const TemplatesPartsSlider: FC<Props> = ({ children }) => {
  return (
    <StyledContainer>
      <Stack>
        {React.Children.map(children, (child, index) => (
          <TemplateSliderItem index={index + 1}>{child}</TemplateSliderItem>
        ))}
      </Stack>
    </StyledContainer>
  )
}

const StyledContainer = styled.div`
  height: 100%;
  overflow-y: auto;
  padding: 1em;
`
