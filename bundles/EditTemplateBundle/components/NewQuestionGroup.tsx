import React, { FC, useState } from 'react'
import { Field, Formik } from 'formik'
import { NewQuestionGroupDto } from '../../../types/entities'
import { FormikSubmitHandler } from '../../../types'
import { TextField } from 'formik-mui'
import { Typography } from '@mui/material'
import { FormikInsideForm } from '../../../components/FormikInsideForm'
import QuestionService from '../../../utils/api/QuestionService'
import { queryClient } from '../../../utils/ReactQueryClient'
import { useTemplateContext } from '../utils'
import { useNotifications } from '../../../utils/useNotifications'
import { LoadingButton } from '@mui/lab'

// TODO add focus on new tab
type Props = {
  partId: number
  added: () => void
}
export const NewQuestionGroup: FC<Props> = ({ partId, added }) => {
  const { template } = useTemplateContext()
  const { successNotification } = useNotifications()
  const [loading, setLoading] = useState(false)
  const { errorNotification } = useNotifications()
  const initVals: NewQuestionGroupDto = {
    instructions: '',
  }
  const handleChange: FormikSubmitHandler<NewQuestionGroupDto> = async (
    values,
  ) => {
    try {
      setLoading(true)
      await QuestionService.addNewQuestionGroup(partId, values)
      await queryClient.refetchQueries(`/test_templates/${template.id}`)
      successNotification('Variace přidána')
      setLoading(false)
      added()
    } catch (e) {
      errorNotification('Nastala chyba, prosíme zkuste to později')
    }
  }
  return (
    <Formik initialValues={initVals} onSubmit={handleChange}>
      {(formikProps) => (
        <FormikInsideForm formikProps={formikProps}>
          <Typography variant={'h3'} sx={{ mb: 2 }}>
            Nová varianta
          </Typography>
          <Field
            label={'Instrukce'}
            name={'instructions'}
            component={TextField}
            fullWidth
          />
          <LoadingButton
            loading={loading}
            type={'submit'}
            sx={{ mt: 2 }}
            fullWidth
          >
            Vytvořit
          </LoadingButton>
        </FormikInsideForm>
      )}
    </Formik>
  )
}
