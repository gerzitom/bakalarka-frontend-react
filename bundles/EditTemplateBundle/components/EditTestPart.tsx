import React, { FC } from 'react'
import { TestTemplatePart } from '../../../types/entities'
import { Alert, Paper } from '@mui/material'
import { EditQuestionGroup } from './EditQuestionGroup'
import { TabsLayout, useTabState } from '../../../components/TabsLayout'
import { ModalWrapper, useModalState } from '../../../components/ModalWrapper'
import { NewQuestionGroup } from './NewQuestionGroup'
import testTemplateService from '../../../utils/api/TestTemplateService'
import { useQueryClient } from 'react-query/react'
import { useTemplateContext } from '../utils'

type Props = {
  part: TestTemplatePart
  partDeleted: () => void
}
export const EditTestPart: FC<Props> = ({ part, partDeleted }) => {
  const { isModalOpen, closeModal, openModal } = useModalState(false)
  const { refresh } = useTemplateContext()
  const tabState = useTabState(0)
  const tabDeleted = () => {
    tabState.setActiveTab(0)
  }
  let tabs = []
  let headings = []
  for (const index in part.questionGroups) {
    const question = part.questionGroups[index]
    tabs.push(
      <EditQuestionGroup
        questionGroup={question}
        key={index}
        deleted={tabDeleted}
      />,
    )
    headings.push(question.instructions)
  }

  const deletePart = async () => {
    await testTemplateService.deletePart(part.id)
    refresh().then(() => partDeleted())
  }

  const questionGroupAdded = () => {
    tabState.setActiveTab(tabs.length)
    closeModal()
  }
  return (
    <Paper elevation={0}>
      <TabsLayout
        headings={headings}
        tabsContent={tabs}
        newTabAdded={openModal}
        deletePart={deletePart}
        {...tabState}
      />
      {tabs.length === 0 && (
        <Alert severity={'warning'} sx={{ my: 2, mx: 2 }}>
          Ještě neexistují žádné variace{' '}
        </Alert>
      )}
      <ModalWrapper isOpen={isModalOpen} closeModal={closeModal}>
        <NewQuestionGroup partId={part.id} added={questionGroupAdded} />
      </ModalWrapper>
    </Paper>
  )
}
