import React, { FC } from 'react'
import { ModalWrapper, useModalState } from '../../../components/ModalWrapper'
import { Button, Typography } from '@mui/material'

const TUTORIAL_LOCAL_STORAGE_KEY = 'show-new-template-tutorial'
export const NewTemplateTutorialModal: FC = () => {
  const localStorageKey = localStorage.getItem(TUTORIAL_LOCAL_STORAGE_KEY)
  const showTutorial = !localStorageKey
  const { isModalOpen, closeModal } = useModalState(Boolean(showTutorial))
  const closeTutorial = () => {
    localStorage.setItem(TUTORIAL_LOCAL_STORAGE_KEY, 'true')
    closeModal()
  }
  return (
    <ModalWrapper isOpen={isModalOpen} closeModal={closeTutorial}>
      <Typography variant={'h2'}>Vítejte u tvorby testových šablon</Typography>
      <Typography variant={'body1'}>
        Toto je krátký úvodní tutoriál, který Vám ukáže základy editoru.
      </Typography>
      <Button onClick={closeModal}>Přeskočit tutoriál</Button>
    </ModalWrapper>
  )
}
