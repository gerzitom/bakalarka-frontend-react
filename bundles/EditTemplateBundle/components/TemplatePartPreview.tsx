import React, { FC, useMemo } from 'react'
import { Paper, Typography } from '@mui/material'
import { TestTemplatePart } from '../../../types/entities'
import styled, { css } from 'styled-components'
import WatchLaterIcon from '@mui/icons-material/WatchLater'
import { QuestionTypeDisplay } from '../../../components/QuestionTypeDisplay'
import { useTheme } from '@mui/system'
import { useDarkMode } from '../../../hooks/useDarkMode'
import { formatVariations } from '../utils'
import GradeIcon from '@mui/icons-material/Grade'

type Props = {
  testPart: TestTemplatePart
  active: boolean
  clicked: () => void
}
export const TemplatePartPreview: FC<Props> = ({
  testPart,
  active,
  clicked,
}) => {
  const variationsCount = testPart?.questionGroups?.length
  const theme = useTheme()
  const { isDarkMode } = useDarkMode()
  const background = useMemo(
    () => (isDarkMode ? theme.palette.background.default : '#fff'),
    [theme],
  )
  return (
    <StyledCard
      sx={{ p: 2, my: 1, width: '100%' }}
      background={background}
      $active={active ?? false}
      onClick={clicked}
    >
      <Heading variant={'h5'} sx={{ mt: 1 }}>
        {testPart.name}
      </Heading>
      <Chips>
        <QuestionTypeDisplay questionType={testPart.preferredType} />
      </Chips>
      <Chips>
        <WatchLaterIcon fontSize={'small'} />
        <span>{testPart.estimatedTime} min</span>
      </Chips>
      <Chips>
        <GradeIcon fontSize={'small'} />
        <span>{testPart.points} bodů</span>
      </Chips>
      <Variations noVariations={variationsCount === 0} background={background}>
        <span>{formatVariations(variationsCount)}</span>
      </Variations>
    </StyledCard>
  )
}

const ActiveCard = css`
  cursor: pointer;
  box-shadow: 0 3px 15px rgba(0, 0, 0, 0.1);
  transform: translateY(-3px);
`

const getBackground = (props: any) => {
  return props.theme.palette.mode === 'dark'
    ? props.theme.palette.background.default
    : '#fff'
}

const StyledCard = styled(Paper)<{ $active: boolean; background: string }>`
  width: 100%;
  box-shadow: 2px 2px 18px rgba(0, 0, 0, 0.2);
  transition: 300ms;
  background: ${(props) => {
    return getBackground(props)
  }};
  border: 2px solid ${(props) => (props.$active ? '#F0BA2A' : props.background)};
  &:hover {
    ${ActiveCard}
  }
  ${(props) => props.$active && ActiveCard}
`

const Heading = styled(Typography)`
  font-weight: 500;
`

const variationsColor = (props: any) =>
  props.theme.palette.mode === 'dark' ? '#fff' : '#000'

const Variations = styled.div<{ noVariations: boolean; background: string }>`
  border-top: 1px solid
    ${(props) => (props.noVariations ? '#E5E5E5' : variationsColor(props))};
  position: relative;
  margin-top: 20px;
  color: ${(props) => (props.noVariations ? '#C4C4C4' : '#000000')};
  span {
    position: absolute;
    top: -10px;
    background: ${(props) => getBackground(props)};
    color: ${(props) => variationsColor(props)};
    padding: 0 10px;
    font-size: 11px;
    text-transform: uppercase;
    left: 50%;
    transform: translateX(-50%);
  }
`

const Chips = styled.div`
  display: flex;
  align-items: center;
  column-gap: 5px;
  margin-top: 5px;
`
