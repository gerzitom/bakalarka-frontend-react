import React, { FC, useState } from 'react'
import { useQueryClient } from 'react-query/react'
import { QuestionTypes } from '../../../types/entities'
import { TextField } from 'formik-mui'
import { Field, Form, Formik } from 'formik'
import { FormikSubmitHandler, SelectItem } from '../../../types'
import { Grid, TextFieldProps, Typography } from '@mui/material'
import * as Yup from 'yup'
import { FormSelect } from '../../../components/FormSelect'
import { useTemplateContext } from '../utils'
import { LoadingButton } from '@mui/lab'
import testTemplateService, {
  TemplatePartCreateDTO,
} from '../../../utils/api/TestTemplateService'
import { useNotifications } from '../../../utils/useNotifications'
import { useGlobalLoading } from '../../../components/GlobalLoader'

type Props = {
  created: (newPartId: number) => void
}
export const NewTestPart: FC<Props> = ({ created }) => {
  const Schema = Yup.object().shape({
    name: Yup.string().required('Jméno je povinné'),
  })
  const initVals: TemplatePartCreateDTO = {
    name: '',
    preferredType: 'ONE_OPTION',
    estimatedTime: 0,
    points: 0,
    description: '',
  }

  const { template, refresh } = useTemplateContext()
  const { successNotification } = useNotifications()
  const { startGlobalLoading, stopGlobalLoading } = useGlobalLoading()
  const handleSubmit: FormikSubmitHandler<TemplatePartCreateDTO> = async (
    values,
  ) => {
    startGlobalLoading()
    const newPart = await testTemplateService.addTestPart(values, template.id)
    successNotification('Část testu přidána')
    await refresh()
    created(newPart.id)
    stopGlobalLoading()
  }

  const questionTypes: SelectItem[] = [
    { label: QuestionTypes['ONE_OPTION'], value: 'ONE_OPTION' },
    { label: QuestionTypes['MULTIPLE_OPTIONS'], value: 'MULTIPLE_OPTION' },
    { label: QuestionTypes['SPOJOVACKA'], value: 'SPOJOVACKA' },
    { label: QuestionTypes['OPEN'], value: 'TEXT_AREA' },
  ]

  const inputProps: TextFieldProps = {
    size: 'small',
    fullWidth: true,
    sx: { mb: 4 },
    variant: 'standard',
  }
  return (
    <Formik
      initialValues={initVals}
      onSubmit={handleSubmit}
      validationSchema={Schema}
    >
      {(formikProps) => (
        <Form>
          <Typography variant={'h2'}>Nová část testu</Typography>
          <Field
            name={'name'}
            label={'Název'}
            component={TextField}
            {...inputProps}
          />
          <FormSelect
            values={questionTypes}
            name={'preferredType'}
            variant={'standard'}
            label={'Třída'}
            textLabel={'Typ odpovědi'}
            size={'small'}
            sx={{ mb: 4 }}
            fullWidth
          />
          <Grid container spacing={3}>
            <Grid item xs={6}>
              <Field
                name={'estimatedTime'}
                label={'Doporučený čas na vypracování'}
                component={TextField}
                {...inputProps}
              />
            </Grid>
            <Grid item xs={6}>
              <Field
                name={'points'}
                label={'Body za otázku'}
                component={TextField}
                {...inputProps}
              />
            </Grid>
          </Grid>
          <LoadingButton
            color={'primary'}
            variant={'outlined'}
            fullWidth
            type={'submit'}
          >
            Vytvořit šablonu
          </LoadingButton>
        </Form>
      )}
    </Formik>
  )
}
