import React, { FC } from 'react'
import styled from 'styled-components'

type Props = {
  index: number
}
export const TemplateSliderItem: FC<Props> = ({ index, children }) => {
  return (
    <StyledContainer>
      <StyledIndex>{index}</StyledIndex>
      {children}
    </StyledContainer>
  )
}

const StyledIndex = styled.div`
  font-weight: bold;
  font-size: 20px;
  color: #c4c4c4;
`

const StyledContainer = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  column-gap: 10px;
`
