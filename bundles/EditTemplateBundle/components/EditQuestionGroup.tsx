import React, { FC } from 'react'
import { QuestionGroup } from '../../../types/entities'
import { EditSimpleQuestionGroup } from './questionGroups/EditSimpleQuestionGroup'
import { Alert, Button } from '@mui/material'
import { EditSpojovacka } from './questionGroups/EditSpojovacka'
import { EditOpenQuestion } from './questionGroups/EditOpenQuestion'
import TestTemplateService from '../../../utils/api/TestTemplateService'
import { useNotifications } from '../../../utils/useNotifications'
import * as Sentry from '@sentry/browser'
import { useQueryClient } from 'react-query/react'
import { useTemplateContext } from '../utils'

type Props = {
  questionGroup: QuestionGroup
  deleted: () => void
}
export const EditQuestionGroup: FC<Props> = ({ questionGroup, deleted }) => {
  const { successNotification, errorNotification } = useNotifications()
  const { refresh } = useTemplateContext()
  const handleDelete = () => {
    TestTemplateService.deleteTestTemplateGroup(questionGroup.id)
      .then(() => {
        successNotification('Otázka smazána')
        deleted()
        refresh()
      })
      .catch((err) => {
        Sentry.captureException(err)
        errorNotification(
          'Otázku se nepodařilo smazat. Prosím zkuste to později.',
        )
      })
  }
  return (
    <>
      <QuestionGroupTypeResolver questionGroup={questionGroup} />
      <Button color={'error'} onClick={handleDelete}>
        Smazat otázku
      </Button>
    </>
  )
}

type ResolverProps = {
  questionGroup: QuestionGroup
}
const QuestionGroupTypeResolver: FC<ResolverProps> = ({ questionGroup }) => {
  if (questionGroup.groupType === 'SIMPLE')
    return <EditSimpleQuestionGroup questionGroup={questionGroup} />
  else if (questionGroup.groupType === 'SPOJOVACKA')
    return <EditSpojovacka questionGroup={questionGroup} />
  else if (questionGroup.groupType === 'OPEN')
    return <EditOpenQuestion questionGroup={questionGroup} />
  return (
    <Alert severity={'error'}>
      Nepodporovaný typ otázky: {questionGroup.groupType}
    </Alert>
  )
}
