import React, { FC, useMemo } from 'react'
import { Test } from '../../../types/entities'
import { Button, Card, Typography } from '@mui/material'
import TimerRoundedIcon from '@mui/icons-material/TimerRounded'
import { Box } from '@mui/system'
import WatchLaterRoundedIcon from '@mui/icons-material/WatchLaterRounded'
import { computeMinutesToEnd, formatTime } from '../../../utils'
import Link from 'next/link'
import { useGlobalLoading } from '../../../components/GlobalLoader'

type Props = {
  test: Test
}
export const StudentRunningTest: FC<Props> = ({ test }) => {
  const testEndTime = useMemo(() => new Date(test.endDate), [test])
  const minutesToEnd = useMemo(() => computeMinutesToEnd(test), [test])
  const { startGlobalLoading } = useGlobalLoading()
  return (
    <Card sx={{ px: 4, py: 3, mb: 3, mr: 4, display: 'inline-block' }}>
      <Box sx={{ display: 'flex', alignItems: 'center' }}>
        <div>
          <Typography variant={'h5'} sx={{ mb: 1 }}>
            {test.name}
          </Typography>
          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <TimerRoundedIcon sx={{ mr: 1 }} />{' '}
            <span>{minutesToEnd} min do konce testu</span>
          </Box>
          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <WatchLaterRoundedIcon sx={{ mr: 1 }} />{' '}
            <span>{formatTime(testEndTime, true)}</span>
          </Box>
        </div>
        <Link href={`/app/test/${test.id}`}>
          <Button
            variant={'contained'}
            sx={{ ml: 7 }}
            size={'large'}
            disableElevation
            onClick={startGlobalLoading}
          >
            {test.results[0].timeStarted ? 'Pokračovat' : 'Začít test'}
          </Button>
        </Link>
      </Box>
    </Card>
  )
}
