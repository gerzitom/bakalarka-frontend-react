import React, { FC, useState } from 'react'
import { Button, Typography } from '@mui/material'
import { Field, Formik } from 'formik'
import { FormikSubmitHandler } from '../../../types'
import { Box } from '@mui/system'
import { ModalWrapper, useModalState } from '../../../components/ModalWrapper'
import * as Yup from 'yup'
import { useQueryClient } from 'react-query/react'
import testTemplateService from '../../../utils/api/TestTemplateService'
import { useRouter } from 'next/router'
import { TestTemplate } from '../../../types/entities'
import { LoadingButton } from '@mui/lab'
import { FormikInsideForm } from '../../../components/FormikInsideForm'
import { TextField } from 'formik-mui'

type FormikVals = {
  templateName: string
}
type Props = {}
export const NewTestTemplate: FC<Props> = () => {
  const { openModal, isModalOpen, closeModal } = useModalState(false)
  const { addNewTestTemplate } = testTemplateService
  const router = useRouter()
  const queryClient = useQueryClient()
  const [loading, setLoading] = useState(false)
  const Schema = Yup.object().shape({
    templateName: Yup.string().required('Jméno je povinné'),
  })
  const initVals: FormikVals = {
    templateName: '',
  }

  const handleSubmit: FormikSubmitHandler<FormikVals> = (values) => {
    setLoading(true)
    addNewTestTemplate(values.templateName)
      .then((newTestTemplate) => {
        queryClient.setQueryData<TestTemplate>(
          `/test_templates/${newTestTemplate.id}`,
          newTestTemplate,
        )
        router.push(`/app/templates/${newTestTemplate.id}?new`)
      })
      .finally(() => {
        // setLoading(false)
      })
  }
  return (
    <>
      <Button variant={'outlined'} onClick={openModal} sx={{ mt: 2 }} fullWidth>
        Vytvořit novou šablonu
      </Button>
      <ModalWrapper isOpen={isModalOpen} closeModal={closeModal}>
        <Box>
          <Typography variant={'h4'}>Nová šablona</Typography>
          <Formik
            initialValues={initVals}
            onSubmit={handleSubmit}
            validationSchema={Schema}
          >
            {(formikProps) => (
              <FormikInsideForm formikProps={formikProps}>
                <Field
                  fullWidth
                  size={'small'}
                  name={'templateName'}
                  label={'Název nové šablony'}
                  sx={{ my: 2 }}
                  component={TextField}
                />
                <LoadingButton
                  loading={loading}
                  color={'primary'}
                  variant={'outlined'}
                  fullWidth
                  type={'submit'}
                >
                  Vytvořit šablonu
                </LoadingButton>
              </FormikInsideForm>
            )}
          </Formik>
        </Box>
      </ModalWrapper>
    </>
  )
}
