import React, { FC, useMemo } from 'react'
import {
  Alert,
  Button,
  Chip,
  Divider,
  Grid,
  Paper,
  Skeleton,
  Typography,
} from '@mui/material'
import Link from 'next/link'
import AddIcon from '@mui/icons-material/Add'
import { DashboardPanel } from '../../../components/DashboardPanel'
import { TestPreview } from '../../../components/TestPreview'
import { TestTemplatePreview } from './TestTemplatePreview'
import { NewTestTemplate } from './NewTestTemplate'
import { useQuery } from 'react-query'
import { Classroom, Test, TestTemplate, User } from '../../../types/entities'
import { sortExamsByDate } from '../utils'
import { ClassroomPreview } from '../../../components/ClassroomPreview'

type Props = {
  user: User
}
export const TeacherDashboard: FC<Props> = ({ user }) => {
  const { data: templates, isLoading } =
    useQuery<TestTemplate[]>('/test_templates')
  const { data: tests } = useQuery<Test[]>('/exams')
  const { data: classrooms, isLoading: classroomsLoading } =
    useQuery<Classroom[]>('/classrooms')
  return (
    <>
      <Grid container alignItems={'center'}>
        <Grid item xs={8}>
          <Typography variant={'overline'}>Vítejte</Typography>
          <Typography variant={'h1'} sx={{ mb: 1 }}>
            Tomáš Geržičák
          </Typography>
          <Chip label={'Učitel'} sx={{ mb: 3 }} color={'primary'} />
        </Grid>
        <Grid item xs={4}>
          <Link href={`/app/new_test`}>
            <Button
              size={'large'}
              variant={'contained'}
              startIcon={<AddIcon />}
            >
              Vytvořit test
            </Button>
          </Link>
        </Grid>
      </Grid>

      <Grid container spacing={2}>
        <Grid item xs={4}>
          {tests && <TeacherExams tests={tests} />}
          {!tests && (
            <>
              <TestsLoadingSkeleton />
              <TestsLoadingSkeleton />
              <TestsLoadingSkeleton />
            </>
          )}
        </Grid>
        <Grid item xs={4}>
          <DashboardPanel heading={'Šablony testů'}>
            {!isLoading &&
              templates?.map((template, index) => (
                <TestTemplatePreview template={template} key={index} />
              ))}
            {isLoading && <TestsLoadingSkeleton />}
            <NewTestTemplate />
          </DashboardPanel>
        </Grid>
        <Grid item xs={4}>
          <DashboardPanel heading={'Moje třídy'}>
            {classrooms &&
              classrooms?.map((classroom) => (
                <ClassroomPreview classroom={classroom} key={classroom.id} />
              ))}
          </DashboardPanel>
        </Grid>
        <Grid item xs={4}>
          <Paper></Paper>
        </Grid>
      </Grid>
    </>
  )
}

type ExamsProps = {
  tests: Test[]
}
const TeacherExams: FC<ExamsProps> = ({ tests }) => {
  const sortedExams = useMemo(() => sortExamsByDate(tests), [tests])
  const buttonStyles = {
    sx: {
      mt: 2,
    },
  }
  return (
    <>
      <DashboardPanel heading={'Probíhající testy'}>
        {sortedExams.running?.map((test, index) => (
          <TestPreview key={index} test={test} />
        ))}
        {sortedExams.running.length === 0 && (
          <Alert severity={'info'}>Žádné testy teď neprobíhají.</Alert>
        )}
      </DashboardPanel>
      <DashboardPanel heading={'Nadcházející testy'}>
        {sortedExams.upcomming.map((test, index) => (
          <TestPreview key={index} test={test} />
        ))}
        {sortedExams.upcomming.length === 0 && (
          <Alert severity={'info'}>Zatím nejsou žádné nadcházející testy</Alert>
        )}
        <Link href={`/app/new_test`}>
          <Button variant={'outlined'} fullWidth {...buttonStyles}>
            Zadat nový test
          </Button>
        </Link>
      </DashboardPanel>

      <DashboardPanel heading={'Předešlé testy'}>
        {sortedExams.finished?.map((test, index) => (
          <TestPreview key={index} test={test} />
        ))}
      </DashboardPanel>
    </>
  )
}

const TestsLoadingSkeleton: FC = () => {
  return (
    <DashboardPanel heading={''}>
      <Skeleton height={40} />
      <Skeleton height={40} />
      <Skeleton height={40} />
    </DashboardPanel>
  )
}
