import React, { FC, useState } from 'react'
import { TestTemplate } from '../../../types/entities'
import { Box, Chip, Divider, LinearProgress, Typography } from '@mui/material'
import Link from 'next/link'
import styled from 'styled-components'

type Props = {
  template: TestTemplate
}
export const TestTemplatePreview: FC<Props> = ({ template }) => {
  const [clicked, setClicked] = useState(false)
  return (
    <StyledListItem sx={{ mx: -4 }}>
      <Divider />
      <Box sx={{ px: 4, py: 2 }}>
        <Link href={`/app/templates/${template.id}`}>
          <a onClick={() => setClicked(true)}>
            <Typography variant={'body1'}>{template.name}</Typography>
            <Chips>
              <Chip
                label={`Části testu: ${template.parts.length}`}
                size={'small'}
                color={'warning'}
              />
              <Chip
                label={`Doporučený čas: ${template.recommendedTime} minut`}
                size={'small'}
                color={'info'}
              />
            </Chips>
          </a>
        </Link>
      </Box>
      {clicked && <LinearProgress />}
      <Divider />
    </StyledListItem>
  )
}

export const StyledListItem = styled(Box)`
  cursor: pointer;
  display: block;
  transition: 300ms;
  margin-bottom: -1px;
  &:hover {
    background: rgba(0, 0, 0, 0.04);
  }
`

const Chips = styled.div`
  display: flex;
  column-gap: 5px;
  margin-top: 5px;
`
