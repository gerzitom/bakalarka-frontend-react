import React, { FC, useEffect, useMemo, useState } from 'react'
import { useQuery } from 'react-query'
import { Test } from '../../../types/entities'
import { Chip, Grid, Paper, Skeleton, Typography } from '@mui/material'
import { DashboardPanel } from '../../../components/DashboardPanel'
import { useUser } from '../../../hooks/useUser'
import Link from 'next/link'
import { StudentTestPreview } from './StudentTestPreview'
import { sortExamsByDate, sortTests } from '../utils'
import { StudentRunningTest } from './StudentRunningTest'

type Props = {}
export const StudentDashboard: FC<Props> = (props) => {
  const { user } = useUser()
  const { data: tests, isLoading: testsLoading } = useQuery<Test[]>('/tests')
  const [repeater, setRepeater] = useState(0)

  const sortedExams = sortExamsByDate(tests ?? [])
  const finishedTests = sortedExams.finished.sort(sortTests)

  /**
   * Recalculate test times every 30 seconds
   */
  useEffect(() => {
    const interval = setInterval(
      () => setRepeater((prevState) => prevState + 1),
      1000 * 30,
    )
    return () => clearInterval(interval)
  })

  return (
    <>
      <Grid container>
        <Grid item xs={8}>
          <Typography variant={'overline'}>Vítejte</Typography>
          <Typography variant={'h1'} sx={{ mb: 1 }}>
            {user.name}
          </Typography>
          <Chip label={'Student'} sx={{ mb: 3 }} color={'primary'} />
          <Chip label={'8.A'} sx={{ mb: 3, ml: 2 }} color={'secondary'} />
        </Grid>
        <Grid item xs={4}></Grid>
      </Grid>

      {sortedExams.running.map((exam) => (
        <StudentRunningTest test={exam} key={exam.id} />
      ))}

      <Grid container spacing={2}>
        <Grid item xs={4}>
          <DashboardPanel heading={'Moje nadcházející testy'}>
            {!testsLoading &&
              sortedExams.upcomming?.map((test) => (
                <Link href={`/app/test/${test.id}`} key={test.id}>
                  <a>
                    <StudentTestPreview test={test} />
                  </a>
                </Link>
              ))}
            {testsLoading && (
              <>
                <Skeleton height={50} />
                <Skeleton height={50} />
              </>
            )}
          </DashboardPanel>
        </Grid>
        <Grid item xs={4}>
          <DashboardPanel heading={'Moje uplynulé testy'}>
            {!testsLoading &&
              finishedTests?.map((test) => (
                <Link href={`/app/test/${test.id}`} key={test.id}>
                  <a>
                    <StudentTestPreview test={test} />
                  </a>
                </Link>
              ))}
          </DashboardPanel>
        </Grid>
        <Grid item xs={4}>
          <Paper></Paper>
        </Grid>
      </Grid>
    </>
  )
}
