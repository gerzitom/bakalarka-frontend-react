import React, { FC } from 'react'
import { StyledListItem } from './TestTemplatePreview'
import { Chip, Grid, Typography } from '@mui/material'
import Link from 'next/link'
import { Test } from '../../../types/entities'
import { formatTime, isTimeClose } from '../../../utils'
import { parseISO } from 'date-fns'

type Props = {
  test: Test
}
export const StudentTestPreview: FC<Props> = ({ test }) => {
  const startDate = parseISO(test.startDate)
  const testDone = new Date() > startDate
  const timeClose = isTimeClose(startDate)
  const formatedStartDate = formatTime(new Date(test.startDate))
  return (
    <Link href={`/app/test/${test.id}`}>
      <a>
        <StyledListItem sx={{ mx: -4, px: 4, py: 2 }}>
          <Grid
            container
            justifyContent={'space-between'}
            alignItems={'center'}
          >
            <Grid item flexGrow={1}>
              <Typography variant={'body1'}>{test.name}</Typography>
            </Grid>
            <Grid item>
              {testDone && <Chip color={'info'} label={'Ukončeno'} />}
              {!testDone && (
                <Typography
                  variant={'body2'}
                  color={timeClose ? 'error' : 'secondary'}
                >
                  {formatedStartDate}
                </Typography>
              )}
            </Grid>
          </Grid>
        </StyledListItem>
      </a>
    </Link>
  )
}
