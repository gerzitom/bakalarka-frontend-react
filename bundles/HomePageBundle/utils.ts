import { Test } from '../../types/entities'
import {
  addMinutes,
  differenceInHours,
  differenceInMinutes,
  parseISO,
} from 'date-fns'

export type SortedExams = {
  upcomming: Test[]
  running: Test[]
  finished: Test[]
}

export const sortExamsByDate = (exams: Test[]): SortedExams => {
  const now = new Date()
  return exams.reduce(
    (acc, exam) => {
      const start = new Date(exam.startDate)
      const end = new Date(exam.endDate)
      if (start > now) acc.upcomming.push(exam)
      else if (now > end) acc.finished.push(exam)
      else acc.running.push(exam)
      return acc
    },
    { upcomming: [], running: [], finished: [] } as SortedExams,
  )
}

export const sortTests = (a: Test, b: Test) => {
  const Afinished = new Date(a.results[0].timeFinished)
  const Bfinished = new Date(b.results[0].timeFinished)
  return Afinished < Bfinished ? 1 : 0
}
