import React, { FC } from 'react'
import { useUser } from '../../hooks/useUser'
import { TeacherDashboard } from './components/TeacherDashboard'
import { StudentDashboard } from './components/StudentDashboard'
import { Alert } from '@mui/lab'

export const HomePageBundle: FC = () => {
  const { user } = useUser()
  if (!user) return <Alert severity={'error'}>Uživatel nebyl nalezen</Alert>
  if (user.role === 'teacher') return <TeacherDashboard user={user} />
  else if (user.role === 'student') return <StudentDashboard />
  else return <Alert severity={'error'}>Neznámá role uživatele</Alert>
}
