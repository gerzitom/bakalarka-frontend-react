import React, { FC } from 'react'
import { alpha, Button, Card, Divider, Typography } from '@mui/material'
import { UserAvatar } from '../../../components/UserAvatar'
import styled from 'styled-components'
import { amber, green, lightGreen, yellow } from '@mui/material/colors'
import CheckCircleIcon from '@mui/icons-material/CheckCircle'
import StarBorderIcon from '@mui/icons-material/StarBorder'
import { StudentTestSummary, Test } from '../../../types/entities'
import PlayCircleOutlineIcon from '@mui/icons-material/PlayCircleOutline'
import SignalCellularOffIcon from '@mui/icons-material/SignalCellularOff'
import { formatTime } from '../../../utils'
import Link from 'next/link'

type SummaryState = 'NOT_STARTED' | 'STARTED' | 'FINISHED'
type Props = {
  test: Test
  summary: StudentTestSummary
}
export const StudentTestPreview: FC<Props> = ({ summary, test }) => {
  const summaryState = resolveStudentTestState(summary)
  const points =
    summaryState === 'FINISHED'
      ? `${Math.round(summary.numberOfPoints)}/${summary.maxPoints}`
      : '---'
  return (
    <StyledCard sx={{ py: 1, px: 2, mt: 2 }} elevation={2}>
      <UserAvatar avatarId={summary.student.email} />
      <Typography variant={'subtitle1'}>{summary.student.name}</Typography>
      <TimeContainer>
        <Time summary={summary} test={test} />
      </TimeContainer>
      <div>
        <Points>
          <StarBorderIcon />
          <span>{points}</span>
        </Points>
      </div>
      <Divider orientation="vertical" flexItem />
      <ResultsButton>
        <Link
          href={`/app/test_preview/${summary.testId}/user/${summary.student.id}`}
        >
          <Button>Výsledky</Button>
        </Link>
      </ResultsButton>
    </StyledCard>
  )
}

const resolveStudentTestState = (summary: StudentTestSummary): SummaryState => {
  if (!summary.timeStarted) return 'NOT_STARTED'
  else if (!summary.timeFinished) return 'STARTED'
  else return 'FINISHED'
}

const Time: FC<Props> = ({ summary }) => {
  if (!summary.timeStarted)
    return (
      <MyChip>
        <SignalCellularOffIcon />
        <span>Student ještě nezačal</span>
      </MyChip>
    )
  else if (!summary.timeFinished)
    return (
      <MyChip>
        <PlayCircleOutlineIcon />
        <span>{formatTime(summary.timeStarted, true)}</span>
      </MyChip>
    )
  else {
    const start = new Date(summary.timeStarted)
    const end = new Date(summary.timeFinished)
    return (
      <TimeFinished>
        <CheckCircleIcon />
        <span>
          {formatTime(start, true)}&ndash;{formatTime(end, true)}
        </span>
      </TimeFinished>
    )
  }
}

const TimeContainer = styled.div`
  display: flex;
  justify-content: end;
`

const StyledCard = styled(Card)`
  display: grid;
  align-items: center;
  grid-column-gap: 20px;
  grid-template-columns: 50px 1fr 200px 100px 5px 100px;
`

const ResultsButton = styled.div`
  display: flex;
  //justify-content: center;
`

const MyChip = styled.div`
  border-radius: 10px;
  display: inline-flex;
  align-items: center;
  padding: 0.4em 0.9em;
  font-size: 0.9rem;
  span {
    margin-left: 10px;
  }
`

const finishedTimeColors: {
  [key: string]: { bg: string; color: string; icon: string }
} = {
  dark: {
    bg: 'rgb(12, 19, 13)',
    color: green[50],
    icon: lightGreen[300],
  },
  light: {
    bg: alpha(green[100], 0.4),
    color: green[900],
    icon: alpha(green[300], 0.9),
  },
}

const TimeFinished = styled(MyChip)`
  background: ${(props) => finishedTimeColors[props.theme.palette.mode].bg};
  color: ${(props) => finishedTimeColors[props.theme.palette.mode].color};
  path {
    color: ${(props) => finishedTimeColors[props.theme.palette.mode].icon};
  }
`

const pointsColors: {
  [key: string]: { bg: string; color: string; icon: string }
} = {
  dark: {
    bg: alpha(yellow['A700'], 0.2),
    color: green[50],
    icon: yellow['A700'],
  },
  light: {
    bg: amber[50],
    color: green[900],
    icon: 'black',
  },
}

const Points = styled(MyChip)`
  background: ${(props) => pointsColors[props.theme.palette.mode].bg};
  path {
    color: ${(props) => pointsColors[props.theme.palette.mode].icon};
  }
`
