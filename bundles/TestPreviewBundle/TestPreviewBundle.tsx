import React, { FC, useMemo } from 'react'
import {
  Alert,
  Button,
  Grid,
  LinearProgress,
  Paper,
  Typography,
} from '@mui/material'
import { StudentTestSummary, Test } from '../../types/entities'
import { formatTime } from '../../utils'
import styled from 'styled-components'
import { Box } from '@mui/system'
import { StudentTestPreview } from './components/StudentTestPreview'
import { DeleteModal } from '../../components/DeleteModal'
import TestService from '../../utils/api/TestService'
import { useNotifications } from '../../utils/useNotifications'
import * as Sentry from '@sentry/browser'
import { useRouter } from 'next/router'

type Props = {
  test: Test
  students: StudentTestSummary[]
}
export const TestPreviewBundle: FC<Props> = ({ test, students }) => {
  const { successNotification, errorNotification } = useNotifications()
  const router = useRouter()
  const studentsEnded = useMemo(
    () => students.filter((student) => !!student.timeFinished),
    [students],
  )
  const studentsInProgress = useMemo(
    () => students.filter((student) => !student.timeFinished),
    [students],
  )

  const totalUsers = students.length
  const testUserProgress = (studentsEnded.length / students.length) * 100

  const handleDelete = () => {
    TestService.deleteTest(test.id)
      .then(() => {
        successNotification('Test úspěšně smazán')
        router.push('/app')
      })
      .catch((err) => {
        errorNotification(
          'Test se nepodařilo smazat. Prosím zkuste to za chvíli',
        )
        Sentry.captureException(err)
      })
  }
  return (
    <Box sx={{ mt: 2 }}>
      <Typography variant={'overline'}>Název testu</Typography>
      <Typography variant={'h1'}>{test.name}</Typography>

      <Grid container spacing={3} sx={{ mt: 1 }}>
        <Grid item xs={8}>
          <Typography variant={'h3'} sx={{ mt: 3 }}>
            Odevzdalo
          </Typography>
          {studentsEnded.map((student) => (
            <StudentTestPreview
              key={student.id}
              summary={student}
              test={test}
            />
          ))}
          {studentsEnded.length === 0 && (
            <Alert severity={'warning'} sx={{ mt: 2 }}>
              Neště neodevzdal žádný student
            </Alert>
          )}

          <Typography variant={'h3'} sx={{ mt: 3 }}>
            Neodevzdalo
          </Typography>
          {studentsInProgress.map((student) => (
            <StudentTestPreview
              key={student.id}
              summary={student}
              test={test}
            />
          ))}
        </Grid>
        <Grid item xs={4}>
          <Paper sx={{ p: 3 }}>
            <Typography variant={'h3'}>Začátek testu</Typography>
            <Time>{formatTime(test.startDate)}</Time>
            <Typography variant={'h3'}>Konec testu</Typography>
            <Time>{formatTime(test.endDate)}</Time>
            <p>
              Odevzdalo: {studentsEnded.length} z {totalUsers} studentů
            </p>
            <LinearProgress variant="determinate" value={testUserProgress} />
          </Paper>
          <DeleteModal
            buttonText={'Smazat test'}
            text={'Opravdu si přejete smazat celý test'}
            sureDelete={handleDelete}
          />
        </Grid>
      </Grid>
    </Box>
  )
}

const Time = styled.p`
  margin: 0;
  font-size: 2rem;
  font-weight: 500;
  font-family: 'Bebas Neue', sans-serif;
  margin-bottom: 20px;
`
