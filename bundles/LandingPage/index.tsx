import React, { FC } from 'react'
import Image from 'next/image'
import styled from 'styled-components'
import {
  AppBar,
  Button,
  Card,
  Container,
  Grid,
  Typography,
} from '@mui/material'
import { Box } from '@mui/system'
import { BasicLayoutAppBar } from '../../components/layout/BasicLayoutAppBar'
import Link from 'next/link'

export const LandingPage: FC = () => {
  return (
    <>
      <BasicLayoutAppBar />
      <Hero>
        <Container>
          <Grid container justifyContent={'center'}>
            <Grid item xs={6}>
              <Box sx={{ textAlign: 'center', mb: 7, mt: 10 }}>
                <Typography variant={'h1'}>Tvorba testů jednoduše</Typography>

                <Typography variant={'body1'}>
                  Vytvořte test, který reflektuje vaše učivo a jedním klikem ho
                  pošlete všem vašim žákům
                </Typography>
                <Box sx={{ display: 'flex', justifyContent: 'center', mt: 3 }}>
                  <Button variant={'contained'} size={'large'} sx={{ mx: 2 }}>
                    Zaregistrovat se
                  </Button>
                  <Link href={'/auth/login'}>
                    <Button
                      variant={'outlined'}
                      size={'large'}
                      color={'secondary'}
                      sx={{ mx: 2 }}
                    >
                      Přihlásit se
                    </Button>
                  </Link>
                </Box>
              </Box>
            </Grid>
          </Grid>
          <HeroImage>
            <Image src={'/hero.png'} width={1439} height={704} />
          </HeroImage>
        </Container>
      </Hero>

      <Container>
        <Cards>
          <Card sx={{ p: 4 }}>
            <Typography variant={'h2'} sx={{ mb: 2 }}>
              Vytvořte testovou šablonu
            </Typography>
            <Typography variant={'body1'}>
              Vytvořte test, který reflektuje vaše učivo a jedním klikem ho
              pošlete všem vašim žákům. asdVytvořte test, který reflektuje vaše
              učivo a jedním klikem ho pošlete všem vašim žákům .as .as Vytvořte
              test, který reflektuje vaše učivo a jedním klikem ho pošlete všem
              vašim žákům
            </Typography>
          </Card>
          <Card sx={{ p: 4 }}>
            <Typography variant={'h2'} sx={{ mb: 2 }}>
              Vytvořte testovou šablonu
            </Typography>
            <Typography variant={'body1'}>
              Vytvořte test, který reflektuje vaše učivo a jedním klikem ho
              pošlete všem vašim žákům. asdVytvořte test, který reflektuje vaše
              učivo a jedním klikem ho pošlete všem vašim žákům .as .as Vytvořte
              test, který reflektuje vaše učivo a jedním klikem ho pošlete všem
              vašim žákům
            </Typography>
          </Card>
          <Card sx={{ p: 4 }}>
            <Typography variant={'h2'} sx={{ mb: 2 }}>
              Vytvořte testovou šablonu
            </Typography>
            <Typography variant={'body1'}>
              Vytvořte test, který reflektuje vaše učivo a jedním klikem ho
              pošlete všem vašim žákům. asdVytvořte test, který reflektuje vaše
              učivo a jedním klikem ho pošlete všem vašim žákům .as .as Vytvořte
              test, který reflektuje vaše učivo a jedním klikem ho pošlete všem
              vašim žákům
            </Typography>
          </Card>
        </Cards>
      </Container>
    </>
  )
}

const MyAppBar = styled(AppBar)`
  background: white;
`

const Hero = styled.section`
  padding-top: 100px;
  background: rgba(72, 167, 255, 0.5);
  img {
    width: 100%;
  }
`

const HeroImage = styled.div`
  margin-bottom: -200px !important;
`

const Cards = styled.div`
  margin-top: 300px;
  display: grid;
  grid-template-columns: 1fr 1fr 1fr;
  grid-column-gap: 40px;
`
