import React, { FC, useMemo } from 'react'
import { TestTemplate } from '../../../types/entities'
import { useField } from 'formik'
import { Alert } from '@mui/material'

type Props = {
  templates: TestTemplate[]
}
export const RecommendedTime: FC<Props> = ({ templates }) => {
  const [selectedTemplate] = useField('template')
  const recommendedTime = useMemo(
    () =>
      templates?.find((template) => template.id === selectedTemplate.value)
        ?.recommendedTime,
    [selectedTemplate],
  )

  return (
    <Alert severity={'info'} sx={{ mt: 5 }} elevation={0}>
      Doporučený čas podle šablony: {recommendedTime} min
    </Alert>
  )
}
