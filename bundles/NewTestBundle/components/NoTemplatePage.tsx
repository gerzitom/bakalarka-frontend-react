import React, { FC } from 'react'
import { Alert, Paper } from '@mui/material'
import Link from 'next/link'
import { NewTestTemplate } from '../../HomePageBundle/components/NewTestTemplate'

type Props = {}
export const NoTemplatePage: FC<Props> = () => {
  return (
    <Paper sx={{ p: 5 }} elevation={0}>
      <Alert severity={'error'}>Zatím není vytvořená žádná šablona</Alert>
      <NewTestTemplate />
    </Paper>
  )
}
