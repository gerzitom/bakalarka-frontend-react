import React, { FC, useMemo } from 'react'
import { DashboardPanel } from '../../../components/DashboardPanel'
import { Classroom } from '../../../types/entities'
import { mapToSelect } from '../utils'
import { FormikProps, useField, useFormikContext } from 'formik'
import { NewTestDto } from '../entities'
import { FormSelect } from '../../../components/FormSelect'
import { Typography } from '@mui/material'
import { UserAvatar } from '../../../components/UserAvatar'
import { Box } from '@mui/system'
import { UserProfileCard } from '../../../components/UserProfileCard'

type Props = {
  classrooms: Classroom[]
  formikProps: FormikProps<NewTestDto>
}
export const NewTestClassroomSelect: FC<Props> = ({
  classrooms,
  formikProps,
}) => {
  const classroomsSelect = useMemo(() => mapToSelect(classrooms), [classrooms])
  const selectedClassroomId = formikProps.values.classroomId
  const selectedClassroom = useMemo(
    () => classrooms.find((classroom) => classroom.id === selectedClassroomId),
    [selectedClassroomId],
  )
  return (
    <DashboardPanel heading={'Výběr třídy'}>
      <FormSelect
        sx={{ mb: 4 }}
        values={classroomsSelect}
        name={'classroomId'}
        variant={'standard'}
        label={'Třída'}
        textLabel={'Třída'}
      />
      <Typography variant={'overline'}>Studenti</Typography>
      {selectedClassroom?.students.map((student) => (
        <UserProfileCard user={student} key={student.id} />
      ))}
    </DashboardPanel>
  )
}
