import React, { FC, useMemo } from 'react'
import { TestTemplate } from '../../../types/entities'
import { mapToSelect } from '../utils'
import { FormSelect } from '../../../components/FormSelect'
import { FormikProps } from 'formik'
import { NewTestDto } from '../entities'
import { Typography } from '@mui/material'
import { UserProfileCard } from '../../../components/UserProfileCard'
import { TemplatePartPreview } from '../../EditTemplateBundle/components/TemplatePartPreview'

type Props = {
  templates: TestTemplate[]
  formikProps: FormikProps<NewTestDto>
}
export const NewTestChooseTemplate: FC<Props> = ({
  templates,
  formikProps,
}) => {
  const templatesSelect = useMemo(() => mapToSelect(templates), [templates])
  const selectedTemplateId = formikProps.values.template
  const selectedTemplate = useMemo(
    () => templates.find((template) => template.id === selectedTemplateId),
    [selectedTemplateId],
  )
  return (
    <div>
      <FormSelect
        sx={{ mb: 4 }}
        values={templatesSelect}
        name={'template'}
        variant={'standard'}
        label={'Šablona'}
        textLabel={'Šablona'}
      />
      {selectedTemplate && (
        <>
          <Typography variant={'overline'}>části testu</Typography>
          {selectedTemplate?.parts.map((part) => (
            <TemplatePartPreview
              key={part.id}
              testPart={part}
              active={false}
              clicked={() => {}}
            />
          ))}
        </>
      )}
    </div>
  )
}
