import React, { FC } from 'react'
import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  TextField as MuiTextField,
  Typography,
} from '@mui/material'
import ExpandMoreIcon from '@mui/icons-material/ExpandMore'
import { formatTime } from '../../../utils'
import { StaticTimePicker } from '@mui/lab'
import { useField } from 'formik'

type Props = {}
export const ChooseEndTime: FC<Props> = () => {
  const [startField, startMeta, startHelper] = useField('startDate')
  const [endField, endMeta, endHelper] = useField('endDate')
  return (
    <Accordion>
      <AccordionSummary expandIcon={<ExpandMoreIcon />}>
        <Typography>
          Začátek testu: {formatTime(endField.value, true)}
        </Typography>
      </AccordionSummary>
      <AccordionDetails>
        <StaticTimePicker
          displayStaticWrapperAs="mobile"
          ampm={false}
          value={endField.value}
          minTime={startMeta.value}
          minutesStep={5}
          orientation={'landscape'}
          onChange={(newValue) => {
            endHelper.setValue(newValue)
          }}
          renderInput={(params) => <MuiTextField {...params} />}
        />
      </AccordionDetails>
    </Accordion>
  )
}
