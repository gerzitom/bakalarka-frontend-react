import React, { FC, useMemo, useState } from 'react'
import { FormikProps } from 'formik'
import { differenceInMinutes } from 'date-fns'
import { Box } from '@mui/system'
import { Slider, Typography } from '@mui/material'
import { computeStudentTime } from '../utils'
import { NewTestDto } from '../entities'

type MyProps = {
  formikProps: FormikProps<NewTestDto>
}
export const StudentTimeToTest: FC<MyProps> = ({ formikProps }) => {
  const totalMinutes = differenceInMinutes(
    formikProps.values.endDate,
    formikProps.values.startDate,
  )
  const [state, setState] = useState(formikProps.values.duration)
  const minutes = useMemo(
    () => computeStudentTime(totalMinutes, state),
    [state, formikProps.values.startDate, formikProps.values.endDate],
  )

  return (
    <div>
      <Box sx={{ display: 'flex', justifyContent: 'space-between' }}>
        <Typography variant={'h3'} sx={{ mt: 5 }}>
          Čas studenta na test
        </Typography>
        <Typography variant={'h3'} sx={{ mt: 5 }}>
          {minutes} min
        </Typography>
      </Box>
      <Slider
        value={state}
        onChange={(ev, value) => setState(Number(value))}
        onChangeCommitted={(ev, value) =>
          formikProps.setFieldValue('duration', value)
        }
        marks={[
          { value: 0, label: '0 min' },
          { value: 100, label: `${totalMinutes} min` },
        ]}
      />
    </div>
  )
}
