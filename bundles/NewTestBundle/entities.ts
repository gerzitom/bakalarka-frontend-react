export type NewTestDto = {
  name: string
  classroomId: number | string
  template: number | string
  startDate: Date
  endDate: Date
  duration: number
}
