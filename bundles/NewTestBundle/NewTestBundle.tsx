import React, { FC, useMemo, useState } from 'react'
import { Box } from '@mui/system'
import { Grid, Slider, Typography } from '@mui/material'
import { Field, Form, Formik, FormikProps } from 'formik'
import { Classroom, TestTemplate } from '../../types/entities'
import { useNotifications } from '../../utils/useNotifications'
import { FormikSubmitHandler } from '../../types'
import { addMinutes, differenceInMinutes } from 'date-fns'
import { NewTestClassroomSelect } from './components/NewTestClassroomSelect'
import { DashboardPanel } from '../../components/DashboardPanel'
import * as Yup from 'yup'
import { LoadingButton } from '@mui/lab'
import { TextField } from 'formik-mui'
import { NewTestDto } from './entities'
import { NewTestChooseTemplate } from './components/NewTestChooseTemplate'
import { ChooseStartTime } from './components/ChooseStartTime'
import { ChooseEndTime } from './components/ChooseEndTime'
import { RecommendedTime } from './components/RecommendedTime'
import TestService from '../../utils/api/TestService'
import { StudentTimeToTest } from './components/StudentTimeToTest'
import { computeStudentTime } from './utils'
import * as Sentry from '@sentry/browser'
import { useRouter } from 'next/router'

type Props = {
  classrooms: Classroom[]
  templates: TestTemplate[]
}
export const NewTestBundle: FC<Props> = ({ classrooms, templates }) => {
  const { successNotification, errorNotification } = useNotifications()
  const router = useRouter()
  const initVals: NewTestDto = {
    name: '',
    classroomId: classrooms[0].id,
    template: templates[0].id,
    startDate: new Date(),
    endDate: addMinutes(new Date(), templates[0].recommendedTime),
    duration: 100,
  }

  const formikSubmit: FormikSubmitHandler<NewTestDto> = (values) => {
    const totalMinutes = differenceInMinutes(values.endDate, values.startDate)
    const dto: NewTestDto = {
      ...values,
      duration: computeStudentTime(totalMinutes, values.duration),
    }
    TestService.generateTest(dto)
      .then(() => {
        successNotification('Test byl vytvořen!')
        router.push('/app')
      })
      .catch((err) => {
        Sentry.captureException(err)
        errorNotification(err.message)
      })
  }

  const Schema = Yup.object().shape({
    name: Yup.string().required('Jméno je povinné'),
    duration: Yup.number()
      .required()
      .positive()
      .integer()
      .moreThan(0, 'Musíte zadat čas na test'),
  })

  return (
    <Box>
      <Box sx={{ mt: 2 }}>
        <Formik
          initialValues={initVals}
          onSubmit={formikSubmit}
          validationSchema={Schema}
        >
          {(formikProps) => (
            <Form>
              <Grid container spacing={4}>
                <Grid item xs={6}>
                  <DashboardPanel heading={'Název testu'}>
                    <Field
                      name={'name'}
                      label={'Název testu'}
                      variant={'standard'}
                      fullWidth
                      error={!!formikProps.errors.name}
                      helperText={formikProps.errors.name}
                      component={TextField}
                    />
                  </DashboardPanel>
                  <DashboardPanel heading={'Čas testu'}>
                    <ChooseStartTime />
                    <ChooseEndTime />
                    <RecommendedTime templates={templates} />
                    <StudentTimeToTest formikProps={formikProps} />
                  </DashboardPanel>
                  <NewTestClassroomSelect
                    classrooms={classrooms ?? []}
                    formikProps={formikProps}
                  />
                </Grid>
                <Grid item xs={6}>
                  <DashboardPanel heading={'Šablona testu'}>
                    <NewTestChooseTemplate
                      templates={templates}
                      formikProps={formikProps}
                    />
                  </DashboardPanel>

                  <LoadingButton
                    size={'large'}
                    color={'primary'}
                    variant={'contained'}
                    type={'submit'}
                    fullWidth
                  >
                    Vytvořit test
                  </LoadingButton>
                </Grid>
              </Grid>
            </Form>
          )}
        </Formik>
      </Box>
    </Box>
  )
}
