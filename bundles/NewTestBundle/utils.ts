import { SelectItem } from '../../types'

export const mapToSelect = (
  arr: { id: number; name: string }[],
): SelectItem[] =>
  arr.map((item) => ({
    value: item.id,
    label: item.name,
  }))

export const computeStudentTime = (totalMinutes: number, percent: number) =>
  Math.round((totalMinutes * percent) / 100)
