export type UserSaveDto = {
  username: string
  password: string
  firstName: string
  surname: string
  email: string
}
