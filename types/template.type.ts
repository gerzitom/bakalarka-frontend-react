export type PrefferedType =
  | 'ONE_OPTION'
  | 'MULTIPLE_OPTION'
  | 'TEXT_AREA'
  | 'SPOJOVACKA'
