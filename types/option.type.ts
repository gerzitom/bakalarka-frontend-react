export type TemplateOptionSaveDto = {
  text?: string
  additionalInfo?: string
  correct?: boolean
}
