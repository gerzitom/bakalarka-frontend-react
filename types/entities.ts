import { PrefferedType } from './template.type'

export type BasicEntity = {
  id: number
}

export type Test = BasicEntity & {
  name: string
  startDate: string
  endDate: string
  duration: number
  maxPoints: number
  questionGroups: QuestionGroup[]
  results: {
    points: number
    maxPoints: number
    timeFinished: Date
    timeStarted: Date
  }[]
}

export type UserAnswers = {
  questionGroupId: number
  userAnswersQuestionsDtos: {
    possibleAnswers: Option[]
    questionId: number
  }[]
}

export type OpenAnswer = BasicEntity & {
  answer: string
  marked: boolean
  questionReadDto: {
    id: number
  }
}

export type TestAnswers = {
  rightAnswers: any[]
  userAnswers: any[]
  testSummaryReadDto: {
    numberOfPoints: number
    timeStarted: string
    timeFinished: string
    maxPoints: number
  }
}

export type QuestionType = 'ONE_OPTION' | 'MULTIPLE_OPTIONS'
export type TestTemplatePart = BasicEntity & {
  name: string
  description: string
  estimatedTime: number
  templateId: number
  points: number
  questionGroups: QuestionGroup[]
  preferredType: PrefferedType
}

export type TestTemplate = BasicEntity & {
  name: string
  authorId: number
  parts: TestTemplatePart[]
  recommendedTime: number
}

export const QuestionTypes = {
  ONE_OPTION: 'Single option',
  MULTIPLE_OPTIONS: 'Multiple option',
  SPOJOVACKA: 'Spojovačka',
  OPEN: 'Text',
}

export type Option = BasicEntity & {
  text: string
  singleOption: {
    text: string
  }
}

export type OptionsProps = {
  options: Option[]
  questionId: number
  disabled?: boolean
  value?: any
  rightAnswers?: number[]
}

export type Question = BasicEntity & {
  content: string
  templateId: number
  answerType: QuestionType
  points: number
  possibleAnswers: Option[]
}

export type QuestionGroupType = 'SIMPLE' | 'SPOJOVACKA'
export type QuestionGroup = BasicEntity & {
  instructions: string
  numberOfGeneratedQuestions: number
  partId: number
  groupType: QuestionGroupType
  questions: QuestionWithAnswers[]
}

export type PossibleType = {
  id: number
  singleOption: Option
  correct: boolean
}
export type QuestionWithAnswers = Question & {
  possibleAnswers: PossibleType[]
  rightAnswers: number[]
}

export type Student = BasicEntity & {
  name: string
}

export type Classroom = BasicEntity & {
  name: string
  students: User[]
}

export type UserType = 'admin' | 'student' | 'teacher'
export type User = BasicEntity & {
  name: string
  username: string
  firstname: string
  surname: string
  email: string
  role: UserType
  token: string
}

export type NewQuestionGroupDto = {
  instructions: string
}

export type NewQuestionDto = {
  content: string
  answerType: QuestionType
}

export type TestQuestionGroup = BasicEntity &
  NewQuestionGroupDto & {
    questions: Question[]
    groupType: 'SIMPLE' | 'SPOJOVACKA' | 'FILL_GAPS' | 'OPEN'
  }

export type StudentTestSummary = BasicEntity & {
  numberOfPoints: number
  maxPoints: number
  student: User
  timeStarted: string
  timeFinished: string
  testId: number
}
