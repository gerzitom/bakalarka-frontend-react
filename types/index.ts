import { FormikHelpers } from 'formik'

export type SelectItem = {
  value: string | number
  label: string
}

export type FormikSubmitHandler<Values> = (
  values: Values,
  formikHelpers: FormikHelpers<Values>,
) => void | Promise<any>
