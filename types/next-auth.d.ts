declare module 'next-auth' {
  import { DefaultSession } from 'next-auth'

  interface Session {
    user: {
      role: string
    } & DefaultSession['user']
  }
}
