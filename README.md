# Bachelor thesis

## Getting Started

### Using with backend

Backend for this app is located here https://gitlab.com/gerzitom/bakalarka-backend.
No run backend, please follow instructions specified in this repository.

To use locally run backend first. Then clone this repository

```bash
git clone https://gitlab.com/gerzitom/bakalarka-backend.git
```

Then go to root of the project and install dependencies by

```bash
npm i
```

Then run Next.js

```bash
npm run dev
# or
yarn dev
```

Everything is setup to work with localy deployed backend, so there is no need to change any `.env` properties.

And visit http://localhost:3000/ For landing page of the whole app.
The whole app is located under /app route.
