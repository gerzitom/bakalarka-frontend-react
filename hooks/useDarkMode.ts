import { useTheme } from '@mui/system'

export const useDarkMode = () => {
  const theme = useTheme()

  return {
    isDarkMode: theme.palette.mode === 'dark',
  }
}
