import { myFetch } from '../utils/ReactQueryClient'
import { useSession } from 'next-auth/react'

type RequestMethod = 'POST' | 'GET' | 'PUT' | 'DELETE'

export const useRequest = () => {
  const { data } = useSession()
  const request = async <Body>(
    queryKey: string,
    method: RequestMethod,
    body: any = null,
  ) => {
    const baseUrl = 'http://localhost:8080/rest'
    const url = baseUrl + queryKey
    if (!data) return Promise.resolve()
    const headers = [{ key: 'Authorization', value: 'Basic ' + data?.token }]
    const response = (await myFetch(url, method, body, headers)) as Body
    return response
  }

  return {
    request,
  }
}
