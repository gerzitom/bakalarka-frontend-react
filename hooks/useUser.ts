import { createContext, useContext } from 'react'
import { User } from '../types/entities'

// @ts-ignore
export const UserContext = createContext<User>(undefined)

export const useUser = () => {
  const user = useContext(UserContext)
  return { user }
}
