import { QueryClient, QueryFunction } from 'react-query'

export const settings = {
  token: '',
}
type RequestMethod = 'POST' | 'GET' | 'PUT' | 'DELETE'

type HeaderType = {
  key: string
  value: string
}

export const myFetch = async (
  url: string,
  method: RequestMethod = 'GET',
  body: any = null,
  additionalHeaders: HeaderType[] | null = null,
) => {
  const myHeaders = new Headers({
    'Content-Type': 'application/json',
    Authorization: 'Basic ' + settings.token,
  })
  additionalHeaders?.forEach((header) =>
    myHeaders.set(header.key, header.value),
  )
  const response = await fetch(url, {
    method: method,
    headers: myHeaders,
    body: body ? JSON.stringify(body) : null,
  })
  const text = await response.text()
  if (!response.ok) throw Error(text)
  else {
    return text ? JSON.parse(text) : {}
  }
}
export const request = async <Body>(
  queryKey: string,
  method: RequestMethod,
  body: any = null,
  headers: HeaderType[] | null = null,
): Promise<Body> => {
  const baseUrl = process.env.URL
  const url = baseUrl + queryKey
  return (await myFetch(url, method, body, headers)) as Body
}
const defaultQueryFn: QueryFunction = async ({ queryKey }) => {
  const baseUrl = process.env.URL
  if (!baseUrl) throw new Error('BE URL is not defined')
  return await myFetch(baseUrl + queryKey[0])
}

export const queryClient = new QueryClient({
  defaultOptions: {
    queries: {
      queryFn: defaultQueryFn,
    },
  },
})
