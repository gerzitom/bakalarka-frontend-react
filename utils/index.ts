import {
  addMinutes,
  differenceInHours,
  differenceInMinutes,
  format,
} from 'date-fns'
import { Test, TestTemplate } from '../types/entities'
import { createContext } from 'react'

export const formatTime = (
  date: Date | string,
  onlyTime: boolean = false,
): string => {
  if (!date) return ''
  if (typeof date === 'string') date = new Date(date)
  if (isNaN(Date.parse(date.toString()))) return ''
  if (onlyTime) return format(date, 'HH:mm')
  else return format(date, 'd. M. HH:mm')
}

export const isTimeClose = (date: Date): boolean => {
  const now = new Date()
  const diffInHours = differenceInHours(date, now)
  return diffInHours < 2
}

export const computeTestRealEnd = (test: Test): Date => {
  const testEndTime = new Date(test.endDate)
  const started = test.results[0].timeStarted
    ? new Date(test.results[0].timeStarted)
    : null
  if (!started) return testEndTime
  const computedEndTime = addMinutes(started!, test.duration)
  return computedEndTime < testEndTime ? computedEndTime : testEndTime
}

export const computeMinutesToEnd = (test: Test) => {
  const testEndTime = new Date(test.endDate)
  const started = test.results[0].timeStarted
    ? new Date(test.results[0].timeStarted)
    : null
  if (!started) return test.duration
  const now = new Date()
  const computedEndTime = addMinutes(started!, test.duration)
  const finalEndTime =
    computedEndTime < testEndTime ? computedEndTime : testEndTime
  return differenceInMinutes(finalEndTime, now)
}
