import { request } from '../ReactQueryClient'
import { UserSaveDto } from '../../types/userEntities'
import { User } from '../../types/entities'

const registerUser = async (userSaveDto: UserSaveDto) => {
  return await request(`/user/register`, 'POST', userSaveDto)
}

const loginUser = async (username: string, password: string) => {
  return request('/user/login', 'POST', { username, password })
}

const getCurrentUser = async (authString?: string): Promise<User> => {
  return request(`/user/me`, 'GET')
}

const UserService = {
  registerUser,
  getCurrentUser,
  loginUser,
}

export default UserService
