import { request } from '../ReactQueryClient'
import { NewQuestionGroupDto, QuestionGroup } from '../../types/entities'

const removeOption = async (optionId: number) => {
  await request(`/test_templates/questions/option/${optionId}`, 'DELETE')
}

const updateOption = async (optionId: number, state: boolean) => {
  await request(`/test_templates/questions/option/${optionId}`, 'PUT', {
    correct: state,
  })
}

const addNewQuestionGroup = async (
  partId: number,
  newQuestionGroup: NewQuestionGroupDto,
): Promise<QuestionGroup> => {
  return await request(
    `/test_templates/parts/${partId}/group`,
    'POST',
    newQuestionGroup,
  )
}
const QuestionService = {
  removeOption,
  addNewQuestionGroup,
  updateOption,
}

export default QuestionService
