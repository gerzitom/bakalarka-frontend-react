import { request } from '../ReactQueryClient'
import { NewTestDto } from '../../bundles/NewTestBundle/entities'
import { OpenAnswer } from '../../types/entities'

const startTest = async (testId: number) => {
  return request(`/tests/${testId}/start`, 'POST')
}

const endTest = async (testId: number) => {
  return request(`/tests/${testId}/end`, 'POST')
}

const saveQuestionAnswer = async (
  testId: number,
  questionId: number,
  answer: any,
) => {
  const correctAnswer = Array.isArray(answer) ? answer : [Number(answer)]
  const body = {
    questionId: questionId,
    answerIds: answer ? correctAnswer : [],
  }
  return request(`/tests/${testId}/saveAnswer`, 'POST', body)
}

const saveOpenAnswer = async (
  testId: number,
  questionId: number,
  answer: string,
) => {
  return request(`/tests/${testId}/saveOpenAnswer`, 'POST', {
    questionId: questionId,
    answer,
  })
}

const generateTest = async (dto: NewTestDto) => {
  return request(`/tests/${dto.template}/generateTest`, 'POST', dto)
}

const evaluateOpenAnswer = async (answerId: number, points: number) => {
  return request(`/tests/${answerId}/evaluate`, 'POST', {
    points,
  })
}

const getOpenAnswer = async (questionId: number): Promise<OpenAnswer[]> => {
  return request<OpenAnswer[]>(
    `/tests/question/${questionId}/openAnswer`,
    'GET',
  )
}

const deleteTest = async (testId: number) => {
  return request(`/exams/${testId}`, 'DELETE')
}

const TestService = {
  startTest,
  endTest,
  saveQuestionAnswer,
  generateTest,
  saveOpenAnswer,
  getOpenAnswer,
  evaluateOpenAnswer,
  deleteTest,
}

export default TestService
