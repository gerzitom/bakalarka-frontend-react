import {
  BasicEntity,
  NewQuestionDto,
  Question,
  TestTemplate,
  TestTemplatePart,
} from '../../types/entities'
import { request } from '../ReactQueryClient'
import { TemplateOptionSaveDto } from '../../types/option.type'

export type TemplatePartCreateDTO = Pick<
  TestTemplatePart,
  'name' | 'preferredType' | 'estimatedTime' | 'points' | 'description'
>

const addNewTestTemplate = async (
  templateName: string,
): Promise<TestTemplate> => {
  return request(`/test_templates`, 'POST', { name: templateName })
}

const deleteTestTemplate = async (templateId: number) => {
  return request(`/test_templates/${templateId}`, 'DELETE')
}

const deleteTestTemplateGroup = async (qrouId: number) => {
  return request(`/test_templates/parts/group/${qrouId}`, 'DELETE')
}

const addTestPart = async (
  part: TemplatePartCreateDTO,
  testTemplateId: number,
): Promise<BasicEntity> => {
  const dto: TemplatePartCreateDTO = {
    ...part,
    estimatedTime: Number(part.estimatedTime),
    points: Number(part.points),
  }
  return request(`/test_templates/${testTemplateId}/parts`, 'POST', dto)
}

const updateQuestionGroup = async (questionId: number, assignment: string) => {
  return request(`/test_templates/parts/group/${questionId}`, 'PUT', {
    instructions: assignment,
  })
}

const updateQuestion = async (questionId: number, assignment: string) => {
  return request(`/test_templates/parts/groups/question/${questionId}`, 'PUT', {
    content: assignment,
  })
}

const addNewQuestion = async (
  questionGroupId: number,
  question: NewQuestionDto,
): Promise<Question> => {
  return request(
    `/test_templates/parts/group/${questionGroupId}/question`,
    'POST',
    question,
  )
}

const addNewSpojovackaLine = async (
  questionGroupId: number,
  content: string,
  answer: string,
) => {
  const newQuestion = await addNewQuestion(questionGroupId, {
    content,
    answerType: 'ONE_OPTION',
  })
  await addNewOption(newQuestion.id, {
    text: answer,
    correct: true,
  })
}

const addNewOption = async (questionId: number, dto: TemplateOptionSaveDto) => {
  return request(`/test_templates/questions/${questionId}/option`, 'POST', dto)
}

const updateOption = async (
  optionId: number,
  updateDto: TemplateOptionSaveDto,
) => {
  return request(
    `/test_templates/questions/option/${optionId}`,
    'PUT',
    updateDto,
  )
}

const deletePart = async (partId: number) => {
  return request(`/test_templates/parts/${partId}`, 'DELETE')
}

const deleteQuestion = async (questionId: number) => {
  return request(
    `/test_templates/parts/groups/question/${questionId}`,
    'DELETE',
  )
}

const TestTemplateService = {
  addTestPart,
  updateQuestionGroup,
  updateQuestion,
  addNewTestTemplate,
  addNewQuestion,
  addNewOption,
  updateOption,
  deletePart,
  deleteQuestion,
  addNewSpojovackaLine,
  deleteTestTemplate,
  deleteTestTemplateGroup,
}

export default TestTemplateService
