import { useSnackbar } from 'notistack'
import { useCallback } from 'react'

export const useNotifications = () => {
  const { enqueueSnackbar } = useSnackbar()
  const successNotification = useCallback(
    (messageText: string | null = null) => {
      const defaultText = 'Uloženo!'
      enqueueSnackbar(messageText ?? defaultText, {
        variant: 'success',
      })
    },
    [],
  )
  const errorNotification = useCallback((errorText: string | null = null) => {
    const defaultErrorText =
      'Chyba v komunikaci se serverem, prosím zkuste to za chvíli.'
    enqueueSnackbar(errorText ?? defaultErrorText, {
      variant: 'error',
    })
  }, [])
  return { successNotification, errorNotification }
}
