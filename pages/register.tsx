import React from 'react'
import { Field, Formik } from 'formik'
import { FormikSubmitHandler } from '../types'
import { FormikInsideForm } from '../components/FormikInsideForm'
import { TextField } from 'formik-mui'
import { NextPage } from 'next'
import { Button, Grid, Paper, Typography } from '@mui/material'
import styled from 'styled-components'
import { UserSaveDto } from '../types/userEntities'
import userService from '../utils/api/UserService'
import { useRouter } from 'next/router'
import { useNotifications } from '../utils/useNotifications'
import * as Sentry from '@sentry/browser'

type Props = {}
const RegisterPage: NextPage<Props> = (props) => {
  const router = useRouter()
  const { errorNotification } = useNotifications()
  const { registerUser } = userService
  const initVals: UserSaveDto = {
    username: 'gerzitom',
    firstName: 'Tomáš',
    surname: 'Geržičák',
    password: 'haha',
    email: 'tomas.gerzicak@gmail.com',
  }

  const handleSubmit: FormikSubmitHandler<UserSaveDto> = (values) => {
    registerUser(values)
      .then((response) => {
        router.push('/')
      })
      .catch((err) => {
        Sentry.captureException(err)
        errorNotification(err.message)
      })
  }

  const fieldProps = {
    sx: {
      my: 2,
    },
    variant: 'standard',
  }
  return (
    <Formik initialValues={initVals} onSubmit={handleSubmit}>
      {(formikProps) => (
        <RegisterContainer>
          <Paper sx={{ p: 5 }}>
            <FormikInsideForm formikProps={formikProps}>
              <Typography variant={'h1'}>Registrace uživatele</Typography>
              <Field
                label={'Uživatelské jméno'}
                name={'username'}
                component={TextField}
                fullWidth
                {...fieldProps}
              />
              <Grid container spacing={4}>
                <Grid item xs={6}>
                  <Field
                    label={'Jméno'}
                    name={'firstName'}
                    component={TextField}
                    fullWidth
                    {...fieldProps}
                  />
                </Grid>
                <Grid item xs={6}>
                  <Field
                    label={'Příjmení'}
                    name={'firstName'}
                    component={TextField}
                    fullWidth
                    {...fieldProps}
                  />
                </Grid>
              </Grid>
              <Field
                label={'E-mail'}
                name={'email'}
                type={'email'}
                component={TextField}
                fullWidth
                {...fieldProps}
              />
              <Field
                label={'Heslo'}
                name={'email'}
                type={'password'}
                component={TextField}
                fullWidth
                {...fieldProps}
              />
              <Button
                type={'submit'}
                fullWidth
                {...fieldProps}
                variant={'outlined'}
              >
                Registrovat
              </Button>
            </FormikInsideForm>
          </Paper>
        </RegisterContainer>
      )}
    </Formik>
  )
}

const RegisterContainer = styled.div`
  flex-grow: 1;
  display: flex;
  justify-content: center;
  align-items: center;
`

export default RegisterPage
