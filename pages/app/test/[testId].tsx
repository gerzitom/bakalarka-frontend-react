import React from 'react'
import { GetServerSideProps, NextPage } from 'next'
import { BasicLayout } from '../../../components/layout/BasicLayout'
import { Alert, Button, CircularProgress, Container } from '@mui/material'
import { useQuery } from 'react-query'
import { Test, TestAnswers, UserAnswers } from '../../../types/entities'
import { TestPageBundle } from '../../../bundles/TestPage'
import Link from 'next/link'
import { useGlobalLoading } from '../../../components/GlobalLoader'

type Props = {
  testId: number
}
const TestPage: NextPage<Props> = ({ testId }) => {
  const {
    data: test,
    isLoading: testIsLoading,
    error,
    isError,
  } = useQuery<Test>(`/tests/${testId}`)
  const { data: results, isLoading } = useQuery<UserAnswers[]>(
    `/tests/${testId}/userAnswers`,
  )
  const { data: textAnswers, isLoading: textAnswersLoading } = useQuery<
    UserAnswers[]
  >(`/tests/${testId}/TextAnswers`)
  const { stopGlobalLoading } = useGlobalLoading()
  if (testIsLoading && isLoading && !results) return <CircularProgress />
  stopGlobalLoading() // stop loading when student clicked on start test
  return (
    <BasicLayout>
      <Container>
        {error && (
          <>
            <Alert severity={'error'}>Tento test nebyl nalezen</Alert>
            <Link href={'/'} passHref>
              <Button>Přejít na domovskou stránku</Button>
            </Link>
          </>
        )}
        {!error && !testIsLoading && test && (
          <TestPageBundle test={test!} answers={results!} />
        )}
        {!error && !testIsLoading && !test && (
          <Alert severity={'error'}>Chyba v načítání testu</Alert>
        )}
        {testIsLoading && <CircularProgress />}
      </Container>
    </BasicLayout>
  )
}

// @ts-ignore
TestPage.auth = true

export default TestPage

export const getServerSideProps: GetServerSideProps = async (context) => {
  return {
    props: {
      testId: context.params?.testId,
    },
  }
}
