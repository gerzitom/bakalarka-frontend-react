import React, { useMemo } from 'react'
import { GetServerSideProps, NextPage } from 'next'
import { BasicLayout } from '../../components/layout/BasicLayout'
import { Alert, Breadcrumbs, Typography } from '@mui/material'
import Link from 'next/link'
import { useQuery } from 'react-query'
import { Classroom, TestTemplate } from '../../types/entities'
import { FullpageSpinner } from '../../components/FullpageSpinner'
import { NewTestBundle } from '../../bundles/NewTestBundle/NewTestBundle'
import { NoTemplatePage } from '../../bundles/NewTestBundle/components/NoTemplatePage'

type Props = {
  teacherId: number
}
const NewTestPage: NextPage<Props> = ({ teacherId }) => {
  const { data: classrooms, isLoading: classroomsLoading } =
    useQuery<Classroom[]>('/classrooms')
  const { data, isLoading: templatesLoading } =
    useQuery<TestTemplate[]>('/test_templates')
  const loading = classroomsLoading && templatesLoading
  if (loading) return <FullpageSpinner />
  return (
    <BasicLayout>
      <Breadcrumbs sx={{ mb: 4 }}>
        <Link href={`/app`}>Hlavní profil</Link>
        <Typography color="text.primary">Nový test</Typography>
      </Breadcrumbs>
      <Typography variant={'h1'} sx={{ textAlign: 'center' }}>
        Naplánovat nový test
      </Typography>
      {data?.length === 0 && <NoTemplatePage />}
      {data && classrooms && data.length > 0 && (
        <NewTestBundle classrooms={classrooms!} templates={data!} />
      )}
    </BasicLayout>
  )
}
// @ts-ignore
NewTestPage.auth = true
export default NewTestPage

export const getServerSideProps: GetServerSideProps = async (context) => {
  return {
    props: {},
  }
}
