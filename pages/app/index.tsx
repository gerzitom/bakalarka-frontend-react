import type { NextPage } from 'next'
import { BasicLayout } from '../../components/layout/BasicLayout'
import { Container } from '@mui/material'
import { HomePageBundle } from '../../bundles/HomePageBundle/HomePageBundle'

const Home: NextPage = () => {
  return (
    <BasicLayout>
      <Container>
        <HomePageBundle />
      </Container>
    </BasicLayout>
  )
}
// @ts-ignore
Home.auth = true

export default Home
