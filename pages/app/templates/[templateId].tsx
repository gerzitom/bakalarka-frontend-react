import React, { FC } from 'react'
import { BasicLayout } from '../../../components/layout/BasicLayout'
import { useQuery } from 'react-query/react'
import { GetServerSideProps } from 'next'
import { TestTemplate } from '../../../types/entities'
import { EditTemplateBundle } from '../../../bundles/EditTemplateBundle/EditTemplateBundle'
import { FullpageSpinner } from '../../../components/FullpageSpinner'
import { Alert } from '@mui/material'

type Props = {
  templateId: string
  url: string
}
const TemplatePage: FC<Props> = ({ templateId, url }) => {
  const { data, isLoading, error } = useQuery<TestTemplate>(
    `/test_templates/${templateId}`,
  )
  return (
    <BasicLayout fullWidth={true}>
      {!isLoading && !error && <EditTemplateBundle template={data!} />}
      {isLoading && !error && <FullpageSpinner />}
      {error && <Alert severity={'error'}>Šablona nebyla nalezena</Alert>}
    </BasicLayout>
  )
}

export const getServerSideProps: GetServerSideProps = async (context) => {
  return {
    props: {
      templateId: context.params?.templateId,
      url: context.req.url,
    },
  }
}

// @ts-ignore
TemplatePage.auth = true

export default TemplatePage
