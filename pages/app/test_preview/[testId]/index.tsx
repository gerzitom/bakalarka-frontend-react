import React, { useEffect, useMemo, useRef } from 'react'
import { GetServerSideProps, NextPage } from 'next'
import { useQuery } from 'react-query'
import { StudentTestSummary, Test } from '../../../../types/entities'
import { TestPreviewBundle } from '../../../../bundles/TestPreviewBundle/TestPreviewBundle'
import { BasicLayout } from '../../../../components/layout/BasicLayout'
import { FullpageSpinner } from '../../../../components/FullpageSpinner'
import { Alert } from '@mui/material'
import { useQueryClient } from 'react-query/react'

type Props = {
  testId: number
}
const TestPreviewPage: NextPage<Props> = ({ testId }) => {
  const { data: test, isLoading: testIsLoading } = useQuery<Test>(
    `/exams/${testId}`,
  )
  const { data: testAnswers, isLoading: testAnswersIsLoading } = useQuery<
    StudentTestSummary[]
  >(`/exams/${testId}/allTests`)
  const queryClient = useQueryClient()
  const loading = useMemo(
    () => testIsLoading && testAnswersIsLoading,
    [testIsLoading, testAnswersIsLoading],
  )

  const ws = useRef<WebSocket | null>(null)

  useEffect(() => {
    WSConnect()
    return () => {
      ws.current?.close()
    }
  }, [])

  const WSConnect = () => {
    ws.current = new WebSocket('ws://localhost:8080/finishTest/' + testId)
    ws.current.onmessage = () => {
      queryClient.refetchQueries(`/exams/${testId}/allTests`)
    }
    ws.current.onclose = () => {
      setTimeout(function () {
        WSConnect()
      }, 1000)
    }
  }
  if (loading) return <FullpageSpinner />
  if (!testAnswers)
    return <Alert severity={'error'}>Výsledky nebyly načteny</Alert>
  return (
    <BasicLayout>
      <TestPreviewBundle test={test!} students={testAnswers!} />
    </BasicLayout>
  )
}

// @ts-ignore
TestPreviewPage.auth = true

export default TestPreviewPage

export const getServerSideProps: GetServerSideProps = async (context) => {
  return {
    props: {
      testId: context.params?.testId,
    },
  }
}
