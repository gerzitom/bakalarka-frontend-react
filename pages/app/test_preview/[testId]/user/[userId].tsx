import React, { createContext, useContext, useMemo } from 'react'
import { GetServerSideProps, NextPage } from 'next'
import { useQuery } from 'react-query'
import { Test, TestAnswers, TestTemplate } from '../../../../../types/entities'
import { BasicLayout } from '../../../../../components/layout/BasicLayout'
import { StudentTestResults } from '../../../../../bundles/TestPage/components/StudentTestResults'
import { FullpageSpinner } from '../../../../../components/FullpageSpinner'
import TestPreviewPage from '../index'
import { Button, Grid, Paper, Typography } from '@mui/material'
import { TestResultIndicator } from '../../../../../bundles/TestPage/components/TestResultIndicator'
import { formatTime } from '../../../../../utils'
import Link from 'next/link'
import { useQueryClient } from 'react-query/react'

type TeacherPreview = {
  refetch: () => void
}

export const TeacherPreviewContext = createContext<TeacherPreview | undefined>(
  undefined,
)

export const useTeacherPreviewContext = () => {
  const teacherPreview = useContext(TeacherPreviewContext)
  return teacherPreview
}

type Props = {
  userId: number
  testId: number
}
const UserTestPreview: NextPage<Props> = ({ userId, testId }) => {
  const refetchInterval = 1000 * 60 // one minute
  const queryClient = useQueryClient()
  const { data: results, isLoading } = useQuery<TestAnswers>(
    `/tests/${testId}/answers`,
    { refetchInterval: refetchInterval },
  )
  const { data: test, isLoading: testIsLoading } = useQuery<Test>(
    `/tests/${testId}`,
  )
  const refetch = () => {
    queryClient.refetchQueries()
  }
  if (!test && !results) return <FullpageSpinner />
  return (
    <TeacherPreviewContext.Provider value={{ refetch }}>
      <BasicLayout>
        <Grid container spacing={4}>
          <Grid item xs={8}>
            <StudentTestResults test={test!} results={results!} />
          </Grid>
          <Grid item xs={4}>
            <Paper sx={{ p: 3 }}>
              <Typography variant={'h3'} sx={{ mb: 2 }}>
                Výsledek
              </Typography>
              <TestResultIndicator
                maxPoints={results?.testSummaryReadDto.maxPoints ?? 0}
                totalPoints={results?.testSummaryReadDto.numberOfPoints ?? 0}
              />
              <Grid sx={{ pt: 4 }} container>
                <Grid item xs={6}>
                  <Typography variant={'overline'}>Začátek testu</Typography>
                  <Typography variant={'h5'}>
                    {formatTime(
                      results?.testSummaryReadDto.timeStarted ?? '',
                      true,
                    )}
                  </Typography>
                </Grid>
                <Grid item xs={6}>
                  <Typography variant={'overline'}>Konec testu</Typography>
                  <Typography variant={'h5'}>
                    {formatTime(
                      results?.testSummaryReadDto.timeFinished ?? '',
                      true,
                    )}
                  </Typography>
                </Grid>
              </Grid>

              <Link href={'/app'}>
                <Button variant={'contained'} fullWidth sx={{ mt: 4 }}>
                  Ukončit prohlídku
                </Button>
              </Link>
            </Paper>
          </Grid>
        </Grid>
      </BasicLayout>
    </TeacherPreviewContext.Provider>
  )
}

//@ts-ignore
UserTestPreview.auth = true

export default UserTestPreview

export const getServerSideProps: GetServerSideProps = async (context) => {
  return {
    props: {
      testId: context.params?.testId,
      userId: context.params?.userId,
    },
  }
}
