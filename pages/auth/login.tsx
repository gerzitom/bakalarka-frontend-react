import React, { useEffect, useState } from 'react'
import { GetServerSideProps, NextPage } from 'next'
import { Alert, Button, Card, Typography } from '@mui/material'
import { Field, Formik } from 'formik'
import { FormikSubmitHandler } from '../../types'
import { TextField } from 'formik-mui'
import styled from 'styled-components'
import { signIn, useSession } from 'next-auth/react'
import { FormikInsideForm } from '../../components/FormikInsideForm'
import { useNotifications } from '../../utils/useNotifications'
import { useRouter } from 'next/router'

type FormikVals = {
  username: string
  password: string
}
type Props = {
  callbackUrl: string
  error?: string
}
const LoginPage: NextPage<Props> = ({ callbackUrl, error }) => {
  const { data } = useSession()
  const [authError, setAuthError] = useState(false)
  const router = useRouter()
  const { successNotification } = useNotifications()
  useEffect(() => {
    if (data?.token) {
      successNotification('Již jste přihlášeni')
      router.push(callbackUrl ?? '/')
    }
  }, [data])

  const initVals: FormikVals = {
    username: '',
    password: '',
  }

  const handleSubmit: FormikSubmitHandler<FormikVals> = async (values) => {
    const response = await signIn('credentials', {
      redirect: false,
      email: values.username,
      password: values.password,
      callbackUrl: callbackUrl ?? '',
    })
    //@ts-ignore
    if (response?.status === 401) {
      setAuthError(true)
    }
  }

  const inputProps = {
    fullWidth: true,
    sx: { mb: 2 },
  }
  return (
    <StyledContainer>
      <Card sx={{ p: 3, maxWidth: '400px' }}>
        <Typography variant={'h2'}>Přihlášení</Typography>
        {error && (
          <Alert sx={{ my: 3 }} severity={'error'}>
            Přihlášení se nezdařilo, uživatel nebyl nalezen.
          </Alert>
        )}
        {authError && (
          <Alert sx={{ my: 3 }} severity={'error'}>
            Přihlášení se nezdařilo
          </Alert>
        )}
        <Formik initialValues={initVals} onSubmit={handleSubmit}>
          {(formikProps) => (
            <FormikInsideForm formikProps={formikProps}>
              <Field
                name={'username'}
                component={TextField}
                label={'Uživatelské jméno'}
                {...inputProps}
              />
              <Field
                name={'password'}
                component={TextField}
                label={'Heslo'}
                type={'password'}
                {...inputProps}
              />
              <Button
                fullWidth
                color={'primary'}
                variant={'contained'}
                type={'submit'}
              >
                Přihlásit
              </Button>
            </FormikInsideForm>
          )}
        </Formik>
      </Card>
    </StyledContainer>
  )
}

const StyledContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  margin-top: 5%;
`
export default LoginPage

export const getServerSideProps: GetServerSideProps = async (context) => {
  return {
    props: {
      callbackUrl: context.query.callbackUrl ?? null,
      error: context.query.error ?? null,
    },
  }
}
