import '../styles/globals.css'
import type { AppProps } from 'next/app'
import { QueryClientProvider } from 'react-query'
import { createTheme } from '@mui/material/styles'
import { queryClient, settings } from '../utils/ReactQueryClient'
import { CssBaseline, ThemeProvider, useMediaQuery } from '@mui/material'
import DateAdapter from '@mui/lab/AdapterDateFns'
import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns'
import csLocale from 'date-fns/locale/cs'
import { LocalizationProvider } from '@mui/lab'
import { SessionProvider, signIn, useSession } from 'next-auth/react'
import { ReactQueryDevtools } from 'react-query/devtools'
import { SnackbarOrigin, SnackbarProvider } from 'notistack'
import darkTheme from '../styles/theme/darkTheme'
import lightTheme from '../styles/theme/lightTheme'
import { ThemeProvider as StyledThemeProvider } from 'styled-components'
import { FC, useEffect, useState } from 'react'
import ErrorBoundary from '../components/ErrorBoundary'
import * as Sentry from '@sentry/react'
import { FullpageSpinner } from '../components/FullpageSpinner'
import { GlobalLoadingContext } from '../components/GlobalLoader'

function MyApp({ Component, pageProps: { session, ...pageProps } }: AppProps) {
  const prefersDarkMode = useMediaQuery('(prefers-color-scheme: dark)')
  const themeSetup = prefersDarkMode ? darkTheme : lightTheme
  const theme = createTheme(themeSetup)
  theme.shadows[1] = '0px 0px 10px rgba(0, 0, 0, 0.15)'

  const NotistackAnchorOrigins: SnackbarOrigin = {
    vertical: 'top',
    horizontal: 'right',
  }

  const [globalLoading, setGlobalLoading] = useState(false)
  return (
    <Sentry.ErrorBoundary fallback={<p>Error occured</p>} showDialog>
      <QueryClientProvider client={queryClient}>
        <SessionProvider session={session}>
          <ThemeProvider theme={theme}>
            <StyledThemeProvider theme={theme}>
              <LocalizationProvider dateAdapter={AdapterDateFns}>
                <GlobalLoadingContext.Provider
                  value={{ state: globalLoading, setState: setGlobalLoading }}
                >
                  <SnackbarProvider
                    maxSnack={5}
                    anchorOrigin={NotistackAnchorOrigins}
                  >
                    {
                      // @ts-ignore for pages that needs authorization
                      Component?.auth ? (
                        <Auth>
                          <ErrorBoundary>
                            <Component {...pageProps} />
                          </ErrorBoundary>
                        </Auth>
                      ) : (
                        <Component {...pageProps} />
                      )
                    }
                    <CssBaseline />
                  </SnackbarProvider>
                </GlobalLoadingContext.Provider>
              </LocalizationProvider>
            </StyledThemeProvider>
          </ThemeProvider>
        </SessionProvider>
        <ReactQueryDevtools initialIsOpen={false} />
      </QueryClientProvider>
    </Sentry.ErrorBoundary>
  )
}

// @ts-ignore
const Auth: FC = ({ children }) => {
  const { data: session, status } = useSession()
  const isUser = !!session?.user
  const loading = status === 'loading'
  useEffect(() => {
    if (loading) return // Do nothing while loading
    if (!isUser) signIn() // If not authenticated, force log in
  }, [isUser, loading])

  if (isUser) {
    // set auth token for react query
    settings.token = session?.token as string
    return children
  }
  return <FullpageSpinner />
}

export default MyApp
