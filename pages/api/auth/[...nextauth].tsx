import React from 'react'
// import NextAuth from 'next-auth'
import { User } from '../../../types/entities'
// @ts-ignore
import NextAuth, { IncomingRequest } from 'next-auth'
import CredentialsProvider from 'next-auth/providers/credentials'
import { request } from '../../../utils/ReactQueryClient'

// @ts-ignore
export default NextAuth({
  providers: [
    CredentialsProvider({
      name: 'Přihlášení',
      credentials: {
        username: { label: 'Username', type: 'text' },
        password: { label: 'Heslo', type: 'password' },
      },
      async authorize<C>(
        credentials: Record<keyof C, string> | undefined,
        req: Pick<IncomingRequest, 'body' | 'query' | 'headers' | 'method'>,
      ): Promise<User | null> {
        try {
          // @ts-ignore
          const authParts = credentials?.email + ':' + credentials?.password
          const authString = btoa(authParts)
          const headers = [
            { key: 'Authorization', value: 'Basic ' + authString },
          ]
          const response = (await request(
            `/user/me`,
            'GET',
            null,
            headers,
          )) as User
          response.token = authString
          return response
        } catch (e) {
          return null
        }
      },
    }),
  ],
  secret: 'haha',
  pages: {
    signIn: '/auth/login',
  },
  callbacks: {
    // @ts-ignore
    async jwt({ token, user, account, profile, isNewUser }) {
      if (user) {
        const authToken = user?.token ?? ''
        return {
          ...token,
          authToken,
        }
      }
      return token
    },
    // @ts-ignore
    async session({ session, token }) {
      session.token = token.authToken
      return session
    },
  },
  session: {
    maxAge: 30 * 24 * 60 * 60,
    updateAge: 24 * 60 * 60,
  },
})
