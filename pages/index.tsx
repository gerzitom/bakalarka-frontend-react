import { NextPage } from 'next'
import { LandingPage } from '../bundles/LandingPage'

const HomePage: NextPage = () => {
  return <LandingPage />
}

export default HomePage
