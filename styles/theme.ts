import { createTheme } from '@mui/material/styles'
import { red } from '@mui/material/colors'
import { colors } from './colors'
// Create a theme instance.
const theme = createTheme({
  palette: {
    // mode: 'dark',
    primary: {
      main: colors.main,
    },
    secondary: {
      main: '#0B2B40',
    },
    error: {
      main: red.A400,
    },
  },
})

export default theme
