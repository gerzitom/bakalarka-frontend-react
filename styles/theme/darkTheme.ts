import { ThemeOptions } from '@mui/material/styles/createTheme'
import baseTheme from './baseTheme'
import { colors } from '../colors'
import { red } from '@mui/material/colors'

const darkTheme: ThemeOptions = {
  palette: {
    mode: 'dark',
    primary: {
      main: colors.main,
    },
    secondary: {
      main: '#92EBF0',
    },
    error: {
      main: red.A400,
    },
    background: {
      paper: '#1c1c1c',
    },
  },
  ...baseTheme,
  components: {
    MuiPaper: {
      styleOverrides: {
        root: {
          boxShadow: 'none',
        },
      },
    },
  },
}

export default darkTheme
