import { ThemeOptions } from '@mui/material/styles/createTheme'

const baseTheme: ThemeOptions = {
  components: {
    MuiPaper: {
      // defaultProps: {
      //   sx: {
      //     boxShadow: 1,
      //   },
      // },
      styleOverrides: {
        root: {
          borderRadius: '10px',
        },
      },
    },
    MuiButton: {
      styleOverrides: {
        root: {
          borderRadius: '10px',
        },
      },
    },
  },
  typography: {
    h1: {
      fontFamily: 'Bebas Neue',
      fontSize: '4rem',
      lineHeight: 1,
    },
    h2: {
      fontFamily: 'Bebas Neue',
      fontSize: '2.5rem',
      lineHeight: 1,
    },
    h3: {
      fontFamily: 'Ubuntu',
      fontSize: '1rem',
      textTransform: 'uppercase',
      letterSpacing: 0.5,
      lineHeight: 1,
    },
    fontFamily: ['Ubuntu', 'cursive'].join(','),
  },
}

export default baseTheme
