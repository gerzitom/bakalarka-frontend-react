import { colors } from '../colors'
import { red } from '@mui/material/colors'
import { ThemeOptions } from '@mui/material/styles/createTheme'
import baseTheme from './baseTheme'

const lightTheme: ThemeOptions = {
  palette: {
    mode: 'light',
    primary: {
      main: colors.main,
    },
    secondary: {
      main: '#0B2B40',
    },
    error: {
      main: red.A400,
    },
    background: {
      paper: '#ffffff',
      default: '#F4F4F4',
    },
  },
  ...baseTheme,
}

export default lightTheme
